'use strict';

(function($) {
  $(function() {
    woopq_toggle_options();
  });

  $(document).
      on('change',
          'input[name^="_woopq_quantity"], select[name^="_woopq_type"]',
          function() {
            woopq_toggle_options();
          });

  $('#woocommerce-product-data').
      on('woocommerce_variations_loaded', function() {
        woopq_toggle_options();
      });

  function woopq_toggle_options() {
    $('input[name^="_woopq_quantity"]:checked').each(
        function() {
          if ($(this).val() == 'overwrite') {
            $(this).
                closest('.woopq_table').
                find('.woopq_show_if_overwrite').show();
          } else {
            $(this).
                closest('.woopq_table').
                find('.woopq_show_if_overwrite').hide();
          }
        },
    );

    $('select[name^="_woopq_type"]').each(
        function() {
          var _val = $(this).val();

          $(this).
              closest('.woopq-table').
              find('.woopq_show_if_type').
              hide();

          $(this).
              closest('.woopq-table').
              find('.woopq_show_if_type_' + _val).
              show();
        },
    );
  }
})(jQuery);