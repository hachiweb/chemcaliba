<?php
/*
Plugin Name: WPC Product Quantity for WooCommerce
Plugin URI: https://wpclever.net/
Description: WPC Product Quantity provides powerful controls for product quantity.
Version: 2.4.4
Author: WPClever
Author URI: https://wpclever.net
Text Domain: wpc-product-quantity
Domain Path: /languages/
Requires at least: 4.0
Tested up to: 6.0
WC requires at least: 3.0
WC tested up to: 6.5
*/

defined( 'ABSPATH' ) || exit;

! defined( 'WOOPQ_VERSION' ) && define( 'WOOPQ_VERSION', '2.4.4' );
! defined( 'WOOPQ_URI' ) && define( 'WOOPQ_URI', plugin_dir_url( __FILE__ ) );
! defined( 'WOOPQ_DIR' ) && define( 'WOOPQ_DIR', plugin_dir_path( __FILE__ ) );
! defined( 'WOOPQ_REVIEWS' ) && define( 'WOOPQ_REVIEWS', 'https://wordpress.org/support/plugin/wpc-product-quantity/reviews/?filter=5' );
! defined( 'WOOPQ_CHANGELOG' ) && define( 'WOOPQ_CHANGELOG', 'https://wordpress.org/plugins/wpc-product-quantity/#developers' );
! defined( 'WOOPQ_DISCUSSION' ) && define( 'WOOPQ_DISCUSSION', 'https://wordpress.org/support/plugin/wpc-product-quantity' );
! defined( 'WPC_URI' ) && define( 'WPC_URI', WOOPQ_URI );

include 'includes/wpc-dashboard.php';
include 'includes/wpc-menu.php';
include 'includes/wpc-kit.php';
//include 'includes/wpc-notice.php';

if ( ! function_exists( 'woopq_init' ) ) {
	add_action( 'plugins_loaded', 'woopq_init', 11 );

	function woopq_init() {
		// load text-domain
		load_plugin_textdomain( 'wpc-product-quantity', false, basename( __DIR__ ) . '/languages/' );

		if ( ! function_exists( 'WC' ) || ! version_compare( WC()->version, '3.0', '>=' ) ) {
			add_action( 'admin_notices', 'woopq_notice_wc' );

			return;
		}

		if ( ! class_exists( 'WPCleverWoopq' ) && class_exists( 'WC_Product' ) ) {
			class WPCleverWoopq {
				function __construct() {
					// enqueue backend
					add_action( 'admin_enqueue_scripts', [ $this, 'admin_enqueue_scripts' ], 99 );

					// enqueue frontend
					add_action( 'wp_enqueue_scripts', [ $this, 'enqueue_scripts' ] );

					// settings page
					add_action( 'admin_menu', [ $this, 'admin_menu' ] );

					// settings link
					add_filter( 'plugin_action_links', [ $this, 'action_links' ], 10, 2 );
					add_filter( 'plugin_row_meta', [ $this, 'row_meta' ], 10, 2 );

					// args
					add_filter( 'woocommerce_quantity_input_args', [ $this, 'quantity_input_args' ], 10, 2 );
					add_filter( 'woocommerce_loop_add_to_cart_args', [ $this, 'loop_add_to_cart_args' ], 10, 2 );

					// default input
					add_filter( 'woocommerce_quantity_input_min', [ $this, 'quantity_input_min' ], 10, 2 );
					add_filter( 'woocommerce_quantity_input_max', [ $this, 'quantity_input_max' ], 10, 2 );
					add_filter( 'woocommerce_quantity_input_step', [ $this, 'quantity_input_step' ], 10, 2 );

					// admin input
					add_filter( 'woocommerce_quantity_input_min_admin', [ $this, 'quantity_input_min' ], 10, 2 );
					add_filter( 'woocommerce_quantity_input_step_admin', [ $this, 'quantity_input_step' ], 10, 2 );

					// decimal
					if ( get_option( '_woopq_decimal', 'no' ) === 'yes' ) {
						remove_filter( 'woocommerce_stock_amount', 'intval' );
						add_filter( 'woocommerce_stock_amount', 'floatval' );
					}

					// template
					add_filter( 'wc_get_template', [ $this, 'quantity_input_template' ], 10, 2 );

					// add to cart
					add_filter( 'woocommerce_add_to_cart_validation', [ $this, 'add_to_cart_validation' ], 10, 3 );

					// add to cart message
					if ( get_option( '_woopq_decimal', 'no' ) === 'yes' ) {
						add_filter( 'wc_add_to_cart_message_html', [ $this, 'add_to_cart_message_html' ], 99, 3 );
					}

					// product settings
					add_filter( 'woocommerce_product_data_tabs', [ $this, 'product_data_tabs' ], 10, 1 );
					add_action( 'woocommerce_product_data_panels', [ $this, 'product_data_panels' ] );

					// variation settings
					add_action( 'woocommerce_product_after_variable_attributes', [
						$this,
						'variation_settings'
					], 10, 3 );
				}

				function admin_enqueue_scripts() {
					wp_enqueue_style( 'woopq-backend', WOOPQ_URI . 'assets/css/backend.css', array(), WOOPQ_VERSION );
					wp_enqueue_script( 'woopq-backend', WOOPQ_URI . 'assets/js/backend.js', array( 'jquery' ), WOOPQ_VERSION, true );
				}

				function enqueue_scripts() {
					wp_enqueue_style( 'woopq-frontend', WOOPQ_URI . 'assets/css/frontend.css', array(), WOOPQ_VERSION );
					wp_enqueue_script( 'woopq-frontend', WOOPQ_URI . 'assets/js/frontend.js', array( 'jquery' ), WOOPQ_VERSION, true );
					wp_localize_script( 'woopq-frontend', 'woopq_vars', array(
							'rounding'     => get_option( '_woopq_rounding', 'down' ),
							'auto_correct' => get_option( '_woopq_auto_correct', 'entering' ),
							'timeout'      => apply_filters( 'woopq_auto_correct_timeout', 1000 ),
						)
					);
				}

				function admin_menu() {
					add_submenu_page( 'wpclever', esc_html__( 'WPC Product Quantity', 'wpc-product-quantity' ), esc_html__( 'Product Quantity', 'wpc-product-quantity' ), 'manage_options', 'wpclever-woopq', array(
						$this,
						'admin_menu_content'
					) );
				}

				function admin_menu_content() {
					$active_tab = isset( $_GET['tab'] ) ? sanitize_key( $_GET['tab'] ) : 'settings';
					?>
                    <div class="wpclever_settings_page wrap">
                        <h1 class="wpclever_settings_page_title"><?php echo esc_html__( 'WPC Product Quantity', 'wpc-product-quantity' ) . ' ' . WOOPQ_VERSION; ?></h1>
                        <div class="wpclever_settings_page_desc about-text">
                            <p>
								<?php printf( esc_html__( 'Thank you for using our plugin! If you are satisfied, please reward it a full five-star %s rating.', 'wpc-product-quantity' ), '<span style="color:#ffb900">&#9733;&#9733;&#9733;&#9733;&#9733;</span>' ); ?>
                                <br/>
                                <a href="<?php echo esc_url( WOOPQ_REVIEWS ); ?>"
                                   target="_blank"><?php esc_html_e( 'Reviews', 'wpc-product-quantity' ); ?></a> | <a
                                        href="<?php echo esc_url( WOOPQ_CHANGELOG ); ?>"
                                        target="_blank"><?php esc_html_e( 'Changelog', 'wpc-product-quantity' ); ?></a>
                                | <a href="<?php echo esc_url( WOOPQ_DISCUSSION ); ?>"
                                     target="_blank"><?php esc_html_e( 'Discussion', 'wpc-product-quantity' ); ?></a>
                            </p>
                        </div>
                        <div class="wpclever_settings_page_nav">
                            <h2 class="nav-tab-wrapper">
                                <a href="<?php echo admin_url( 'admin.php?page=wpclever-woopq&tab=settings' ); ?>"
                                   class="<?php echo esc_attr( $active_tab === 'settings' ? 'nav-tab nav-tab-active' : 'nav-tab' ); ?>">
									<?php esc_html_e( 'Settings', 'wpc-product-quantity' ); ?>
                                </a>
                                <a href="<?php echo admin_url( 'admin.php?page=wpclever-woopq&tab=premium' ); ?>"
                                   class="<?php echo esc_attr( $active_tab === 'premium' ? 'nav-tab nav-tab-active' : 'nav-tab' ); ?>"
                                   style="color: #c9356e">
									<?php esc_html_e( 'Premium Version', 'wpc-product-quantity' ); ?>
                                </a>
                                <a href="<?php echo admin_url( 'admin.php?page=wpclever-kit' ); ?>" class="nav-tab">
									<?php esc_html_e( 'Essential Kit', 'wpc-product-quantity' ); ?>
                                </a>
                            </h2>
                        </div>
                        <div class="wpclever_settings_page_content">
							<?php if ( $active_tab === 'settings' ) {
								$woopq_step = '1';

								if ( get_option( '_woopq_decimal', 'no' ) === 'yes' ) {
									$woopq_step = '0.000001';
								}
								?>
                                <form method="post" action="options.php">
									<?php wp_nonce_field( 'update-options' ); ?>
                                    <table class="form-table woopq-table">
                                        <tr class="heading">
                                            <th colspan="2">
												<?php esc_html_e( 'General', 'wpc-product-quantity' ); ?>
                                            </th>
                                        </tr>
                                        <tr>
                                            <th>
												<?php esc_html_e( 'Decimal quantities', 'wpc-product-quantity' ); ?>
                                            </th>
                                            <td>
                                                <select name="_woopq_decimal">
                                                    <option value="no" <?php echo esc_attr( get_option( '_woopq_decimal', 'no' ) === 'no' ? 'selected' : '' ); ?>><?php esc_html_e( 'No', 'wpc-product-quantity' ); ?></option>
                                                    <option value="yes" <?php echo esc_attr( get_option( '_woopq_decimal', 'no' ) === 'yes' ? 'selected' : '' ); ?>><?php esc_html_e( 'Yes', 'wpc-product-quantity' ); ?></option>
                                                </select>
                                                <span class="description"><?php esc_html_e( 'Press "Update Options" after enabling this option, then you can enter decimal quantities in min, max, step quantity options.', 'wpc-product-quantity' ); ?></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th><?php esc_html_e( 'Plus/minus button', 'wpc-product-quantity' ); ?></th>
                                            <td>
                                                <select name="_woopq_plus_minus">
                                                    <option value="show" <?php echo esc_attr( get_option( '_woopq_plus_minus', 'hide' ) === 'show' ? 'selected' : '' ); ?>><?php esc_html_e( 'Show', 'wpc-product-quantity' ); ?></option>
                                                    <option value="hide" <?php echo esc_attr( get_option( '_woopq_plus_minus', 'hide' ) === 'hide' ? 'selected' : '' ); ?>><?php esc_html_e( 'Hide', 'wpc-product-quantity' ); ?></option>
                                                </select>
                                                <span class="description"><?php esc_html_e( 'Show the plus/minus button for the input type to increase/decrease the quantity.', 'wpc-product-quantity' ); ?></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th><?php esc_html_e( 'Auto-correct', 'wpc-product-quantity' ); ?></th>
                                            <td>
                                                <select name="_woopq_auto_correct">
                                                    <option value="entering" <?php echo esc_attr( get_option( '_woopq_auto_correct', 'entering' ) === 'entering' ? 'selected' : '' ); ?>><?php esc_html_e( 'While entering', 'wpc-product-quantity' ); ?></option>
                                                    <option value="out_of_focus" <?php echo esc_attr( get_option( '_woopq_auto_correct', 'entering' ) === 'out_of_focus' ? 'selected' : '' ); ?>><?php esc_html_e( 'Out of focus', 'wpc-product-quantity' ); ?></option>
                                                </select>
                                                <span class="description"><?php esc_html_e( 'When the auto-correct functionality will be triggered: while entering the number or out of focus on the input (click outside).', 'wpc-product-quantity' ); ?></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th><?php esc_html_e( 'Rounding values', 'wpc-product-quantity' ); ?></th>
                                            <td>
                                                <select name="_woopq_rounding">
                                                    <option value="down" <?php echo esc_attr( get_option( '_woopq_rounding', 'down' ) === 'down' ? 'selected' : '' ); ?>><?php esc_html_e( 'Down', 'wpc-product-quantity' ); ?></option>
                                                    <option value="up" <?php echo esc_attr( get_option( '_woopq_rounding', 'down' ) === 'up' ? 'selected' : '' ); ?>><?php esc_html_e( 'Up', 'wpc-product-quantity' ); ?></option>
                                                </select>
                                                <span class="description"><?php esc_html_e( 'Round the quantity to the nearest bigger (up) or smaller (down) value when an invalid number is inputted.', 'wpc-product-quantity' ); ?></span>
                                            </td>
                                        </tr>
                                        <tr class="heading">
                                            <th colspan="2">
												<?php esc_html_e( 'Quantity', 'wpc-product-quantity' ); ?>
                                            </th>
                                        </tr>
                                        <tr>
                                            <th><?php esc_html_e( 'Type', 'wpc-product-quantity' ); ?></th>
                                            <td>
                                                <select name="_woopq_type">
                                                    <option value="default" <?php echo esc_attr( get_option( '_woopq_type', 'default' ) === 'default' ? 'selected' : '' ); ?>><?php esc_html_e( 'Input (Default)', 'wpc-product-quantity' ); ?></option>
                                                    <option value="select" <?php echo esc_attr( get_option( '_woopq_type', 'default' ) === 'select' ? 'selected' : '' ); ?>><?php esc_html_e( 'Select', 'wpc-product-quantity' ); ?></option>
                                                    <option value="radio" <?php echo esc_attr( get_option( '_woopq_type', 'default' ) === 'radio' ? 'selected' : '' ); ?>><?php esc_html_e( 'Radio', 'wpc-product-quantity' ); ?></option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr class="woopq_show_if_type woopq_show_if_type_select woopq_show_if_type_radio">
                                            <th><?php esc_html_e( 'Values', 'wpc-product-quantity' ); ?></th>
                                            <td>
                                                <textarea name="_woopq_values" rows="10"
                                                          cols="50"><?php echo get_option( '_woopq_values' ); ?></textarea>
                                                <p class="description"><?php esc_html_e( 'These values will be used for select/radio type. Enter each value in one line and can use the range e.g "10-20".', 'wpc-product-quantity' ); ?></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th><?php esc_html_e( 'Minimum', 'wpc-product-quantity' ); ?></th>
                                            <td>
                                                <input type="number" name="_woopq_min" min="0"
                                                       step="<?php echo esc_attr( $woopq_step ); ?>"
                                                       value="<?php echo get_option( '_woopq_min' ); ?>"/>
                                                <span class="description"><?php esc_html_e( 'Leave blank to disable.', 'wpc-product-quantity' ); ?></span>
                                            </td>
                                        </tr>
                                        <tr class="woopq_show_if_type woopq_show_if_type_default">
                                            <th><?php esc_html_e( 'Step', 'wpc-product-quantity' ); ?></th>
                                            <td>
                                                <input type="number" name="_woopq_step" min="0"
                                                       step="<?php echo esc_attr( $woopq_step ); ?>"
                                                       value="<?php echo get_option( '_woopq_step' ); ?>"/>
                                                <span class="description"><?php esc_html_e( 'Leave blank to disable.', 'wpc-product-quantity' ); ?></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th><?php esc_html_e( 'Maximum', 'wpc-product-quantity' ); ?></th>
                                            <td>
                                                <input type="number" name="_woopq_max" min="0"
                                                       step="<?php echo esc_attr( $woopq_step ); ?>"
                                                       value="<?php echo get_option( '_woopq_max' ); ?>"/>
                                                <span class="description"><?php esc_html_e( 'Leave blank to disable.', 'wpc-product-quantity' ); ?></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th><?php esc_html_e( 'Default value', 'wpc-product-quantity' ); ?></th>
                                            <td>
                                                <input type="number" name="_woopq_value" min="0"
                                                       step="<?php echo esc_attr( $woopq_step ); ?>"
                                                       value="<?php echo get_option( '_woopq_value', 1 ); ?>"/>
                                                <span class="description"><?php esc_html_e( 'Leave blank to disable.', 'wpc-product-quantity' ); ?></span>
                                            </td>
                                        </tr>
                                        <tr class="submit">
                                            <th colspan="2">
                                                <input type="submit" name="submit" class="button button-primary"
                                                       value="<?php esc_html_e( 'Update Options', 'wpc-product-quantity' ); ?>"/>
                                                <input type="hidden" name="action" value="update"/>
                                                <input type="hidden" name="page_options"
                                                       value="_woopq_decimal,_woopq_type,_woopq_plus_minus,_woopq_auto_correct,_woopq_rounding,_woopq_min,_woopq_step,_woopq_max,_woopq_value,_woopq_values"/>
                                            </th>
                                        </tr>
                                    </table>
                                </form>
							<?php } elseif ( $active_tab === 'premium' ) { ?>
                                <div class="wpclever_settings_page_content_text">
                                    <p>
                                        Get the Premium Version just $29! <a
                                                href="https://wpclever.net/downloads/product-quantity?utm_source=pro&utm_medium=woopq&utm_campaign=wporg"
                                                target="_blank">https://wpclever.net/downloads/product-quantity</a>
                                    </p>
                                    <p><strong>Extra features for Premium Version:</strong></p>
                                    <ul style="margin-bottom: 0">
                                        <li>- Add quantity settings at product and variation basis.</li>
                                        <li>- Get the lifetime update & premium support.</li>
                                    </ul>
                                </div>
							<?php } ?>
                        </div>
                    </div>
					<?php
				}

				function action_links( $links, $file ) {
					static $plugin;

					if ( ! isset( $plugin ) ) {
						$plugin = plugin_basename( __FILE__ );
					}

					if ( $plugin === $file ) {
						$settings             = '<a href="' . admin_url( 'admin.php?page=wpclever-woopq&tab=settings' ) . '">' . esc_html__( 'Settings', 'wpc-product-quantity' ) . '</a>';
						$links['wpc-premium'] = '<a href="' . admin_url( 'admin.php?page=wpclever-woopq&tab=premium' ) . '">' . esc_html__( 'Premium Version', 'wpc-product-quantity' ) . '</a>';
						array_unshift( $links, $settings );
					}

					return (array) $links;
				}

				function row_meta( $links, $file ) {
					static $plugin;

					if ( ! isset( $plugin ) ) {
						$plugin = plugin_basename( __FILE__ );
					}

					if ( $plugin === $file ) {
						$row_meta = array(
							'support' => '<a href="' . esc_url( WOOPQ_DISCUSSION ) . '" target="_blank">' . esc_html__( 'Community support', 'wpc-product-quantity' ) . '</a>',
						);

						return array_merge( $links, $row_meta );
					}

					return (array) $links;
				}

				function loop_add_to_cart_args( $args, $product ) {
					if ( $product ) {
						$woopq_value = self::get_value( $product );
						$woopq_min   = self::get_min( $product );

						if ( ! empty( $woopq_min ) && ( $woopq_value < $woopq_min ) ) {
							$args['quantity'] = $woopq_min;
						} else {
							$args['quantity'] = $woopq_value;
						}
					}

					return $args;
				}

				function quantity_input_args( $args, $product ) {
					if ( $product ) {
						$args['product_id'] = $product->get_id();
						$args['min_value']  = self::get_min( $product, $args['min_value'] );
						$args['max_value']  = self::get_max( $product, $args['max_value'] );
						$args['step']       = self::get_step( $product, $args['step'] );

						if ( substr( $args['input_name'], 0, 8 ) === 'quantity' ) {
							// check if isn't in the cart
							$args['input_value'] = self::get_value( $product, $args['input_value'] );
						}
					}

					return $args;
				}

				function quantity_input_min( $min, $product ) {
					if ( $product ) {
						return self::get_min( $product, $min );
					}

					return $min;
				}

				function quantity_input_max( $max, $product ) {
					if ( $product ) {
						return self::get_max( $product, $max );
					}

					return $max;
				}

				function quantity_input_step( $step, $product ) {
					if ( $product ) {
						return self::get_step( $product, $step );
					}

					return $step;
				}

				function get_quantity( $_product, $is_variation = false ) {
					if ( is_numeric( $_product ) ) {
						$product_id = $_product;
						$_product   = wc_get_product( $product_id );
					} else {
						$product_id = $_product->get_id();
					}

					if ( $is_variation || $_product->is_type( 'variation' ) ) {
						return apply_filters( 'woopq_product_quantity', get_post_meta( $product_id, '_woopq_quantity', true ) ?: 'parent', $product_id );
					}

					return apply_filters( 'woopq_product_quantity', get_post_meta( $product_id, '_woopq_quantity', true ) ?: 'default', $product_id );
				}

				function get_type( $product_id ) {
					return apply_filters( 'woopq_product_type', get_post_meta( $product_id, '_woopq_type', true ) ?: 'default', $product_id );
				}

				function get_min( $product, $min = 0 ) {
					$product_id = $product->get_id();
					$quantity   = self::get_quantity( $product );

					switch ( $quantity ) {
						case 'disable':
							$woopq_min = $min;

							break;
						case 'default':
							$woopq_min = get_option( '_woopq_min' );

							break;
						case 'parent':
							if ( $product->is_type( 'variation' ) && ( $parent_id = $product->get_parent_id() ) ) {
								$parent    = wc_get_product( $parent_id );
								$woopq_min = self::get_min( $parent );
							}

							break;
						default:
							$woopq_min = get_post_meta( $product_id, '_woopq_min', true );

							break;
					}

					if ( ! is_numeric( $woopq_min ) ) {
						// leave blank to disable
						$woopq_min = $min;
					}

					$woopq_min = (float) $woopq_min;

					if ( get_option( '_woopq_decimal', 'no' ) !== 'yes' ) {
						$woopq_min = ceil( $woopq_min );
					}

					return apply_filters( 'woopq_product_min', $woopq_min, $product_id, $product );
				}

				function get_max( $product, $max = 100000 ) {
					$product_id = $product->get_id();
					$quantity   = self::get_quantity( $product );
					$max_value  = $product->get_max_purchase_quantity();

					switch ( $quantity ) {
						case 'disable':
							$woopq_max = $max;

							break;
						case 'default':
							$woopq_max = get_option( '_woopq_max' );

							break;
						case 'parent':
							if ( $product->is_type( 'variation' ) && ( $parent_id = $product->get_parent_id() ) ) {
								$parent    = wc_get_product( $parent_id );
								$woopq_max = self::get_max( $parent );
							}

							break;
						default:
							$woopq_max = get_post_meta( $product_id, '_woopq_max', true );

							break;
					}

					if ( ! is_numeric( $woopq_max ) ) {
						// leave blank to disable
						$woopq_max = $max;
					}

					$woopq_max = (float) $woopq_max;

					if ( ( $max_value > 0 ) && ( $woopq_max > $max_value ) ) {
						$woopq_max = $max_value;
					}

					if ( get_option( '_woopq_decimal', 'no' ) !== 'yes' ) {
						$woopq_max = ceil( $woopq_max );
					}

					return apply_filters( 'woopq_product_max', $woopq_max, $product_id, $product );
				}

				function get_step( $product, $step = 1 ) {
					$product_id = $product->get_id();
					$quantity   = self::get_quantity( $product );

					switch ( $quantity ) {
						case 'disable':
							$woopq_step = $step;

							break;
						case 'default':
							$woopq_step = get_option( '_woopq_step' );

							break;
						case 'parent':
							if ( $product->is_type( 'variation' ) && ( $parent_id = $product->get_parent_id() ) ) {
								$parent     = wc_get_product( $parent_id );
								$woopq_step = self::get_step( $parent );
							}

							break;
						default:
							$woopq_step = get_post_meta( $product_id, '_woopq_step', true );

							break;
					}

					if ( ! is_numeric( $woopq_step ) ) {
						// leave blank to disable
						$woopq_step = $step;
					}

					$woopq_step = (float) $woopq_step;

					if ( get_option( '_woopq_decimal', 'no' ) !== 'yes' ) {
						$woopq_step = ceil( $woopq_step );
					}

					return apply_filters( 'woopq_product_step', $woopq_step, $product_id, $product );
				}

				function get_value( $product, $value = 1 ) {
					$product_id = $product->get_id();
					$quantity   = self::get_quantity( $product );

					switch ( $quantity ) {
						case 'disable':
							$woopq_value = $value;

							break;
						case 'default':
							$woopq_value = get_option( '_woopq_value' );

							break;
						case 'parent':
							if ( $product->is_type( 'variation' ) && ( $parent_id = $product->get_parent_id() ) ) {
								$parent      = wc_get_product( $parent_id );
								$woopq_value = self::get_value( $parent );
							}

							break;
						default:
							$woopq_value = get_post_meta( $product_id, '_woopq_value', true );

							break;
					}

					if ( ! is_numeric( $woopq_value ) ) {
						// leave blank to disable
						$woopq_value = $value;
					}

					$woopq_value = (float) $woopq_value;

					if ( get_option( '_woopq_decimal', 'no' ) !== 'yes' ) {
						$woopq_value = ceil( $woopq_value );
					}

					return apply_filters( 'woopq_product_value', $woopq_value, $product_id, $product );
				}

				function quantity_input_template( $located, $template_name ) {
					if ( $template_name === 'global/quantity-input.php' ) {
						return WOOPQ_DIR . 'templates/quantity-input.php';
					}

					return $located;
				}

				function product_data_tabs( $tabs ) {
					$tabs['woopq'] = array(
						'label'  => esc_html__( 'Quantity', 'wpc-product-quantity' ),
						'target' => 'woopq_settings',
					);

					return $tabs;
				}

				function product_data_panels() {
					global $post;
					$post_id = $post->ID;

					self::product_settings( $post_id );
				}

				function product_settings( $post_id, $is_variation = false ) {
					$woopq_step = '1';

					if ( get_option( '_woopq_decimal', 'no' ) === 'yes' ) {
						$woopq_step = '0.000001';
					}

					$quantity = self::get_quantity( $post_id, $is_variation );
					$type     = self::get_type( $post_id );

					$name  = '';
					$id    = 'woopq_settings';
					$class = 'woopq_table woopq-table panel woocommerce_options_panel';

					if ( $is_variation ) {
						$name  = '_v[' . $post_id . ']';
						$id    = '';
						$class = 'woopq_table woopq-table';
					}
					?>
                    <div id="<?php echo esc_attr( $id ); ?>" class="<?php echo esc_attr( $class ); ?>">
                        <div class="woopq_tr">
                            <div class="woopq_td"><?php esc_html_e( 'Quantity', 'wpc-product-quantity' ); ?></div>
                            <div class="woopq_td">
                                <div class="woopq_quantity_active">
                                    <input name="<?php echo esc_attr( '_woopq_quantity' . $name ); ?>" type="radio"
                                           value="default" <?php echo esc_attr( $quantity === 'default' ? 'checked' : '' ); ?>/> <?php esc_html_e( 'Default', 'wpc-product-quantity' ); ?>
                                    (<a
                                            href="<?php echo admin_url( 'admin.php?page=wpclever-woopq&tab=settings' ); ?>"
                                            target="_blank"><?php esc_html_e( 'settings', 'wpc-product-quantity' ); ?></a>)
                                </div>
								<?php if ( $is_variation ) { ?>
                                    <div class="woopq_quantity_active">
                                        <input name="<?php echo esc_attr( '_woopq_quantity' . $name ); ?>" type="radio"
                                               value="parent" <?php echo esc_attr( $quantity === 'parent' ? 'checked' : '' ); ?>/> <?php esc_html_e( 'Parent', 'wpc-product-quantity' ); ?>
                                    </div>
								<?php } ?>
                                <div class="woopq_quantity_active">
                                    <input name="<?php echo esc_attr( '_woopq_quantity' . $name ); ?>" type="radio"
                                           value="disable" <?php echo esc_attr( $quantity === 'disable' ? 'checked' : '' ); ?>/> <?php esc_html_e( 'Disable', 'wpc-product-quantity' ); ?>
                                </div>
                                <div class="woopq_quantity_active">
                                    <input name="<?php echo esc_attr( '_woopq_quantity' . $name ); ?>" type="radio"
                                           value="overwrite" <?php echo esc_attr( $quantity === 'overwrite' ? 'checked' : '' ); ?>/> <?php esc_html_e( 'Overwrite', 'wpc-product-quantity' ); ?>
                                </div>
                                <div style="color: #c9356e; padding-left: 0; padding-right: 0; margin-top: 10px">You
                                    only can use the <a
                                            href="<?php echo admin_url( 'admin.php?page=wpclever-woopq&tab=settings' ); ?>"
                                            target="_blank">default settings</a> for all products and variations.<br/>Quantity
                                    settings
                                    at a product or variation
                                    basis only available on the Premium
                                    Version. <a
                                            href="https://wpclever.net/downloads/product-quantity?utm_source=pro&utm_medium=woopq&utm_campaign=wporg"
                                            target="_blank">Click here</a> to buy, just $29!
                                </div>
                            </div>
                        </div>
                        <div class="woopq_show_if_overwrite">
                            <div class="woopq_tr">
                                <div class="woopq_td"><?php esc_html_e( 'Type', 'wpc-product-quantity' ); ?></div>
                                <div class="woopq_td">
                                    <select name="<?php echo esc_attr( '_woopq_type' . $name ); ?>">
                                        <option value="default" <?php echo esc_attr( $type === 'default' ? 'selected' : '' ); ?>><?php esc_html_e( 'Input (Default)', 'wpc-product-quantity' ); ?></option>
                                        <option value="select" <?php echo esc_attr( $type === 'select' ? 'selected' : '' ); ?>><?php esc_html_e( 'Select', 'wpc-product-quantity' ); ?></option>
                                        <option value="radio" <?php echo esc_attr( $type === 'radio' ? 'selected' : '' ); ?>><?php esc_html_e( 'Radio', 'wpc-product-quantity' ); ?></option>
                                    </select>
                                </div>
                            </div>
                            <div class="woopq_tr woopq_show_if_type woopq_show_if_type_select woopq_show_if_type_radio">
                                <div class="woopq_td"><?php esc_html_e( 'Values', 'wpc-product-quantity' ); ?></div>
                                <div class="woopq_td">
                                                <textarea name="<?php echo esc_attr( '_woopq_values' . $name ); ?>"
                                                          rows="10"
                                                          cols="50"
                                                          style="float: none; width: 100%; height: 200px"><?php echo get_post_meta( $post_id, '_woopq_values', true ); ?></textarea>
                                    <p class="description"
                                       style="margin-left: 0"><?php esc_html_e( 'These values will be used for select/radio type. Enter each value in one line and can use the range e.g "10-20".', 'wpc-product-quantity' ); ?></p>
                                </div>
                            </div>
                            <div class="woopq_tr">
                                <div class="woopq_td"><?php esc_html_e( 'Minimum', 'wpc-product-quantity' ); ?></div>
                                <div class="woopq_td">
                                    <input type="number" name="<?php echo esc_attr( '_woopq_min' . $name ); ?>" min="0"
                                           step="<?php echo esc_attr( $woopq_step ); ?>"
                                           value="<?php echo get_post_meta( $post_id, '_woopq_min', true ); ?>"
                                           style="width: 120px"/>
                                    <span class="description"><?php esc_html_e( 'Leave blank to disable.', 'wpc-product-quantity' ); ?></span>
                                </div>
                            </div>
                            <div class="woopq_tr woopq_show_if_type woopq_show_if_type_default">
                                <div class="woopq_td"><?php esc_html_e( 'Step', 'wpc-product-quantity' ); ?></div>
                                <div class="woopq_td">
                                    <input type="number" name="<?php echo esc_attr( '_woopq_step' . $name ); ?>" min="0"
                                           step="<?php echo esc_attr( $woopq_step ); ?>"
                                           value="<?php echo get_post_meta( $post_id, '_woopq_step', true ); ?>"
                                           style="width: 120px"/>
                                    <span class="description"><?php esc_html_e( 'Leave blank to disable.', 'wpc-product-quantity' ); ?></span>
                                </div>
                            </div>
                            <div class="woopq_tr">
                                <div class="woopq_td"><?php esc_html_e( 'Maximum', 'wpc-product-quantity' ); ?></div>
                                <div class="woopq_td">
                                    <input type="number" name="<?php echo esc_attr( '_woopq_max' . $name ); ?>" min="0"
                                           step="<?php echo esc_attr( $woopq_step ); ?>"
                                           value="<?php echo get_post_meta( $post_id, '_woopq_max', true ); ?>"
                                           style="width: 120px"/>
                                    <span class="description"><?php esc_html_e( 'Leave blank to disable.', 'wpc-product-quantity' ); ?></span>
                                </div>
                            </div>
                            <div class="woopq_tr">
                                <div class="woopq_td"><?php esc_html_e( 'Default value', 'wpc-product-quantity' ); ?></div>
                                <div class="woopq_td">
                                    <input type="number" name="<?php echo esc_attr( '_woopq_value' . $name ); ?>"
                                           min="0"
                                           step="<?php echo esc_attr( $woopq_step ); ?>"
                                           value="<?php echo get_post_meta( $post_id, '_woopq_value', true ); ?>"
                                           style="width: 120px"/>
                                    <span class="description"><?php esc_html_e( 'Leave blank to disable.', 'wpc-product-quantity' ); ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
					<?php
				}

				function add_to_cart_validation( $passed, $product_id, $qty ) {
					$_product = wc_get_product( $product_id );
					$max      = self::get_max( $_product );
					$added    = self::qty_in_cart( $product_id );

					if ( ( $max > 0 ) && ( $qty + $added ) > $max ) {
						wc_add_notice( sprintf( esc_html__( 'You can\'t add more than %s "%s" to the cart.', 'wpc-product-quantity' ), $max, esc_html( get_the_title( $product_id ) ) ), 'error' );

						return false;
					}

					return $passed;
				}

				function qty_in_cart( $product_id ) {
					$qty = 0;

					foreach ( WC()->cart->get_cart() as $cart_item ) {
						if ( $cart_item['product_id'] === $product_id ) {
							$qty += $cart_item['quantity'];
						}
					}

					return $qty;
				}

				function add_to_cart_message_html( $message, $products, $show_qty ) {
					$titles = array();
					$count  = 0;

					if ( ! is_array( $products ) ) {
						$products = array( $products => 1 );
						$show_qty = false;
					}

					if ( ! $show_qty ) {
						$products = array_fill_keys( array_keys( $products ), 1 );
					}

					foreach ( $products as $product_id => $qty ) {
						/* translators: %s: product name */
						$titles[] = apply_filters( 'woocommerce_add_to_cart_qty_html', ( $qty > 1 ? (float) $qty . ' &times; ' : '' ), $product_id ) . apply_filters( 'woocommerce_add_to_cart_item_name_in_quotes', sprintf( _x( '&ldquo;%s&rdquo;', 'Item name in quotes', 'wpc-product-quantity' ), strip_tags( get_the_title( $product_id ) ) ), $product_id );
						$count    += $qty;
					}

					$titles = array_filter( $titles );
					/* translators: %s: product name */
					$added_text = sprintf( _n( '%s has been added to your cart.', '%s have been added to your cart.', $count, 'wpc-product-quantity' ), wc_format_list_of_items( $titles ) );

					// Output success messages.
					if ( 'yes' === get_option( 'woocommerce_cart_redirect_after_add' ) ) {
						$return_to = apply_filters( 'woocommerce_continue_shopping_redirect', wc_get_raw_referer() ? wp_validate_redirect( wc_get_raw_referer(), false ) : wc_get_page_permalink( 'shop' ) );
						$message   = sprintf( '<a href="%s" tabindex="1" class="button wc-forward">%s</a> %s', esc_url( $return_to ), esc_html__( 'Continue shopping', 'wpc-product-quantity' ), esc_html( $added_text ) );
					} else {
						$message = sprintf( '<a href="%s" tabindex="1" class="button wc-forward">%s</a> %s', esc_url( wc_get_cart_url() ), esc_html__( 'View cart', 'wpc-product-quantity' ), esc_html( $added_text ) );
					}

					return $message;
				}

				function variation_settings( $loop, $variation_data, $variation ) {
					$variation_id = absint( $variation->ID );
					?>
                    <div class="form-row form-row-full woopq-variation-settings">
                        <label><?php esc_html_e( 'WPC Product Quantity', 'wpc-product-quantity' ); ?></label>
                        <div class="woopq-variation-wrap woopq-variation-wrap-<?php echo esc_attr( $variation_id ); ?>">
							<?php self::product_settings( $variation_id, true ); ?>
                        </div>
                    </div>
					<?php
				}

				public static function get_values( $values ) {
					$woopq_values  = array();
					$woopq_decimal = get_option( '_woopq_decimal', 'no' );
					$values_arr    = explode( "\n", $values );

					if ( count( $values_arr ) > 0 ) {
						foreach ( $values_arr as $item ) {
							$item_value = self::clean_values( $item );

							if ( strpos( $item_value, '-' ) ) {
								$item_value_arr = explode( '-', $item_value );

								for ( $i = (int) $item_value_arr[0]; $i <= (int) $item_value_arr[1]; $i ++ ) {
									$woopq_values[] = array( 'name' => $i, 'value' => $i );
								}
							} elseif ( is_numeric( $item_value ) ) {
								if ( $woopq_decimal !== 'yes' ) {
									$woopq_values[] = array(
										'name'  => esc_html( trim( $item ) ),
										'value' => (int) $item_value
									);
								} else {
									$woopq_values[] = array(
										'name'  => esc_html( trim( $item ) ),
										'value' => (float) $item_value
									);
								}
							}
						}
					}

					if ( empty( $woopq_values ) ) {
						// default values
						$woopq_values = array(
							array( 'name' => '1', 'value' => 1 ),
							array( 'name' => '2', 'value' => 2 ),
							array( 'name' => '3', 'value' => 3 ),
							array( 'name' => '4', 'value' => 4 ),
							array( 'name' => '5', 'value' => 5 ),
							array( 'name' => '6', 'value' => 6 ),
							array( 'name' => '7', 'value' => 7 ),
							array( 'name' => '8', 'value' => 8 ),
							array( 'name' => '9', 'value' => 9 ),
							array( 'name' => '10', 'value' => 10 )
						);
					} else {
						$woopq_values = array_intersect_key( $woopq_values, array_unique( array_map( 'serialize', $woopq_values ) ) );
					}

					return apply_filters( 'woopq_values', $woopq_values );
				}

				public static function clean_values( $str ) {
					return preg_replace( '/[^.\-0-9]/', '', $str );
				}
			}

			new WPCleverWoopq();
		}
	}
}

if ( ! function_exists( 'woopq_notice_wc' ) ) {
	function woopq_notice_wc() {
		?>
        <div class="error">
            <p><strong>WPC Product Quantity</strong> requires WooCommerce version 3.0 or greater.</p>
        </div>
		<?php
	}
}
