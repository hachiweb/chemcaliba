/******/ (function() { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./node_modules/@wordpress/icons/build-module/library/chevron-left.js":
/*!****************************************************************************!*\
  !*** ./node_modules/@wordpress/icons/build-module/library/chevron-left.js ***!
  \****************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _wordpress_primitives__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @wordpress/primitives */ "@wordpress/primitives");
/* harmony import */ var _wordpress_primitives__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_wordpress_primitives__WEBPACK_IMPORTED_MODULE_1__);


/**
 * WordPress dependencies
 */

const chevronLeft = (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_wordpress_primitives__WEBPACK_IMPORTED_MODULE_1__.SVG, {
  xmlns: "http://www.w3.org/2000/svg",
  viewBox: "0 0 24 24"
}, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_wordpress_primitives__WEBPACK_IMPORTED_MODULE_1__.Path, {
  d: "M14.6 7l-1.2-1L8 12l5.4 6 1.2-1-4.6-5z"
}));
/* harmony default export */ __webpack_exports__["default"] = (chevronLeft);
//# sourceMappingURL=chevron-left.js.map

/***/ }),

/***/ "./node_modules/@wordpress/icons/build-module/library/chevron-right.js":
/*!*****************************************************************************!*\
  !*** ./node_modules/@wordpress/icons/build-module/library/chevron-right.js ***!
  \*****************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _wordpress_primitives__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @wordpress/primitives */ "@wordpress/primitives");
/* harmony import */ var _wordpress_primitives__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_wordpress_primitives__WEBPACK_IMPORTED_MODULE_1__);


/**
 * WordPress dependencies
 */

const chevronRight = (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_wordpress_primitives__WEBPACK_IMPORTED_MODULE_1__.SVG, {
  xmlns: "http://www.w3.org/2000/svg",
  viewBox: "0 0 24 24"
}, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_wordpress_primitives__WEBPACK_IMPORTED_MODULE_1__.Path, {
  d: "M10.6 6L9.4 7l4.6 5-4.6 5 1.2 1 5.4-6z"
}));
/* harmony default export */ __webpack_exports__["default"] = (chevronRight);
//# sourceMappingURL=chevron-right.js.map

/***/ }),

/***/ "./node_modules/@wordpress/icons/build-module/library/close.js":
/*!*********************************************************************!*\
  !*** ./node_modules/@wordpress/icons/build-module/library/close.js ***!
  \*********************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _wordpress_primitives__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @wordpress/primitives */ "@wordpress/primitives");
/* harmony import */ var _wordpress_primitives__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_wordpress_primitives__WEBPACK_IMPORTED_MODULE_1__);


/**
 * WordPress dependencies
 */

const close = (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_wordpress_primitives__WEBPACK_IMPORTED_MODULE_1__.SVG, {
  xmlns: "http://www.w3.org/2000/svg",
  viewBox: "0 0 24 24"
}, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_wordpress_primitives__WEBPACK_IMPORTED_MODULE_1__.Path, {
  d: "M13 11.8l6.1-6.3-1-1-6.1 6.2-6.1-6.2-1 1 6.1 6.3-6.5 6.7 1 1 6.5-6.6 6.5 6.6 1-1z"
}));
/* harmony default export */ __webpack_exports__["default"] = (close);
//# sourceMappingURL=close.js.map

/***/ }),

/***/ "./src/common/components/eid-wrapper.js":
/*!**********************************************!*\
  !*** ./src/common/components/eid-wrapper.js ***!
  \**********************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "EID_PREFIX": function() { return /* binding */ EID_PREFIX; },
/* harmony export */   "EidWrapper": function() { return /* binding */ EidWrapper; }
/* harmony export */ });
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__);
/** global wp */

/**
 * WordPress dependencies
 */

const EID_PREFIX = 'wp.' + (!!wp.editPost ? 'editor' : 'wpadmin');
/**
 * Return an element wrapped with Data-EID properties.
 *
 * @param {Object} obj
 * @param {Object} obj.children
 * @param {string} obj.section
 * @param {string} obj.target
 * @param {string} obj.action
 */

const EidWrapper = _ref => {
  let {
    children,
    section,
    target,
    action
  } = _ref;

  if (!(0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.isValidElement)(children)) {
    return children;
  }

  return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.cloneElement)(children, { ...children.props,
    'data-eid': `${EID_PREFIX}.${section}.${target}.${action}`
  });
};

/***/ }),

/***/ "./src/common/components/gdl-tool-tip.js":
/*!***********************************************!*\
  !*** ./src/common/components/gdl-tool-tip.js ***!
  \***********************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* binding */ GoDaddyLaunchTooltip; }
/* harmony export */ });
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @wordpress/i18n */ "@wordpress/i18n");
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _wordpress_icons__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @wordpress/icons */ "./node_modules/@wordpress/icons/build-module/library/close.js");
/* harmony import */ var _wordpress_components__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @wordpress/components */ "@wordpress/components");
/* harmony import */ var _wordpress_components__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_wordpress_components__WEBPACK_IMPORTED_MODULE_2__);


/**
 * WordPress dependencies
 */



function GoDaddyLaunchTooltip(props) {
  const {
    tooltip,
    closeCallback
  } = props;
  return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_wordpress_components__WEBPACK_IMPORTED_MODULE_2__.Popover, {
    anchorRect: tooltip.anchorRect,
    className: `publish-guide-tooltip godaddy-styles${!tooltip.nextTooltip ? ' publish-guide-tooltip--next' : ''}`,
    noArrow: false,
    position: "middle left"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("p", {
    className: "publish-guide-tooltip__label"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("svg", {
    fill: "none",
    height: "16",
    viewBox: "0 0 18 16",
    width: "18",
    xmlns: "http://www.w3.org/2000/svg"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("path", {
    d: "M15.5266 0.730524C13.6559 -0.433974 11.1932 -0.159034 8.99681 1.21948C6.80726 -0.158272 4.34306 -0.433974 2.47469 0.730524C-0.48083 2.57438 -0.840161 7.32376 1.67287 11.3374C3.52521 14.2978 6.42199 16.0312 9.00063 15.9992C11.5793 16.0312 14.476 14.2978 16.3284 11.3374C18.8384 7.32376 18.4821 2.57438 15.5266 0.730524ZM3.03161 10.4905C2.51329 9.67028 2.12232 8.77644 1.87199 7.83937C1.6494 7.02621 1.56431 6.18166 1.62023 5.34054C1.73237 3.85769 2.33736 2.70233 3.32304 2.08695C4.30872 1.47157 5.61178 1.43577 6.99951 1.98489C7.21046 2.06958 7.41696 2.1649 7.61823 2.2705C6.8469 2.972 6.17632 3.7765 5.62551 4.66118C4.09969 7.09833 3.63431 9.81117 4.16682 11.9726C3.74261 11.5153 3.36252 11.0191 3.03161 10.4905ZM16.13 7.83937C15.8794 8.77632 15.4884 9.67012 14.9704 10.4905C14.6396 11.0201 14.2595 11.5174 13.8352 11.9757C14.3112 10.0374 13.9878 7.66344 12.8205 5.4365C12.802 5.39967 12.776 5.36715 12.7441 5.34104C12.7122 5.31494 12.6751 5.29584 12.6353 5.28499C12.5955 5.27414 12.5538 5.27178 12.5131 5.27807C12.4723 5.28436 12.4333 5.29916 12.3986 5.3215L8.75955 7.59185C8.72638 7.61245 8.69761 7.63939 8.6749 7.67112C8.65218 7.70284 8.63597 7.73873 8.62718 7.77673C8.61839 7.81473 8.6172 7.85408 8.62368 7.89254C8.63016 7.931 8.64419 7.9678 8.66494 8.00083L9.19898 8.85307C9.21962 8.88618 9.2466 8.9149 9.27839 8.93758C9.31017 8.96025 9.34612 8.97644 9.38418 8.98521C9.42224 8.99399 9.46167 8.99517 9.50019 8.9887C9.53871 8.98223 9.57557 8.96824 9.60867 8.94751L11.9676 7.47685C12.0439 7.70533 12.1202 7.93 12.1774 8.16229C12.3999 8.97547 12.485 9.82 12.4291 10.6611C12.317 12.1447 11.712 13.3001 10.7263 13.9155C10.2179 14.2258 9.63537 14.3941 9.03953 14.4029H8.96324C8.36738 14.3943 7.78478 14.226 7.27645 13.9155C6.29 13.3001 5.68502 12.1447 5.57287 10.6611C5.51741 9.82001 5.60249 8.97554 5.82463 8.16229C6.33618 6.28117 7.39305 4.59284 8.86254 3.30933C9.49632 2.75373 10.2182 2.30718 10.9987 1.98794C12.3826 1.43882 13.688 1.47461 14.6744 2.08999C15.6608 2.70537 16.2651 3.86073 16.3772 5.34358C16.4339 6.18344 16.3504 7.02686 16.13 7.83937Z",
    fill: "#2B2B2B"
  })), (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__.__)('Get Started', 'godaddy-launch')), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("h3", {
    className: "publish-guide-tooltip__title"
  }, tooltip.title), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("div", {
    className: "publish-guide-tooltip__description"
  }, tooltip.description), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_wordpress_components__WEBPACK_IMPORTED_MODULE_2__.Icon, {
    className: "publish-guide-tooltip__close",
    icon: _wordpress_icons__WEBPACK_IMPORTED_MODULE_3__["default"],
    onClick: closeCallback
  }));
}

/***/ }),

/***/ "./src/publish-guide/hooks.js":
/*!************************************!*\
  !*** ./src/publish-guide/hooks.js ***!
  \************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _wordpress_hooks__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/hooks */ "@wordpress/hooks");
/* harmony import */ var _wordpress_hooks__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_hooks__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _wordpress_media_utils__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @wordpress/media-utils */ "@wordpress/media-utils");
/* harmony import */ var _wordpress_media_utils__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_wordpress_media_utils__WEBPACK_IMPORTED_MODULE_1__);
/**
 * WordPress dependencies
 */


(0,_wordpress_hooks__WEBPACK_IMPORTED_MODULE_0__.addFilter)('editor.MediaUpload', 'godaddy-launch/publish-guide/replace-media-upload', () => _wordpress_media_utils__WEBPACK_IMPORTED_MODULE_1__.MediaUpload);

/***/ }),

/***/ "./src/publish-guide/publish-guide-header-button.js":
/*!**********************************************************!*\
  !*** ./src/publish-guide/publish-guide-header-button.js ***!
  \**********************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @wordpress/i18n */ "@wordpress/i18n");
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _wordpress_components__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @wordpress/components */ "@wordpress/components");
/* harmony import */ var _wordpress_components__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_wordpress_components__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _common_components_eid_wrapper__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../common/components/eid-wrapper */ "./src/common/components/eid-wrapper.js");


/**
 * External dependencies
 */

/**
 * WordPress dependencies
 */



/**
 * Internal dependencies
 */



const PublishGuideHeaderButton = _ref => {
  let {
    isOpen,
    onTogglePublishGuideOpen
  } = _ref;
  return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_common_components_eid_wrapper__WEBPACK_IMPORTED_MODULE_4__.EidWrapper, {
    action: "click",
    section: "guide",
    target: isOpen ? 'close' : 'open'
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_wordpress_components__WEBPACK_IMPORTED_MODULE_3__.Button, {
    "aria-label": (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_2__.__)('Publish Guide', 'godaddy-launch'),
    className: classnames__WEBPACK_IMPORTED_MODULE_1___default()('publish-guide-trigger__button', {
      'is-opened': isOpen
    }),
    id: "gdl-publish-guide-trigger-button",
    isPrimary: true,
    onClick: onTogglePublishGuideOpen
  }, (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_2__.__)('Get Started', 'godaddy-launch')));
};

/* harmony default export */ __webpack_exports__["default"] = (PublishGuideHeaderButton);

/***/ }),

/***/ "./src/publish-guide/publish-guide-item.js":
/*!*************************************************!*\
  !*** ./src/publish-guide/publish-guide-item.js ***!
  \*************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* binding */ PublishGuideItem; }
/* harmony export */ });
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _common_components_eid_wrapper__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../common/components/eid-wrapper */ "./src/common/components/eid-wrapper.js");


/**
 * External dependencies
 */

/**
 * Internal dependencies
 */


function PublishGuideItem(props) {
  const {
    children,
    isCompleted,
    name,
    selectedItem,
    setSelectedItem,
    title,
    text
  } = props;
  return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("li", {
    className: classnames__WEBPACK_IMPORTED_MODULE_1___default()('publish-guide-popover__item', {
      'is-completed': isCompleted,
      'is-selected': selectedItem === name
    })
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("div", {
    className: "publish-guide-popover__item__content"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_common_components_eid_wrapper__WEBPACK_IMPORTED_MODULE_2__.EidWrapper, {
    action: "click",
    section: `guide/item/${name}`,
    target: "panel"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("button", {
    className: "publish-guide-popover__item__title",
    onClick: () => !isCompleted && setSelectedItem(name)
  }, title)), selectedItem === name && !isCompleted && (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("p", null, text), children)));
}

/***/ }),

/***/ "./src/publish-guide/publish-guide-items/add-domain.js":
/*!*************************************************************!*\
  !*** ./src/publish-guide/publish-guide-items/add-domain.js ***!
  \*************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/extends */ "./node_modules/@babel/runtime/helpers/esm/extends.js");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @wordpress/i18n */ "@wordpress/i18n");
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _wordpress_icons__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @wordpress/icons */ "./node_modules/@wordpress/icons/build-module/library/chevron-right.js");
/* harmony import */ var _wordpress_data__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @wordpress/data */ "@wordpress/data");
/* harmony import */ var _wordpress_data__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_wordpress_data__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _wordpress_core_data__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @wordpress/core-data */ "@wordpress/core-data");
/* harmony import */ var _wordpress_core_data__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_wordpress_core_data__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _wordpress_components__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @wordpress/components */ "@wordpress/components");
/* harmony import */ var _wordpress_components__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_wordpress_components__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _publish_guide_item__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../publish-guide-item */ "./src/publish-guide/publish-guide-item.js");
/* harmony import */ var _common_components_eid_wrapper__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../common/components/eid-wrapper */ "./src/common/components/eid-wrapper.js");



/* global gdlPublishGuideItems, gdvLinks */

/**
 * WordPress dependencies
 */





/**
 * Internal dependencies
 */




const AddDomainGuide = props => {
  var _gdvLinks;

  const changeDomainUrl = (_gdvLinks = gdvLinks) === null || _gdvLinks === void 0 ? void 0 : _gdvLinks.changeDomain;
  const optionName = gdlPublishGuideItems.AddDomain.propName;
  const [addDomainComplete = gdlPublishGuideItems.AddDomain.default, setAddDomainComplete] = (0,_wordpress_core_data__WEBPACK_IMPORTED_MODULE_4__.useEntityProp)('root', 'site', optionName);

  const setIsCompleted = newValue => {
    setAddDomainComplete(newValue);
    (0,_wordpress_data__WEBPACK_IMPORTED_MODULE_3__.dispatch)('core').saveEntityRecord('root', 'site', {
      [optionName]: newValue
    });
  };

  return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.createElement)(_publish_guide_item__WEBPACK_IMPORTED_MODULE_6__["default"], (0,_babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__["default"])({
    isCompleted: addDomainComplete,
    name: "add_domain",
    text: (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_2__.__)('Look more professional and make it easier for customers to find your website.', 'godaddy-ventures'),
    title: (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_2__.__)('Add your domain', 'godaddy-ventures')
  }, props), addDomainComplete && (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.createElement)(_common_components_eid_wrapper__WEBPACK_IMPORTED_MODULE_7__.EidWrapper, {
    action: "click",
    section: "guide/item/add_domain",
    target: "edit"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.createElement)(_wordpress_components__WEBPACK_IMPORTED_MODULE_5__.Button, {
    className: "publish-guide-popover__link",
    isLink: true,
    onClick: () => setIsCompleted(false)
  }, (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_2__.__)('Change', 'godaddy-ventures'), " ", (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.createElement)(_wordpress_components__WEBPACK_IMPORTED_MODULE_5__.Icon, {
    icon: _wordpress_icons__WEBPACK_IMPORTED_MODULE_8__["default"],
    size: "20"
  }))), !addDomainComplete && (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.createElement)(_common_components_eid_wrapper__WEBPACK_IMPORTED_MODULE_7__.EidWrapper, {
    action: "click",
    section: "guide/item/add_domain",
    target: "edit"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.createElement)(_wordpress_components__WEBPACK_IMPORTED_MODULE_5__.ExternalLink, {
    className: "publish-guide-popover__link components-button is-link",
    href: changeDomainUrl,
    onClick: () => setIsCompleted(true)
  }, (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_2__.__)('Add domain', 'godaddy-ventures'))));
};

/* harmony default export */ __webpack_exports__["default"] = (AddDomainGuide);

/***/ }),

/***/ "./src/publish-guide/publish-guide-items/add-product.js":
/*!**************************************************************!*\
  !*** ./src/publish-guide/publish-guide-items/add-product.js ***!
  \**************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/extends */ "./node_modules/@babel/runtime/helpers/esm/extends.js");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @wordpress/i18n */ "@wordpress/i18n");
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _wordpress_data__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @wordpress/data */ "@wordpress/data");
/* harmony import */ var _wordpress_data__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_wordpress_data__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _wordpress_components__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @wordpress/components */ "@wordpress/components");
/* harmony import */ var _wordpress_components__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_wordpress_components__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _wordpress_core_data__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @wordpress/core-data */ "@wordpress/core-data");
/* harmony import */ var _wordpress_core_data__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_wordpress_core_data__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _common_components_eid_wrapper__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../common/components/eid-wrapper */ "./src/common/components/eid-wrapper.js");
/* harmony import */ var _publish_guide_item__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../publish-guide-item */ "./src/publish-guide/publish-guide-item.js");



/* global gdlPublishGuideItems, gdlConditions */

/**
 * WordPress dependencies
 */




/**
 * Internal dependencies
 */




const AddProductGuide = props => {
  const {
    setTooltip
  } = props;
  const optionName = gdlPublishGuideItems.AddProduct.propName;
  const [addProductComplete = gdlPublishGuideItems.AddProduct.default, setAddProductComplete] = (0,_wordpress_core_data__WEBPACK_IMPORTED_MODULE_5__.useEntityProp)('root', 'site', optionName);

  const setIsCompleted = newValue => {
    setAddProductComplete(newValue);
    (0,_wordpress_data__WEBPACK_IMPORTED_MODULE_3__.dispatch)('core').saveEntityRecord('root', 'site', {
      [optionName]: newValue
    });
  };

  return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.createElement)(_publish_guide_item__WEBPACK_IMPORTED_MODULE_7__["default"], (0,_babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__["default"])({
    isCompleted: addProductComplete,
    name: "add_product",
    text: (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_2__.__)('Take the first step towards e-commerce success by adding physical products, digital goods, or services that you plan to sell.', 'godaddy-ventures'),
    title: (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_2__.__)('Add your first product', 'godaddy-ventures')
  }, props), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.createElement)(_common_components_eid_wrapper__WEBPACK_IMPORTED_MODULE_6__.EidWrapper, {
    action: "click",
    section: "guide/item/add_product",
    target: "edit"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.createElement)(_wordpress_components__WEBPACK_IMPORTED_MODULE_4__.ExternalLink, {
    className: "publish-guide-popover__links components-button is-link",
    href: gdlConditions.newProductUri,
    onClick: () => {
      setTooltip('addProduct');
      setIsCompleted(true);
    }
  }, (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_2__.__)('Add Product', 'godaddy-ventures'))));
};

/* harmony default export */ __webpack_exports__["default"] = (AddProductGuide);

/***/ }),

/***/ "./src/publish-guide/publish-guide-items/site-content.js":
/*!***************************************************************!*\
  !*** ./src/publish-guide/publish-guide-items/site-content.js ***!
  \***************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/extends */ "./node_modules/@babel/runtime/helpers/esm/extends.js");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @wordpress/i18n */ "@wordpress/i18n");
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _wordpress_components__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @wordpress/components */ "@wordpress/components");
/* harmony import */ var _wordpress_components__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_wordpress_components__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _wordpress_data__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @wordpress/data */ "@wordpress/data");
/* harmony import */ var _wordpress_data__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_wordpress_data__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _wordpress_core_data__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @wordpress/core-data */ "@wordpress/core-data");
/* harmony import */ var _wordpress_core_data__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_wordpress_core_data__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _common_components_eid_wrapper__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../common/components/eid-wrapper */ "./src/common/components/eid-wrapper.js");
/* harmony import */ var _publish_guide_item__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../publish-guide-item */ "./src/publish-guide/publish-guide-item.js");



/* global gdlPublishGuideItems, gdvLinks */

/**
 * WordPress dependencies
 */




/**
 * Internal dependencies
 */




const SiteContentGuide = props => {
  const {
    setTooltip
  } = props;
  const optionName = gdlPublishGuideItems.SiteContent.propName;
  const [siteContentComplete = gdlPublishGuideItems.SiteContent.default, setSiteContentComplete] = (0,_wordpress_core_data__WEBPACK_IMPORTED_MODULE_5__.useEntityProp)('root', 'site', optionName);

  const setIsCompleted = newValue => {
    setSiteContentComplete(newValue);
    (0,_wordpress_data__WEBPACK_IMPORTED_MODULE_4__.dispatch)('core').saveEntityRecord('root', 'site', {
      [optionName]: newValue
    });
  };

  return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.createElement)(_publish_guide_item__WEBPACK_IMPORTED_MODULE_7__["default"], (0,_babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__["default"])({
    isCompleted: siteContentComplete,
    name: "site_content",
    title: (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_2__.__)('Add pages to your site', 'godaddy-ventures')
  }, props), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.createElement)("p", null, (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_2__.__)('Start with an adding an "about us" page to inform users about your business or "contact us" page tell users how to get in touch with you.', 'godaddy-ventures')), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.createElement)(_common_components_eid_wrapper__WEBPACK_IMPORTED_MODULE_6__.EidWrapper, {
    action: "click",
    section: "guide/item/site_content",
    target: "edit"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.createElement)(_wordpress_components__WEBPACK_IMPORTED_MODULE_3__.Button, {
    className: "publish-guide-popover__link",
    isLink: true,
    onClick: () => {
      setIsCompleted(true);

      if (!!gdvLinks.editorRedirectUrl) {
        window.location.assign(gdvLinks.editorRedirectUrl + '&tooltip=siteContent');
      } else {
        setTooltip('siteContent');
      }
    }
  }, (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_2__.__)('Get Started', 'godaddy-ventures'))));
};

/* harmony default export */ __webpack_exports__["default"] = (SiteContentGuide);

/***/ }),

/***/ "./src/publish-guide/publish-guide-items/site-design.js":
/*!**************************************************************!*\
  !*** ./src/publish-guide/publish-guide-items/site-design.js ***!
  \**************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/extends */ "./node_modules/@babel/runtime/helpers/esm/extends.js");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _wordpress_data__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @wordpress/data */ "@wordpress/data");
/* harmony import */ var _wordpress_data__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_wordpress_data__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _wordpress_core_data__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @wordpress/core-data */ "@wordpress/core-data");
/* harmony import */ var _wordpress_core_data__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_wordpress_core_data__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @wordpress/i18n */ "@wordpress/i18n");
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _wordpress_components__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @wordpress/components */ "@wordpress/components");
/* harmony import */ var _wordpress_components__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_wordpress_components__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _wordpress_icons__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @wordpress/icons */ "./node_modules/@wordpress/icons/build-module/library/chevron-left.js");
/* harmony import */ var _wordpress_icons__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @wordpress/icons */ "./node_modules/@wordpress/icons/build-module/library/chevron-right.js");
/* harmony import */ var _common_components_eid_wrapper__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../common/components/eid-wrapper */ "./src/common/components/eid-wrapper.js");
/* harmony import */ var _publish_guide_item__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../publish-guide-item */ "./src/publish-guide/publish-guide-item.js");



/* global gdlPublishGuideItems, gdvLinks */

/**
 * WordPress dependencies
 */





/**
 * Internal dependencies
 */




const SiteDesignGuide = props => {
  const {
    setTooltip
  } = props;
  const optionName = gdlPublishGuideItems.SiteDesign.propName;
  const [isComplete = gdlPublishGuideItems.SiteDesign.default, setIsComplete] = (0,_wordpress_core_data__WEBPACK_IMPORTED_MODULE_3__.useEntityProp)('root', 'site', optionName);

  const setIsCompleted = newValue => {
    setIsComplete(newValue);
    (0,_wordpress_data__WEBPACK_IMPORTED_MODULE_2__.dispatch)('core').saveEntityRecord('root', 'site', {
      [optionName]: newValue
    });
  };

  return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.createElement)(_publish_guide_item__WEBPACK_IMPORTED_MODULE_7__["default"], (0,_babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__["default"])({
    isCompleted: isComplete,
    name: "site_design",
    text: (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_4__.__)('Make your website more distinctive and personalized with a variety of colors, designs and fonts.', 'godaddy-ventures'),
    title: (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_4__.__)('Make the design your own', 'godaddy-ventures')
  }, props), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.createElement)(_common_components_eid_wrapper__WEBPACK_IMPORTED_MODULE_6__.EidWrapper, {
    action: "click",
    section: "guide/item/site_design",
    target: "edit"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.createElement)(_wordpress_components__WEBPACK_IMPORTED_MODULE_5__.Button, {
    className: "publish-guide-popover__link",
    isLink: true,
    onClick: () => {
      setIsCompleted(true);

      if (!!gdvLinks.editorRedirectUrl) {
        window.location.assign(gdvLinks.editorRedirectUrl + '&tooltip=siteDesign');
      } else {
        setTooltip('siteDesign');
      }
    }
  }, (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_4__.__)('Customize', 'godaddy-ventures'), " ", (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.createElement)(_wordpress_components__WEBPACK_IMPORTED_MODULE_5__.Icon, {
    icon: (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_4__.isRTL)() ? _wordpress_icons__WEBPACK_IMPORTED_MODULE_8__["default"] : _wordpress_icons__WEBPACK_IMPORTED_MODULE_9__["default"],
    size: "20"
  }))));
};

/* harmony default export */ __webpack_exports__["default"] = (SiteDesignGuide);

/***/ }),

/***/ "./src/publish-guide/publish-guide-items/site-info.js":
/*!************************************************************!*\
  !*** ./src/publish-guide/publish-guide-items/site-info.js ***!
  \************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SiteInfoGuide": function() { return /* binding */ SiteInfoGuide; }
/* harmony export */ });
/* harmony import */ var _babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/extends */ "./node_modules/@babel/runtime/helpers/esm/extends.js");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @wordpress/i18n */ "@wordpress/i18n");
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _wordpress_compose__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @wordpress/compose */ "@wordpress/compose");
/* harmony import */ var _wordpress_compose__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_wordpress_compose__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _wordpress_core_data__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @wordpress/core-data */ "@wordpress/core-data");
/* harmony import */ var _wordpress_core_data__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_wordpress_core_data__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _wordpress_components__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @wordpress/components */ "@wordpress/components");
/* harmony import */ var _wordpress_components__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_wordpress_components__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _wordpress_data__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @wordpress/data */ "@wordpress/data");
/* harmony import */ var _wordpress_data__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_wordpress_data__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _publish_guide_item__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../publish-guide-item */ "./src/publish-guide/publish-guide-item.js");
/* harmony import */ var _common_components_eid_wrapper__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../common/components/eid-wrapper */ "./src/common/components/eid-wrapper.js");



/* global gdlPublishGuideItems */

/**
 * External dependencies
 */

/**
 * WordPress dependencies
 */







/**
 * Internal dependencies
 */



const SiteInfoGuide = props => {
  const [modalOpen, setModalOpen] = (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.useState)(false);
  const optionName = gdlPublishGuideItems.SiteInfo.propName;
  const [siteInfoComplete = gdlPublishGuideItems.SiteInfo.default, setSiteInfoComplete] = (0,_wordpress_core_data__WEBPACK_IMPORTED_MODULE_5__.useEntityProp)('root', 'site', optionName);

  const setIsCompleted = newValue => {
    setSiteInfoComplete(newValue);
    (0,_wordpress_data__WEBPACK_IMPORTED_MODULE_7__.dispatch)('core').saveEntityRecord('root', 'site', {
      [optionName]: newValue
    });
  };

  const closeModal = () => setModalOpen(false);

  return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.createElement)(_publish_guide_item__WEBPACK_IMPORTED_MODULE_8__["default"], (0,_babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__["default"])({
    isCompleted: siteInfoComplete,
    name: "site_info",
    text: (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_3__.__)('Adding site details is essential for your site to be found online.', 'godaddy-ventures'),
    title: (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_3__.__)('Add site details', 'godaddy-ventures')
  }, props), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.createElement)(_common_components_eid_wrapper__WEBPACK_IMPORTED_MODULE_9__.EidWrapper, {
    action: "click",
    section: "guide/item/site_info",
    target: "edit"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.createElement)(_wordpress_components__WEBPACK_IMPORTED_MODULE_6__.Button, {
    className: "publish-guide-popover__link",
    isLink: true,
    onClick: () => setModalOpen(true)
  }, (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_3__.__)('Edit now', 'godaddy-ventures'))), modalOpen && (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.createElement)(SiteInfoModal, (0,_babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__["default"])({
    closeModal: closeModal,
    setIsCompleted: setIsCompleted
  }, props)));
};

const SiteInfoModal = _ref => {
  let {
    closeModal,
    setIsCompleted
  } = _ref;
  const [title] = (0,_wordpress_core_data__WEBPACK_IMPORTED_MODULE_5__.useEntityProp)('root', 'site', 'title');
  const [description] = (0,_wordpress_core_data__WEBPACK_IMPORTED_MODULE_5__.useEntityProp)('root', 'site', 'description');
  const [newTitle, setNewTitle] = (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.useState)(title);
  const [newDescription, setNewDescription] = (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.useState)(description);
  const [isSaving, setIsSaving] = (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.useState)(false); // Save the new settings.

  const saveSettings = () => {
    return (0,_wordpress_data__WEBPACK_IMPORTED_MODULE_7__.dispatch)('core').saveEntityRecord('root', 'site', {
      description: newDescription,
      title: newTitle
    });
  };

  const {
    url
  } = (0,_wordpress_data__WEBPACK_IMPORTED_MODULE_7__.useSelect)(select => {
    var _select$getSite;

    return {
      url: (_select$getSite = select('core').getSite()) === null || _select$getSite === void 0 ? void 0 : _select$getSite.url
    };
  });
  return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.createElement)(_wordpress_components__WEBPACK_IMPORTED_MODULE_6__.Modal, {
    className: "publish-guide-popover__modal godaddy-styles",
    isOpen: true,
    onRequestClose: closeModal,
    title: (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_3__.__)('Add your site info', 'godaddy-ventures')
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.createElement)(_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.Fragment, null, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.createElement)("p", null, (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_3__.__)('Adding your site\'s name and a quick description is helpful not only for Google and other search engines to find you but also for potential customers to know if this is the right place for them.', 'godaddy-ventures')), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.createElement)("div", {
    className: "publish-guide-site-info__content"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.createElement)("div", {
    className: "publish-guide-site-info__section form-section"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.createElement)(_wordpress_components__WEBPACK_IMPORTED_MODULE_6__.TextControl, {
    "data-eid": `${_common_components_eid_wrapper__WEBPACK_IMPORTED_MODULE_9__.EID_PREFIX}.guide/branding.input_title.text`,
    label: (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_3__.__)('What\'s the name of your site?', 'godaddy-ventures'),
    onChange: newValue => setNewTitle(newValue),
    value: newTitle
  }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.createElement)(_wordpress_components__WEBPACK_IMPORTED_MODULE_6__.TextControl, {
    "data-eid": `${_common_components_eid_wrapper__WEBPACK_IMPORTED_MODULE_9__.EID_PREFIX}.guide/branding.input_title.text`,
    label: (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_3__.__)('Describe in one sentence what you do.', 'godaddy-ventures'),
    onChange: newValue => setNewDescription(newValue),
    value: newDescription
  })), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.createElement)("div", {
    className: "publish-guide-site-info__section preview-section"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.createElement)("div", {
    className: "preview-info"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.createElement)("p", {
    className: "preview-info__title"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.createElement)("strong", null, newTitle)), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.createElement)("p", {
    className: "preview-info__site-url"
  }, url), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.createElement)("p", null, newDescription)), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.createElement)("p", {
    className: "preview-subtext"
  }, (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_3__.__)('Note you can change the url in a different step', 'godaddy-ventures')))), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.createElement)("div", {
    className: "publish-guide-site-info__action"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.createElement)(_wordpress_components__WEBPACK_IMPORTED_MODULE_6__.Button, {
    "data-eid": `${_common_components_eid_wrapper__WEBPACK_IMPORTED_MODULE_9__.EID_PREFIX}.guide/branding.close.click`,
    isSecondary: true,
    onClick: closeModal
  }, (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_3__.__)('Back', 'godaddy-ventures')), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.createElement)(_wordpress_components__WEBPACK_IMPORTED_MODULE_6__.Button, {
    className: classnames__WEBPACK_IMPORTED_MODULE_2___default()('publish-guide-popover__button', {
      'is-busy': isSaving
    }),
    "data-eid": `${_common_components_eid_wrapper__WEBPACK_IMPORTED_MODULE_9__.EID_PREFIX}.guide/branding.save.click`,
    disabled: isSaving,
    isPrimary: true,
    onClick: () => {
      setIsSaving(true);
      saveSettings().then(() => {
        // toggleComplete();
        setIsSaving(false);
        setIsCompleted(true);
        closeModal();
      });
    }
  }, (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_3__.__)('Done', 'godaddy-ventures')))));
};

/* harmony default export */ __webpack_exports__["default"] = ((0,_wordpress_compose__WEBPACK_IMPORTED_MODULE_4__.compose)([_wordpress_components__WEBPACK_IMPORTED_MODULE_6__.withNotices])(SiteInfoGuide));

/***/ }),

/***/ "./src/publish-guide/publish-guide-items/site-media.js":
/*!*************************************************************!*\
  !*** ./src/publish-guide/publish-guide-items/site-media.js ***!
  \*************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/extends */ "./node_modules/@babel/runtime/helpers/esm/extends.js");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! lodash */ "lodash");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @wordpress/i18n */ "@wordpress/i18n");
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _wordpress_compose__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @wordpress/compose */ "@wordpress/compose");
/* harmony import */ var _wordpress_compose__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_wordpress_compose__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _wordpress_editor__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @wordpress/editor */ "@wordpress/editor");
/* harmony import */ var _wordpress_editor__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_wordpress_editor__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _wordpress_core_data__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @wordpress/core-data */ "@wordpress/core-data");
/* harmony import */ var _wordpress_core_data__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_wordpress_core_data__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _wordpress_components__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @wordpress/components */ "@wordpress/components");
/* harmony import */ var _wordpress_components__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_wordpress_components__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _wordpress_data__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @wordpress/data */ "@wordpress/data");
/* harmony import */ var _wordpress_data__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(_wordpress_data__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _wordpress_block_editor__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @wordpress/block-editor */ "@wordpress/block-editor");
/* harmony import */ var _wordpress_block_editor__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(_wordpress_block_editor__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _publish_guide_item__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../publish-guide-item */ "./src/publish-guide/publish-guide-item.js");
/* harmony import */ var _common_components_eid_wrapper__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../common/components/eid-wrapper */ "./src/common/components/eid-wrapper.js");



/* global gdlPublishGuideItems */

/**
 * External dependencies
 */

/**
 * WordPress dependencies
 */









/**
 * Internal dependencies
 */



/**
 * Module constants
 */

const ALLOWED_MEDIA_TYPES = ['image'];

const SiteMediaGuide = props => {
  // In the case we are outside a block editor screen, we need to update the state with expected properties for the media upload permission.
  const {
    hasUploadPermissions,
    getSettings
  } = (0,_wordpress_data__WEBPACK_IMPORTED_MODULE_8__.useSelect)(select => {
    return {
      getSettings: select('core/block-editor').getSettings,
      hasUploadPermissions: (0,lodash__WEBPACK_IMPORTED_MODULE_2__.defaultTo)(select('core').canUser('create', 'media'), true)
    };
  });
  const {
    updateSettings
  } = (0,_wordpress_data__WEBPACK_IMPORTED_MODULE_8__.useDispatch)('core/block-editor');
  (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.useEffect)(() => {
    const settings = getSettings();
    updateSettings({ ...settings,
      hasUploadPermissions,
      mediaUpload: _wordpress_editor__WEBPACK_IMPORTED_MODULE_5__.mediaUpload
    });
  }, []);
  const optionName = gdlPublishGuideItems.SiteMedia.propName;
  const [isComplete = gdlPublishGuideItems.SiteMedia.default, setIsComplete] = (0,_wordpress_core_data__WEBPACK_IMPORTED_MODULE_6__.useEntityProp)('root', 'site', optionName);

  const setIsCompleted = newValue => {
    setIsComplete(newValue);
    (0,_wordpress_data__WEBPACK_IMPORTED_MODULE_8__.dispatch)('core').saveEntityRecord('root', 'site', {
      [optionName]: newValue
    });
  };

  const [logoUrl, setLogoUrl] = (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.useState)();
  /* istanbul ignore next */

  const {
    mediaItemData,
    sitelogo
  } = (0,_wordpress_data__WEBPACK_IMPORTED_MODULE_8__.useSelect)(select => {
    const siteSettings = select('core').getEditedEntityRecord('root', 'site');
    const mediaItem = select('core').getEntityRecord('root', 'media', siteSettings.sitelogo);
    return {
      mediaItemData: mediaItem && {
        alt: mediaItem.alt_text,
        url: mediaItem.source_url
      },
      sitelogo: siteSettings.sitelogo
    };
  }, []);
  const {
    editEntityRecord
  } = (0,_wordpress_data__WEBPACK_IMPORTED_MODULE_8__.useDispatch)('core');

  const setLogo = newValue => editEntityRecord('root', 'site', undefined, {
    sitelogo: newValue
  });

  const onSelectLogo = media => {
    if (!media) {
      return;
    }

    if (!media.id && media.url) {
      setLogo('');
      setLogoUrl(media.url);
    }

    if (media.id) {
      var _media$id$toString;

      (0,_wordpress_data__WEBPACK_IMPORTED_MODULE_8__.dispatch)('core').saveEntityRecord('root', 'site', {
        sitelogo: (_media$id$toString = media.id.toString()) !== null && _media$id$toString !== void 0 ? _media$id$toString : ''
      }).then(() => {
        setIsCompleted(true);
      });
    }
  }; // Set the image URL once retrieved.


  if (mediaItemData && logoUrl !== mediaItemData.url) {
    setLogoUrl(mediaItemData.url);
  }

  return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.createElement)(_publish_guide_item__WEBPACK_IMPORTED_MODULE_10__["default"], (0,_babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__["default"])({
    isCompleted: isComplete,
    name: "site_media",
    text: (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_3__.__)('A logo is the foundation of your brand identity and is often the first thing people see when buying products and services.', 'godaddy-ventures'),
    title: (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_3__.__)('Upload your logo', 'godaddy-ventures')
  }, props), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.createElement)(_wordpress_block_editor__WEBPACK_IMPORTED_MODULE_9__.MediaUploadCheck, null, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.createElement)(_wordpress_block_editor__WEBPACK_IMPORTED_MODULE_9__.MediaUpload, {
    allowedTypes: ALLOWED_MEDIA_TYPES,
    onSelect: onSelectLogo,
    render: _ref => {
      let {
        open
      } = _ref;
      return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.createElement)(_common_components_eid_wrapper__WEBPACK_IMPORTED_MODULE_11__.EidWrapper, {
        action: "click",
        section: "guide/item/site_media",
        target: "edit"
      }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_1__.createElement)(_wordpress_components__WEBPACK_IMPORTED_MODULE_7__.Button, {
        className: "publish-guide-popover__link",
        isLink: true,
        onClick: open
      }, (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_3__.__)('Add now', 'godaddy-ventures')));
    },
    title: (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_3__.__)('Upload site logo', 'godaddy-ventures'),
    value: sitelogo
  })));
};

/* harmony default export */ __webpack_exports__["default"] = ((0,_wordpress_compose__WEBPACK_IMPORTED_MODULE_4__.compose)([_wordpress_components__WEBPACK_IMPORTED_MODULE_7__.withNotices])(SiteMediaGuide));

/***/ }),

/***/ "./src/publish-guide/publish-guide-list.js":
/*!*************************************************!*\
  !*** ./src/publish-guide/publish-guide-list.js ***!
  \*************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* binding */ PublishGuideList; }
/* harmony export */ });
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__);


/**
 * WordPress dependencies
 */

function PublishGuideList(props) {
  const {
    children,
    selectedItem,
    setSelectedItem
  } = props;
  return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("ul", {
    className: "publish-guide-popover__items"
  }, children === null || children === void 0 ? void 0 : children.map((child, childIndex) => {
    return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.cloneElement)(child, {
      key: `publish-guide-item-${childIndex}`,
      selectedItem,
      setSelectedItem
    });
  }));
}

/***/ }),

/***/ "./src/publish-guide/publish-guide-progress.js":
/*!*****************************************************!*\
  !*** ./src/publish-guide/publish-guide-progress.js ***!
  \*****************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* binding */ PublishGuideProgress; }
/* harmony export */ });
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__);


/**
 * WordPress dependencies
 */

function PublishGuideProgress(props) {
  const {
    stepsCompleted,
    stepsTotal
  } = props;
  const progressCircle = (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.useRef)();
  const [percent, setPercent] = (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.useState)(0);
  (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.useEffect)(() => {
    const radius = progressCircle.current.r.baseVal.value;
    const circumference = radius * 2 * Math.PI;
    const newPercent = !stepsCompleted || !stepsTotal ? 0 : Math.round(100 * stepsCompleted / stepsTotal);
    const offset = circumference - newPercent / 100 * circumference;
    setPercent(newPercent);
    progressCircle.current.style.strokeDasharray = `${circumference} ${circumference}`;
    progressCircle.current.style.strokeDashoffset = offset;
  }, [stepsCompleted]);
  return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("div", {
    className: "publish-guide-progress"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("svg", {
    className: "publish-guide-progress__svg",
    height: "60",
    width: "60"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("circle", {
    className: "publish-guide-progress__svg__circle-bg",
    cx: "28",
    cy: "30",
    r: "25"
  }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("circle", {
    className: "publish-guide-progress__svg__circle",
    cx: "28",
    cy: "30",
    r: "25",
    ref: progressCircle
  })), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("span", {
    className: "publish-guide-progress__label"
  }, percent, "%"));
}

/***/ }),

/***/ "./node_modules/classnames/index.js":
/*!******************************************!*\
  !*** ./node_modules/classnames/index.js ***!
  \******************************************/
/***/ (function(module, exports) {

var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/*!
  Copyright (c) 2018 Jed Watson.
  Licensed under the MIT License (MIT), see
  http://jedwatson.github.io/classnames
*/
/* global define */

(function () {
	'use strict';

	var hasOwn = {}.hasOwnProperty;

	function classNames() {
		var classes = [];

		for (var i = 0; i < arguments.length; i++) {
			var arg = arguments[i];
			if (!arg) continue;

			var argType = typeof arg;

			if (argType === 'string' || argType === 'number') {
				classes.push(arg);
			} else if (Array.isArray(arg)) {
				if (arg.length) {
					var inner = classNames.apply(null, arg);
					if (inner) {
						classes.push(inner);
					}
				}
			} else if (argType === 'object') {
				if (arg.toString === Object.prototype.toString) {
					for (var key in arg) {
						if (hasOwn.call(arg, key) && arg[key]) {
							classes.push(key);
						}
					}
				} else {
					classes.push(arg.toString());
				}
			}
		}

		return classes.join(' ');
	}

	if ( true && module.exports) {
		classNames.default = classNames;
		module.exports = classNames;
	} else if (true) {
		// register as 'classnames', consistent with npm package name
		!(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_RESULT__ = (function () {
			return classNames;
		}).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
		__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	} else {}
}());


/***/ }),

/***/ "./src/publish-guide/index.scss":
/*!**************************************!*\
  !*** ./src/publish-guide/index.scss ***!
  \**************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
// extracted by mini-css-extract-plugin


/***/ }),

/***/ "lodash":
/*!*************************!*\
  !*** external "lodash" ***!
  \*************************/
/***/ (function(module) {

"use strict";
module.exports = window["lodash"];

/***/ }),

/***/ "@wordpress/block-editor":
/*!*************************************!*\
  !*** external ["wp","blockEditor"] ***!
  \*************************************/
/***/ (function(module) {

"use strict";
module.exports = window["wp"]["blockEditor"];

/***/ }),

/***/ "@wordpress/components":
/*!************************************!*\
  !*** external ["wp","components"] ***!
  \************************************/
/***/ (function(module) {

"use strict";
module.exports = window["wp"]["components"];

/***/ }),

/***/ "@wordpress/compose":
/*!*********************************!*\
  !*** external ["wp","compose"] ***!
  \*********************************/
/***/ (function(module) {

"use strict";
module.exports = window["wp"]["compose"];

/***/ }),

/***/ "@wordpress/core-data":
/*!**********************************!*\
  !*** external ["wp","coreData"] ***!
  \**********************************/
/***/ (function(module) {

"use strict";
module.exports = window["wp"]["coreData"];

/***/ }),

/***/ "@wordpress/data":
/*!******************************!*\
  !*** external ["wp","data"] ***!
  \******************************/
/***/ (function(module) {

"use strict";
module.exports = window["wp"]["data"];

/***/ }),

/***/ "@wordpress/dom-ready":
/*!**********************************!*\
  !*** external ["wp","domReady"] ***!
  \**********************************/
/***/ (function(module) {

"use strict";
module.exports = window["wp"]["domReady"];

/***/ }),

/***/ "@wordpress/editor":
/*!********************************!*\
  !*** external ["wp","editor"] ***!
  \********************************/
/***/ (function(module) {

"use strict";
module.exports = window["wp"]["editor"];

/***/ }),

/***/ "@wordpress/element":
/*!*********************************!*\
  !*** external ["wp","element"] ***!
  \*********************************/
/***/ (function(module) {

"use strict";
module.exports = window["wp"]["element"];

/***/ }),

/***/ "@wordpress/hooks":
/*!*******************************!*\
  !*** external ["wp","hooks"] ***!
  \*******************************/
/***/ (function(module) {

"use strict";
module.exports = window["wp"]["hooks"];

/***/ }),

/***/ "@wordpress/i18n":
/*!******************************!*\
  !*** external ["wp","i18n"] ***!
  \******************************/
/***/ (function(module) {

"use strict";
module.exports = window["wp"]["i18n"];

/***/ }),

/***/ "@wordpress/media-utils":
/*!************************************!*\
  !*** external ["wp","mediaUtils"] ***!
  \************************************/
/***/ (function(module) {

"use strict";
module.exports = window["wp"]["mediaUtils"];

/***/ }),

/***/ "@wordpress/primitives":
/*!************************************!*\
  !*** external ["wp","primitives"] ***!
  \************************************/
/***/ (function(module) {

"use strict";
module.exports = window["wp"]["primitives"];

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/esm/extends.js":
/*!************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/esm/extends.js ***!
  \************************************************************/
/***/ (function(__unused_webpack___webpack_module__, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* binding */ _extends; }
/* harmony export */ });
function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/compat get default export */
/******/ 	!function() {
/******/ 		// getDefaultExport function for compatibility with non-harmony modules
/******/ 		__webpack_require__.n = function(module) {
/******/ 			var getter = module && module.__esModule ?
/******/ 				function() { return module['default']; } :
/******/ 				function() { return module; };
/******/ 			__webpack_require__.d(getter, { a: getter });
/******/ 			return getter;
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	!function() {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = function(exports, definition) {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	!function() {
/******/ 		__webpack_require__.o = function(obj, prop) { return Object.prototype.hasOwnProperty.call(obj, prop); }
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	!function() {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = function(exports) {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	}();
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be in strict mode.
!function() {
"use strict";
/*!************************************!*\
  !*** ./src/publish-guide/index.js ***!
  \************************************/
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PublishGuide": function() { return /* binding */ PublishGuide; }
/* harmony export */ });
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @wordpress/i18n */ "@wordpress/i18n");
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _wordpress_dom_ready__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @wordpress/dom-ready */ "@wordpress/dom-ready");
/* harmony import */ var _wordpress_dom_ready__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_wordpress_dom_ready__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _wordpress_components__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @wordpress/components */ "@wordpress/components");
/* harmony import */ var _wordpress_components__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_wordpress_components__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _wordpress_core_data__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @wordpress/core-data */ "@wordpress/core-data");
/* harmony import */ var _wordpress_core_data__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_wordpress_core_data__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _wordpress_data__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @wordpress/data */ "@wordpress/data");
/* harmony import */ var _wordpress_data__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_wordpress_data__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _publish_guide_items_add_domain__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./publish-guide-items/add-domain */ "./src/publish-guide/publish-guide-items/add-domain.js");
/* harmony import */ var _publish_guide_items_add_product__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./publish-guide-items/add-product */ "./src/publish-guide/publish-guide-items/add-product.js");
/* harmony import */ var _common_components_gdl_tool_tip__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../common/components/gdl-tool-tip */ "./src/common/components/gdl-tool-tip.js");
/* harmony import */ var _publish_guide_header_button__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./publish-guide-header-button */ "./src/publish-guide/publish-guide-header-button.js");
/* harmony import */ var _publish_guide_list__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./publish-guide-list */ "./src/publish-guide/publish-guide-list.js");
/* harmony import */ var _publish_guide_progress__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./publish-guide-progress */ "./src/publish-guide/publish-guide-progress.js");
/* harmony import */ var _publish_guide_items_site_content__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./publish-guide-items/site-content */ "./src/publish-guide/publish-guide-items/site-content.js");
/* harmony import */ var _publish_guide_items_site_design__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./publish-guide-items/site-design */ "./src/publish-guide/publish-guide-items/site-design.js");
/* harmony import */ var _publish_guide_items_site_info__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./publish-guide-items/site-info */ "./src/publish-guide/publish-guide-items/site-info.js");
/* harmony import */ var _publish_guide_items_site_media__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./publish-guide-items/site-media */ "./src/publish-guide/publish-guide-items/site-media.js");
/* harmony import */ var _hooks_js__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./hooks.js */ "./src/publish-guide/hooks.js");
/* harmony import */ var _index_scss__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./index.scss */ "./src/publish-guide/index.scss");
var _ref;



/* global gdvPublishGuideDefaults, gdlPublishGuideItems */

/**
 * WordPress dependencies
 */






/**
 * Internal dependencies
 */













const wpUserData = (_ref = typeof gdvPublishGuideDefaults === 'undefined') !== null && _ref !== void 0 ? _ref : JSON.parse(window.localStorage.getItem('WP_DATA_USER_' + gdvPublishGuideDefaults.userId));
function PublishGuide() {
  var _gdlPublishGuideItems, _gdlPublishGuideItems2, _gdlPublishGuideItems3, _gdlPublishGuideItems4, _gdlPublishGuideItems5, _gdlPublishGuideItems6, _gdlPublishGuideItems7, _gdlPublishGuideItems8, _gdlPublishGuideItems9, _gdlPublishGuideItems10, _gdlPublishGuideItems11, _gdlPublishGuideItems12;

  let tooltipTargetElement;
  let tooltipAutoHideTimeout;
  const tooltips = {
    addProduct: {},
    publishGuideHere: {
      autoHide: 10000,
      collapseSidePanel: false,
      description: (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__.__)('Once you\'re done selecting your site style and colors, click here to continue publishing your site.', 'godaddy-launch'),
      refID: 'gdl-publish-guide-trigger-button',
      title: (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__.__)('Getting Started Guide', 'godaddy-launch')
    },
    siteContent: {
      autoHide: false,
      collapseSidePanel: true,
      description: (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__.__)('This button will open up your page manager.', 'godaddy-launch'),
      refID: 'coblocks-menu-icon-site-content',
      title: (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__.__)('Open the site content tab', 'godaddy-launch')
    },
    siteDesign: {
      autoHide: false,
      collapseSidePanel: true,
      description: (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__.__)('If you want to update the feel of a site and bring in your own brand, this is a good place to start.', 'godaddy-launch'),
      refID: 'coblocks-menu-icon-site-design',
      title: (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__.__)('Change any global styles here', 'godaddy-launch')
    }
  };
  const [guideItems, setGuideItems] = (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.useState)([]);
  const [isCompleted, setIsCompleted] = (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.useState)(false);
  const [isOpen, setIsOpen] = (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.useState)(false);
  const [selectedItem, setSelectedItem] = (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.useState)(null);
  const [stepsCompleted, setStepsCompleted] = (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.useState)(0);
  const [tooltip, setTooltip] = (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.useState)(null);
  const editPostDispatch = (0,_wordpress_data__WEBPACK_IMPORTED_MODULE_5__.useDispatch)('core/edit-post');
  const {
    isWelcomeGuideActive
  } = (0,_wordpress_data__WEBPACK_IMPORTED_MODULE_5__.useSelect)(select => {
    var _select;

    return {
      isWelcomeGuideActive: (_select = select('core/edit-post')) === null || _select === void 0 ? void 0 : _select.isFeatureActive('welcomeGuide')
    };
  }, []); // TODO: refactor global completion counting.

  const [addDomainComplete = (_gdlPublishGuideItems = gdlPublishGuideItems.AddDomain) === null || _gdlPublishGuideItems === void 0 ? void 0 : _gdlPublishGuideItems.default] = (0,_wordpress_core_data__WEBPACK_IMPORTED_MODULE_4__.useEntityProp)('root', 'site', (_gdlPublishGuideItems2 = gdlPublishGuideItems.AddDomain) === null || _gdlPublishGuideItems2 === void 0 ? void 0 : _gdlPublishGuideItems2.propName);
  const [addProductComplete = (_gdlPublishGuideItems3 = gdlPublishGuideItems.AddProduct) === null || _gdlPublishGuideItems3 === void 0 ? void 0 : _gdlPublishGuideItems3.default] = (0,_wordpress_core_data__WEBPACK_IMPORTED_MODULE_4__.useEntityProp)('root', 'site', (_gdlPublishGuideItems4 = gdlPublishGuideItems.AddProduct) === null || _gdlPublishGuideItems4 === void 0 ? void 0 : _gdlPublishGuideItems4.propName);
  const [siteContentComplete = (_gdlPublishGuideItems5 = gdlPublishGuideItems.SiteContent) === null || _gdlPublishGuideItems5 === void 0 ? void 0 : _gdlPublishGuideItems5.default] = (0,_wordpress_core_data__WEBPACK_IMPORTED_MODULE_4__.useEntityProp)('root', 'site', (_gdlPublishGuideItems6 = gdlPublishGuideItems.SiteContent) === null || _gdlPublishGuideItems6 === void 0 ? void 0 : _gdlPublishGuideItems6.propName);
  const [siteDesignComplete = (_gdlPublishGuideItems7 = gdlPublishGuideItems.SiteDesign) === null || _gdlPublishGuideItems7 === void 0 ? void 0 : _gdlPublishGuideItems7.default] = (0,_wordpress_core_data__WEBPACK_IMPORTED_MODULE_4__.useEntityProp)('root', 'site', (_gdlPublishGuideItems8 = gdlPublishGuideItems.SiteDesign) === null || _gdlPublishGuideItems8 === void 0 ? void 0 : _gdlPublishGuideItems8.propName);
  const [siteInfoComplete = (_gdlPublishGuideItems9 = gdlPublishGuideItems.SiteInfo) === null || _gdlPublishGuideItems9 === void 0 ? void 0 : _gdlPublishGuideItems9.default] = (0,_wordpress_core_data__WEBPACK_IMPORTED_MODULE_4__.useEntityProp)('root', 'site', (_gdlPublishGuideItems10 = gdlPublishGuideItems.SiteInfo) === null || _gdlPublishGuideItems10 === void 0 ? void 0 : _gdlPublishGuideItems10.propName);
  const [siteMediaComplete = (_gdlPublishGuideItems11 = gdlPublishGuideItems.SiteMedia) === null || _gdlPublishGuideItems11 === void 0 ? void 0 : _gdlPublishGuideItems11.default] = (0,_wordpress_core_data__WEBPACK_IMPORTED_MODULE_4__.useEntityProp)('root', 'site', (_gdlPublishGuideItems12 = gdlPublishGuideItems.SiteMedia) === null || _gdlPublishGuideItems12 === void 0 ? void 0 : _gdlPublishGuideItems12.propName); // Track for completion state.

  const [// If gdlLiveSiteControlData is undefined, the site has published because we do not localize the data otherwise.
  sitePublished = typeof gdlLiveSiteControlData === 'undefined'] = (0,_wordpress_core_data__WEBPACK_IMPORTED_MODULE_4__.useEntityProp)('root', 'site', 'gdl_site_published');
  (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.useEffect)(() => {
    var _gdlPublishGuideItems13, _gdlPublishGuideItems14, _gdlPublishGuideItems15, _gdlPublishGuideItems16, _gdlPublishGuideItems17, _gdlPublishGuideItems18;

    // Hide the welcomeGuide if no localStorage is present (normally on first load).
    if (!wpUserData && isWelcomeGuideActive && editPostDispatch !== null && editPostDispatch !== void 0 && editPostDispatch.toggleFeature) {
      editPostDispatch === null || editPostDispatch === void 0 ? void 0 : editPostDispatch.toggleFeature('welcomeGuide');
    }

    setGuideItems([!!((_gdlPublishGuideItems13 = gdlPublishGuideItems.SiteInfo) !== null && _gdlPublishGuideItems13 !== void 0 && _gdlPublishGuideItems13.enabled) && (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_publish_guide_items_site_info__WEBPACK_IMPORTED_MODULE_14__["default"], {
      hasCompleted: !!siteInfoComplete,
      key: "SiteInfoGuide"
    }), !!((_gdlPublishGuideItems14 = gdlPublishGuideItems.SiteMedia) !== null && _gdlPublishGuideItems14 !== void 0 && _gdlPublishGuideItems14.enabled) && (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_publish_guide_items_site_media__WEBPACK_IMPORTED_MODULE_15__["default"], {
      hasCompleted: !!siteMediaComplete,
      key: "SiteMediaGuide"
    }), !!((_gdlPublishGuideItems15 = gdlPublishGuideItems.SiteContent) !== null && _gdlPublishGuideItems15 !== void 0 && _gdlPublishGuideItems15.enabled) && (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_publish_guide_items_site_content__WEBPACK_IMPORTED_MODULE_12__["default"], {
      hasCompleted: !!siteContentComplete,
      key: "SiteContentGuide",
      setTooltip: triggerTooltip
    }), !!((_gdlPublishGuideItems16 = gdlPublishGuideItems.SiteDesign) !== null && _gdlPublishGuideItems16 !== void 0 && _gdlPublishGuideItems16.enabled) && (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_publish_guide_items_site_design__WEBPACK_IMPORTED_MODULE_13__["default"], {
      hasCompleted: !!siteDesignComplete,
      key: "SiteDesignGuide",
      setTooltip: triggerTooltip
    }), !!((_gdlPublishGuideItems17 = gdlPublishGuideItems.AddProduct) !== null && _gdlPublishGuideItems17 !== void 0 && _gdlPublishGuideItems17.enabled) && (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_publish_guide_items_add_product__WEBPACK_IMPORTED_MODULE_7__["default"], {
      hasCompleted: !!addProductComplete,
      key: "AddProductGuide",
      setTooltip: triggerTooltip
    }), !!((_gdlPublishGuideItems18 = gdlPublishGuideItems.AddDomain) !== null && _gdlPublishGuideItems18 !== void 0 && _gdlPublishGuideItems18.enabled) && (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_publish_guide_items_add_domain__WEBPACK_IMPORTED_MODULE_6__["default"], {
      hasCompleted: !!addDomainComplete,
      key: "AddDomainGuide"
    })].filter(item => item !== false) // filter disabled items.
    );
    return () => {
      if (!tooltipTargetElement) {
        return null;
      }

      unbindTooltipTargetElementEvent();
    };
  }, [addDomainComplete, addProductComplete, siteContentComplete, siteDesignComplete, siteInfoComplete, siteMediaComplete]);
  (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.useEffect)(() => {
    const completedItemsCount = guideItems.filter(item => item.props.hasCompleted);

    if (guideItems.length > 0 && completedItemsCount.length === guideItems.length) {
      setIsCompleted(true);
    }

    setStepsCompleted(completedItemsCount.length);
  }, [guideItems]);
  (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.useEffect)(() => {
    if (!tooltip) {
      return;
    }

    tooltipTargetElement = document.getElementById(tooltip.refID);

    if (!tooltipTargetElement) {
      return;
    }

    tooltipTargetElement.addEventListener('click', handleTooltipIconClick);
  }, [tooltip]);
  (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.useEffect)(() => {
    const params = {};
    window.location.search.substr(1).split('&').forEach(item => {
      params[item.split('=')[0]] = item.split('=')[1];
    });

    if (params.tooltip) {
      setTimeout(() => triggerTooltip(params.tooltip), 2000);
    }
  }, []);
  /**
   * Toggle the Popover
   */

  const onTogglePublishGuideOpen = () => {
    setIsOpen(!isOpen);
  };
  /**
   * Trigger the Tooltip
   *
   * @param {string} el - Name of the key to display
   */


  const triggerTooltip = el => {
    const activeTooltip = tooltips[el];

    if (!activeTooltip) {
      return null;
    }

    if (tooltipTargetElement) {
      closeTooltip();
    }

    tooltipTargetElement = document.getElementById(activeTooltip.refID);

    if (!tooltipTargetElement) {
      return null;
    }

    if (activeTooltip.collapseSidePanel) {
      editPostDispatch.closeGeneralSidebar();
    }

    if (activeTooltip.autoHide) {
      this.tooltipAutoHideTimeout = setTimeout(closeTooltip, activeTooltip.autoHide);
    }

    setIsOpen(false);
    setTooltip({ ...activeTooltip,
      anchorRect: tooltipTargetElement.getBoundingClientRect()
    });
  };
  /**
   * Handle click actions on the element associated with the Tooltip
   */


  const handleTooltipIconClick = () => {
    unbindTooltipTargetElementEvent();

    if (tooltip && tooltip.nextTooltip) {
      setTimeout(() => triggerTooltip(tooltip.nextTooltip), 200);
      return;
    }

    closeTooltip();
  };
  /**
   * Close the Tooltip
   */


  const closeTooltip = () => {
    clearTimeout(tooltipAutoHideTimeout);
    setTooltip(null);

    if (!tooltipTargetElement) {
      return null;
    }

    unbindTooltipTargetElementEvent();
    tooltipTargetElement = null;
  };
  /**
   * Remove the eventListener on the element associated with the Tooltip
   */


  const unbindTooltipTargetElementEvent = () => tooltipTargetElement.removeEventListener('click', handleTooltipIconClick);

  const PublishGuideButton = () => {
    return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_publish_guide_header_button__WEBPACK_IMPORTED_MODULE_9__["default"], {
      isOpen: isOpen,
      onTogglePublishGuideOpen: onTogglePublishGuideOpen
    });
  };

  const [selector, setSelector] = (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.useState)(false);

  if (!selector) {
    const node = 'post.php' !== gdvPublishGuideDefaults.page && 'post-new.php' !== gdvPublishGuideDefaults.page ? document.querySelector('#wpadminbar') : document.querySelector('.edit-post-header__toolbar');

    if (node) {
      setSelector(node);
    }

    return null;
  }

  return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, (!isCompleted || !sitePublished) && (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createPortal)((0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("div", {
    className: "publish-guide-trigger godaddy-styles",
    id: "gdl-publish-guide"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(PublishGuideButton, null), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_wordpress_components__WEBPACK_IMPORTED_MODULE_3__.Popover, {
    className: `publish-guide-popover`,
    focusOnMount: "container",
    position: "bottom center",
    style: {
      display: isOpen ? 'block' : 'none'
    }
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("div", {
    className: "publish-guide-popover__header components-modal__header"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("div", {
    className: "publish-guide-popover__header__progress"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_publish_guide_progress__WEBPACK_IMPORTED_MODULE_11__["default"], {
    stepsCompleted: stepsCompleted,
    stepsTotal: guideItems.length
  })), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("div", {
    className: "publish-guide-popover__header__content"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("h1", {
    className: "publish-guide-popover__header__title components-modal__header-heading"
  }, (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__.__)('Get Started', 'godaddy-launch')), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("p", null, (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__.__)('Give your site a strong start.', 'godaddy-launch')))), !isCompleted && (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_publish_guide_list__WEBPACK_IMPORTED_MODULE_10__["default"], {
    selectedItem: selectedItem,
    setSelectedItem: name => setSelectedItem(name)
  }, guideItems), isCompleted && (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("div", {
    className: "publish-guide-popover__success"
  }, (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__.__)('Make sure to launch your site so others can see it online.', 'godaddy-launch')), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("div", {
    className: "publish-guide-popover__footer"
  }))), selector), tooltip && (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createPortal)((0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_common_components_gdl_tool_tip__WEBPACK_IMPORTED_MODULE_8__["default"], {
    closeCallback: closeTooltip,
    tooltip: tooltip
  }), document.body));
}
_wordpress_dom_ready__WEBPACK_IMPORTED_MODULE_2___default()(() => {
  (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.render)((0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(PublishGuide, null), document.getElementById(gdvPublishGuideDefaults.appContainerClass));
});
}();
/******/ })()
;
//# sourceMappingURL=publish-guide.js.map