/******/ (function() { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./node_modules/@wordpress/icons/build-module/icon/index.js":
/*!******************************************************************!*\
  !*** ./node_modules/@wordpress/icons/build-module/icon/index.js ***!
  \******************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__);
/**
 * WordPress dependencies
 */

/** @typedef {{icon: JSX.Element, size?: number} & import('@wordpress/primitives').SVGProps} IconProps */

/**
 * Return an SVG icon.
 *
 * @param {IconProps} props icon is the SVG component to render
 *                          size is a number specifiying the icon size in pixels
 *                          Other props will be passed to wrapped SVG component
 *
 * @return {JSX.Element}  Icon component
 */

function Icon(_ref) {
  let {
    icon,
    size = 24,
    ...props
  } = _ref;
  return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.cloneElement)(icon, {
    width: size,
    height: size,
    ...props
  });
}

/* harmony default export */ __webpack_exports__["default"] = (Icon);
//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./node_modules/@wordpress/icons/build-module/library/close.js":
/*!*********************************************************************!*\
  !*** ./node_modules/@wordpress/icons/build-module/library/close.js ***!
  \*********************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _wordpress_primitives__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @wordpress/primitives */ "@wordpress/primitives");
/* harmony import */ var _wordpress_primitives__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_wordpress_primitives__WEBPACK_IMPORTED_MODULE_1__);


/**
 * WordPress dependencies
 */

const close = (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_wordpress_primitives__WEBPACK_IMPORTED_MODULE_1__.SVG, {
  xmlns: "http://www.w3.org/2000/svg",
  viewBox: "0 0 24 24"
}, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_wordpress_primitives__WEBPACK_IMPORTED_MODULE_1__.Path, {
  d: "M13 11.8l6.1-6.3-1-1-6.1 6.2-6.1-6.2-1 1 6.1 6.3-6.5 6.7 1 1 6.5-6.6 6.5 6.6 1-1z"
}));
/* harmony default export */ __webpack_exports__["default"] = (close);
//# sourceMappingURL=close.js.map

/***/ }),

/***/ "./node_modules/@wordpress/icons/build-module/library/pages.js":
/*!*********************************************************************!*\
  !*** ./node_modules/@wordpress/icons/build-module/library/pages.js ***!
  \*********************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _wordpress_primitives__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @wordpress/primitives */ "@wordpress/primitives");
/* harmony import */ var _wordpress_primitives__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_wordpress_primitives__WEBPACK_IMPORTED_MODULE_1__);


/**
 * WordPress dependencies
 */

const pages = (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_wordpress_primitives__WEBPACK_IMPORTED_MODULE_1__.SVG, {
  xmlns: "http://www.w3.org/2000/svg",
  viewBox: "0 0 24 24"
}, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_wordpress_primitives__WEBPACK_IMPORTED_MODULE_1__.Path, {
  d: "M7 13.8h6v-1.5H7v1.5zM18 16V4c0-1.1-.9-2-2-2H6c-1.1 0-2 .9-2 2v12c0 1.1.9 2 2 2h10c1.1 0 2-.9 2-2zM5.5 16V4c0-.3.2-.5.5-.5h10c.3 0 .5.2.5.5v12c0 .3-.2.5-.5.5H6c-.3 0-.5-.2-.5-.5zM7 10.5h8V9H7v1.5zm0-3.3h8V5.8H7v1.4zM20.2 6v13c0 .7-.6 1.2-1.2 1.2H8v1.5h11c1.5 0 2.7-1.2 2.7-2.8V6h-1.5z"
}));
/* harmony default export */ __webpack_exports__["default"] = (pages);
//# sourceMappingURL=pages.js.map

/***/ }),

/***/ "./src/common/components/eid-wrapper.js":
/*!**********************************************!*\
  !*** ./src/common/components/eid-wrapper.js ***!
  \**********************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "EID_PREFIX": function() { return /* binding */ EID_PREFIX; },
/* harmony export */   "EidWrapper": function() { return /* binding */ EidWrapper; }
/* harmony export */ });
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__);
/** global wp */

/**
 * WordPress dependencies
 */

const EID_PREFIX = 'wp.' + (!!wp.editPost ? 'editor' : 'wpadmin');
/**
 * Return an element wrapped with Data-EID properties.
 *
 * @param {Object} obj
 * @param {Object} obj.children
 * @param {string} obj.section
 * @param {string} obj.target
 * @param {string} obj.action
 */

const EidWrapper = _ref => {
  let {
    children,
    section,
    target,
    action
  } = _ref;

  if (!(0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.isValidElement)(children)) {
    return children;
  }

  return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.cloneElement)(children, { ...children.props,
    'data-eid': `${EID_PREFIX}.${section}.${target}.${action}`
  });
};

/***/ }),

/***/ "./src/common/components/gdl-tool-tip.js":
/*!***********************************************!*\
  !*** ./src/common/components/gdl-tool-tip.js ***!
  \***********************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* binding */ GoDaddyLaunchTooltip; }
/* harmony export */ });
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @wordpress/i18n */ "@wordpress/i18n");
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _wordpress_icons__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @wordpress/icons */ "./node_modules/@wordpress/icons/build-module/library/close.js");
/* harmony import */ var _wordpress_components__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @wordpress/components */ "@wordpress/components");
/* harmony import */ var _wordpress_components__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_wordpress_components__WEBPACK_IMPORTED_MODULE_2__);


/**
 * WordPress dependencies
 */



function GoDaddyLaunchTooltip(props) {
  const {
    tooltip,
    closeCallback
  } = props;
  return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_wordpress_components__WEBPACK_IMPORTED_MODULE_2__.Popover, {
    anchorRect: tooltip.anchorRect,
    className: `publish-guide-tooltip godaddy-styles${!tooltip.nextTooltip ? ' publish-guide-tooltip--next' : ''}`,
    noArrow: false,
    position: "middle left"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("p", {
    className: "publish-guide-tooltip__label"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("svg", {
    fill: "none",
    height: "16",
    viewBox: "0 0 18 16",
    width: "18",
    xmlns: "http://www.w3.org/2000/svg"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("path", {
    d: "M15.5266 0.730524C13.6559 -0.433974 11.1932 -0.159034 8.99681 1.21948C6.80726 -0.158272 4.34306 -0.433974 2.47469 0.730524C-0.48083 2.57438 -0.840161 7.32376 1.67287 11.3374C3.52521 14.2978 6.42199 16.0312 9.00063 15.9992C11.5793 16.0312 14.476 14.2978 16.3284 11.3374C18.8384 7.32376 18.4821 2.57438 15.5266 0.730524ZM3.03161 10.4905C2.51329 9.67028 2.12232 8.77644 1.87199 7.83937C1.6494 7.02621 1.56431 6.18166 1.62023 5.34054C1.73237 3.85769 2.33736 2.70233 3.32304 2.08695C4.30872 1.47157 5.61178 1.43577 6.99951 1.98489C7.21046 2.06958 7.41696 2.1649 7.61823 2.2705C6.8469 2.972 6.17632 3.7765 5.62551 4.66118C4.09969 7.09833 3.63431 9.81117 4.16682 11.9726C3.74261 11.5153 3.36252 11.0191 3.03161 10.4905ZM16.13 7.83937C15.8794 8.77632 15.4884 9.67012 14.9704 10.4905C14.6396 11.0201 14.2595 11.5174 13.8352 11.9757C14.3112 10.0374 13.9878 7.66344 12.8205 5.4365C12.802 5.39967 12.776 5.36715 12.7441 5.34104C12.7122 5.31494 12.6751 5.29584 12.6353 5.28499C12.5955 5.27414 12.5538 5.27178 12.5131 5.27807C12.4723 5.28436 12.4333 5.29916 12.3986 5.3215L8.75955 7.59185C8.72638 7.61245 8.69761 7.63939 8.6749 7.67112C8.65218 7.70284 8.63597 7.73873 8.62718 7.77673C8.61839 7.81473 8.6172 7.85408 8.62368 7.89254C8.63016 7.931 8.64419 7.9678 8.66494 8.00083L9.19898 8.85307C9.21962 8.88618 9.2466 8.9149 9.27839 8.93758C9.31017 8.96025 9.34612 8.97644 9.38418 8.98521C9.42224 8.99399 9.46167 8.99517 9.50019 8.9887C9.53871 8.98223 9.57557 8.96824 9.60867 8.94751L11.9676 7.47685C12.0439 7.70533 12.1202 7.93 12.1774 8.16229C12.3999 8.97547 12.485 9.82 12.4291 10.6611C12.317 12.1447 11.712 13.3001 10.7263 13.9155C10.2179 14.2258 9.63537 14.3941 9.03953 14.4029H8.96324C8.36738 14.3943 7.78478 14.226 7.27645 13.9155C6.29 13.3001 5.68502 12.1447 5.57287 10.6611C5.51741 9.82001 5.60249 8.97554 5.82463 8.16229C6.33618 6.28117 7.39305 4.59284 8.86254 3.30933C9.49632 2.75373 10.2182 2.30718 10.9987 1.98794C12.3826 1.43882 13.688 1.47461 14.6744 2.08999C15.6608 2.70537 16.2651 3.86073 16.3772 5.34358C16.4339 6.18344 16.3504 7.02686 16.13 7.83937Z",
    fill: "#2B2B2B"
  })), (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__.__)('Get Started', 'godaddy-launch')), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("h3", {
    className: "publish-guide-tooltip__title"
  }, tooltip.title), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("div", {
    className: "publish-guide-tooltip__description"
  }, tooltip.description), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_wordpress_components__WEBPACK_IMPORTED_MODULE_2__.Icon, {
    className: "publish-guide-tooltip__close",
    icon: _wordpress_icons__WEBPACK_IMPORTED_MODULE_3__["default"],
    onClick: closeCallback
  }));
}

/***/ }),

/***/ "./src/common/utils/instrumentation.js":
/*!*********************************************!*\
  !*** ./src/common/utils/instrumentation.js ***!
  \*********************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "trfq": function() { return /* binding */ trfq; }
/* harmony export */ });
const trfq = window._trfq || [];

/***/ }),

/***/ "./src/live-site-control/coming-soon-template.js":
/*!*******************************************************!*\
  !*** ./src/live-site-control/coming-soon-template.js ***!
  \*******************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @wordpress/i18n */ "@wordpress/i18n");
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__);



const ComingSoonTemplate = () => {
  return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("div", {
    className: "gdl-coming-soon-template-preview__container"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("div", {
    className: "gdl-coming-soon-template-preview__content"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("p", {
    className: "gdl-coming-soon-template-preview__title"
  }, (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__.__)('Great things are coming soon', 'godaddy-launch')), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("p", {
    className: "gdl-coming-soon-template-preview__subtext"
  }, (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__.__)('Stay tuned', 'godaddy-launch'))));
};

/* harmony default export */ __webpack_exports__["default"] = (ComingSoonTemplate);

/***/ }),

/***/ "./src/live-site-control/live-site-confirm-modal.js":
/*!**********************************************************!*\
  !*** ./src/live-site-control/live-site-confirm-modal.js ***!
  \**********************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @wordpress/i18n */ "@wordpress/i18n");
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _wordpress_components__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @wordpress/components */ "@wordpress/components");
/* harmony import */ var _wordpress_components__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_wordpress_components__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _illustration_rocket_png__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./illustration-rocket.png */ "./src/live-site-control/illustration-rocket.png");
/* harmony import */ var _common_utils_instrumentation__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../common/utils/instrumentation */ "./src/common/utils/instrumentation.js");
/* harmony import */ var _common_components_eid_wrapper__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../common/components/eid-wrapper */ "./src/common/components/eid-wrapper.js");


/**
 * WordPress dependencies.
 */



/**
 * Internal dependencies.
 */





const LaunchLiveSiteConfirmModal = _ref => {
  let {
    closeModal,
    completeStep
  } = _ref;
  (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.useEffect)(() => {
    _common_utils_instrumentation__WEBPACK_IMPORTED_MODULE_4__.trfq.push(['cmdLogEvent', 'impression', `${_common_components_eid_wrapper__WEBPACK_IMPORTED_MODULE_5__.EID_PREFIX}.launch/modal.finish.impression`]);
  }, []);
  return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_wordpress_components__WEBPACK_IMPORTED_MODULE_2__.Modal, {
    className: "gdl-launch-site-confirm-modal godaddy-styles",
    isDismissible: false
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("div", {
    className: "gdl-launch-site-confirm-modal__icon-container"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("img", {
    alt: "",
    src: _illustration_rocket_png__WEBPACK_IMPORTED_MODULE_3__
  })), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("p", {
    className: "gdl-launch-site-confirm-modal__header"
  }, (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__.__)('Ready to launch?', 'godaddy-launch')), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("div", {
    className: "gdl-launch-site-confirm-modal__list-container"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("p", {
    className: "gdl-launch-site-confirm-modal__list-container__list-header"
  }, (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__.__)('After your site is launched, it will be discoverable by search engines and available to the world.', 'godaddy-launch'), " ")), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("div", {
    className: "gdl-launch-site-confirm-modal__cta-container"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_common_components_eid_wrapper__WEBPACK_IMPORTED_MODULE_5__.EidWrapper, {
    action: "click",
    section: "launch/modal/finish/choices",
    target: "yes"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_wordpress_components__WEBPACK_IMPORTED_MODULE_2__.Button, {
    className: "live-site-confirm-modal-success",
    onClick: completeStep,
    variant: "primary"
  }, (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__.__)('Yes, launch my site', 'godaddy-launch'))), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_common_components_eid_wrapper__WEBPACK_IMPORTED_MODULE_5__.EidWrapper, {
    action: "click",
    section: "launch/modal/finish/choices",
    target: "no"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_wordpress_components__WEBPACK_IMPORTED_MODULE_2__.Button, {
    onClick: closeModal,
    variant: "secondary"
  }, (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__.__)('No, not yet', 'godaddy-launch')))));
};

/* harmony default export */ __webpack_exports__["default"] = (LaunchLiveSiteConfirmModal);

/***/ }),

/***/ "./src/live-site-control/live-site-launch-success-modal.js":
/*!*****************************************************************!*\
  !*** ./src/live-site-control/live-site-launch-success-modal.js ***!
  \*****************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @wordpress/i18n */ "@wordpress/i18n");
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _wordpress_compose__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @wordpress/compose */ "@wordpress/compose");
/* harmony import */ var _wordpress_compose__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_wordpress_compose__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _wordpress_data__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @wordpress/data */ "@wordpress/data");
/* harmony import */ var _wordpress_data__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_wordpress_data__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _wordpress_components__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @wordpress/components */ "@wordpress/components");
/* harmony import */ var _wordpress_components__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_wordpress_components__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _wordpress_icons__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @wordpress/icons */ "./node_modules/@wordpress/icons/build-module/icon/index.js");
/* harmony import */ var _wordpress_icons__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @wordpress/icons */ "./node_modules/@wordpress/icons/build-module/library/pages.js");


/**
 * WordPress dependencies
 */






/**
 * External dependencies
 */

const Confetti = (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.lazy)(() => __webpack_require__.e(/*! import() */ "vendors-node_modules_react-confetti_dist_react-confetti_min_js").then(__webpack_require__.t.bind(__webpack_require__, /*! react-confetti */ "./node_modules/react-confetti/dist/react-confetti.min.js", 23)));

const LiveSiteLaunchSuccessModal = _ref => {
  let {
    closeModal
  } = _ref;
  const [showConfetti, setShowConfetti] = (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.useState)(true);
  const [copyText, setCopyText] = (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.useState)((0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__.__)('Copy the URL', 'godaddy-launch'));
  const {
    url
  } = (0,_wordpress_data__WEBPACK_IMPORTED_MODULE_3__.useSelect)(select => {
    var _select$getSite;

    return {
      url: (_select$getSite = select('core').getSite()) === null || _select$getSite === void 0 ? void 0 : _select$getSite.url
    };
  });
  const ref = (0,_wordpress_compose__WEBPACK_IMPORTED_MODULE_2__.useCopyToClipboard)(url, () => setCopyText((0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__.__)('Copied!', 'godaddy-launch')));
  return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_wordpress_components__WEBPACK_IMPORTED_MODULE_4__.Modal, {
    className: "gdl-launch-site-success-modal godaddy-styles",
    onRequestClose: closeModal,
    title: (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__.__)('Good work! Your site is live.', 'godaddy-launch')
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("p", {
    className: "gdl-launch-site-success-modal__description"
  }, (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__.__)('Show it off to all of your family, friends, and customers.', 'godaddy-launch')), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("div", {
    className: "gdl-launch-site-success-modal__content"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("div", {
    className: "gdl-launch-site-success-modal__site-description-container"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("div", {
    className: "gdl-launch-site-success-modal__site-description-container__icon-container border-right"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("span", {
    className: "dashicons dashicons-admin-site-alt3"
  })), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("p", {
    className: "gdl-launch-site-success-modal__site-description-container__site-name"
  }, url)), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_wordpress_components__WEBPACK_IMPORTED_MODULE_4__.Button, {
    icon: (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_wordpress_icons__WEBPACK_IMPORTED_MODULE_5__["default"], {
      icon: _wordpress_icons__WEBPACK_IMPORTED_MODULE_6__["default"]
    }),
    isSecondary: true,
    ref: ref
  }, copyText)), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("div", {
    className: "gdl-launch-site-success-modal__cta-container"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_wordpress_components__WEBPACK_IMPORTED_MODULE_4__.Button, {
    href: url,
    isPrimary: true,
    target: "_blank"
  }, (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__.__)('View Site', 'godaddy-launch'))), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.Suspense, {
    fallback: null
  }, showConfetti && (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createPortal)((0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(Confetti, {
    gravity: 0.2,
    initialVelocityY: 20,
    numberOfPieces: 2000,
    onConfettiComplete: () => setShowConfetti(false),
    recycle: false,
    style: {
      zIndex: 99999
    },
    wind: 0.05
  }), document.body)));
};

/* harmony default export */ __webpack_exports__["default"] = (LiveSiteLaunchSuccessModal);

/***/ }),

/***/ "./src/live-site-control/live-site-now-modal.js":
/*!******************************************************!*\
  !*** ./src/live-site-control/live-site-now-modal.js ***!
  \******************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @wordpress/i18n */ "@wordpress/i18n");
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _wordpress_components__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @wordpress/components */ "@wordpress/components");
/* harmony import */ var _wordpress_components__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_wordpress_components__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _coming_soon_template__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./coming-soon-template */ "./src/live-site-control/coming-soon-template.js");
/* harmony import */ var _live_site_option__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./live-site-option */ "./src/live-site-control/live-site-option.js");
/* harmony import */ var _common_utils_instrumentation__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../common/utils/instrumentation */ "./src/common/utils/instrumentation.js");
/* harmony import */ var _common_components_eid_wrapper__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../common/components/eid-wrapper */ "./src/common/components/eid-wrapper.js");


/**
 * WordPress dependencies.
 */



/**
 * Internal dependencies.
 */






const LaunchSiteNowModal = _ref => {
  let {
    completeStep
  } = _ref;
  const [launchSite, setLaunchSite] = (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.useState)(null);
  (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.useEffect)(() => {
    _common_utils_instrumentation__WEBPACK_IMPORTED_MODULE_5__.trfq.push(['cmdLogEvent', 'impression', `${_common_components_eid_wrapper__WEBPACK_IMPORTED_MODULE_6__.EID_PREFIX}.launch/modal.start.impression`]);
  }, []);
  return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_wordpress_components__WEBPACK_IMPORTED_MODULE_2__.Modal, {
    className: "gdl-live-site-control-modal godaddy-styles",
    isDismissible: false
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("div", {
    className: "gdl-live-site-control-modal__container"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("p", {
    className: "gdl-live-site-control-modal__header"
  }, (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__.__)('When should we launch your website for visitors?', 'godaddy-launch')), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("div", {
    className: "gdl-live-site-control-modal__options"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_common_components_eid_wrapper__WEBPACK_IMPORTED_MODULE_6__.EidWrapper, {
    action: "click",
    section: "launch/modal/start/choices",
    target: "launch_now"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_live_site_option__WEBPACK_IMPORTED_MODULE_4__["default"], {
    description: (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__.__)('Visitors can see your site now and any edits you make', 'godaddy-launch'),
    isSelected: launchSite === true,
    onSelect: () => setLaunchSite(true),
    preview: window.location.protocol + '//' + window.location.hostname,
    title: (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__.__)('Launch Now', 'godaddy-launch')
  })), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_common_components_eid_wrapper__WEBPACK_IMPORTED_MODULE_6__.EidWrapper, {
    action: "click",
    section: "launch/modal/start/choices",
    target: "launch_later"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_live_site_option__WEBPACK_IMPORTED_MODULE_4__["default"], {
    description: (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__.__)('Visitors will see a coming soon page until you go live', 'godaddy-launch'),
    isSelected: launchSite === false,
    onSelect: () => setLaunchSite(false),
    preview: _coming_soon_template__WEBPACK_IMPORTED_MODULE_3__["default"],
    title: (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__.__)('Launch Later', 'godaddy-launch')
  }))), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("div", {
    className: "gdl-live-site-control-modal__cta-container"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_common_components_eid_wrapper__WEBPACK_IMPORTED_MODULE_6__.EidWrapper, {
    action: "click",
    section: "launch/modal/start",
    target: "continue"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_wordpress_components__WEBPACK_IMPORTED_MODULE_2__.Button, {
    disabled: launchSite === null,
    onClick: () => completeStep(launchSite),
    variant: "primary"
  }, (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__.__)('Continue', 'godaddy-launch'))))));
};

/* harmony default export */ __webpack_exports__["default"] = (LaunchSiteNowModal);

/***/ }),

/***/ "./src/live-site-control/live-site-option.js":
/*!***************************************************!*\
  !*** ./src/live-site-control/live-site-option.js ***!
  \***************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @wordpress/i18n */ "@wordpress/i18n");
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _wordpress_url__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @wordpress/url */ "@wordpress/url");
/* harmony import */ var _wordpress_url__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_wordpress_url__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _wordpress_components__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @wordpress/components */ "@wordpress/components");
/* harmony import */ var _wordpress_components__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_wordpress_components__WEBPACK_IMPORTED_MODULE_3__);


/* global gdlLiveSiteControlData */

/**
 * WordPress dependencies.
 */





const LiveSiteOption = _ref => {
  let {
    title,
    description,
    onSelect,
    isSelected,
    preview
  } = _ref;
  const [isIframeLoaded, setIframeLoaded] = (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.useState)(false);
  const isComponent = typeof preview === 'function';
  return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("div", {
    className: `gdl-live-site-option__container`,
    onClick: onSelect,
    onKeyDown: onSelect,
    role: "button",
    tabIndex: "0"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("div", {
    className: `gdl-live-site-option__image-container ${isSelected ? 'selected' : ''}`
  }, !isIframeLoaded && !isComponent && (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_wordpress_components__WEBPACK_IMPORTED_MODULE_3__.Spinner, null), isComponent ? preview() : (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("div", {
    className: "gdl-live-site-option__image-container__scale"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("div", {
    className: "gdl-live-site-option__image-container__content"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("iframe", {
    frameBorder: "0",
    height: "100%",
    onLoad: () => setIframeLoaded(true),
    src: (0,_wordpress_url__WEBPACK_IMPORTED_MODULE_2__.addQueryArgs)(preview, {
      [gdlLiveSiteControlData.previewArg]: true
    }),
    title: (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__.__)('Site Preview', 'godaddy-launch'),
    width: "100%"
  })))), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("p", {
    className: "gdl-live-site-option__title"
  }, title), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("p", {
    className: "gdl-live-site-option__description"
  }, description));
};

/* harmony default export */ __webpack_exports__["default"] = (LiveSiteOption);

/***/ }),

/***/ "./src/live-site-control/live-site-publish-guide-button.js":
/*!*****************************************************************!*\
  !*** ./src/live-site-control/live-site-publish-guide-button.js ***!
  \*****************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* binding */ LiveSiteDashboardNotice; }
/* harmony export */ });
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @wordpress/i18n */ "@wordpress/i18n");
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _wordpress_components__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @wordpress/components */ "@wordpress/components");
/* harmony import */ var _wordpress_components__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_wordpress_components__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _common_components_eid_wrapper__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../common/components/eid-wrapper */ "./src/common/components/eid-wrapper.js");


/**
 * WordPress dependencies.
 */



/**
 * Internal dependencies
 */


function LiveSiteDashboardNotice(_ref) {
  let {
    launchWorkflow
  } = _ref;
  const [publishGuideTriggerNode, setPublishGuideTriggerNode] = (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.useState)(null);
  const classSelector = '.publish-guide-popover__footer';
  (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.useEffect)(() => {
    let newPublishGuideTrigger = document.querySelector(classSelector);

    if (!newPublishGuideTrigger) {
      setTimeout(() => {
        newPublishGuideTrigger = document.querySelector(classSelector);
        setPublishGuideTriggerNode(newPublishGuideTrigger);
      });
    } else {
      setPublishGuideTriggerNode(newPublishGuideTrigger);
    }
  }, []);

  if (!publishGuideTriggerNode) {
    return null;
  }

  return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createPortal)((0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)("div", {
    className: "postbox wrap gdl-live-site-button__container"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_common_components_eid_wrapper__WEBPACK_IMPORTED_MODULE_3__.EidWrapper, {
    action: "click",
    section: "guide",
    target: "launch"
  }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_wordpress_components__WEBPACK_IMPORTED_MODULE_2__.Button, {
    isPrimary: true,
    onClick: launchWorkflow
  }, (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__.__)('Launch my site', 'godaddy-launch')))), publishGuideTriggerNode);
}

/***/ }),

/***/ "./src/live-site-control/index.scss":
/*!******************************************!*\
  !*** ./src/live-site-control/index.scss ***!
  \******************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
// extracted by mini-css-extract-plugin


/***/ }),

/***/ "./src/live-site-control/illustration-rocket.png":
/*!*******************************************************!*\
  !*** ./src/live-site-control/illustration-rocket.png ***!
  \*******************************************************/
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

module.exports = __webpack_require__.p + "images/illustration-rocket.d730dedc.png";

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "React" ***!
  \************************/
/***/ (function(module) {

module.exports = window["React"];

/***/ }),

/***/ "@wordpress/components":
/*!************************************!*\
  !*** external ["wp","components"] ***!
  \************************************/
/***/ (function(module) {

module.exports = window["wp"]["components"];

/***/ }),

/***/ "@wordpress/compose":
/*!*********************************!*\
  !*** external ["wp","compose"] ***!
  \*********************************/
/***/ (function(module) {

module.exports = window["wp"]["compose"];

/***/ }),

/***/ "@wordpress/core-data":
/*!**********************************!*\
  !*** external ["wp","coreData"] ***!
  \**********************************/
/***/ (function(module) {

module.exports = window["wp"]["coreData"];

/***/ }),

/***/ "@wordpress/data":
/*!******************************!*\
  !*** external ["wp","data"] ***!
  \******************************/
/***/ (function(module) {

module.exports = window["wp"]["data"];

/***/ }),

/***/ "@wordpress/dom-ready":
/*!**********************************!*\
  !*** external ["wp","domReady"] ***!
  \**********************************/
/***/ (function(module) {

module.exports = window["wp"]["domReady"];

/***/ }),

/***/ "@wordpress/element":
/*!*********************************!*\
  !*** external ["wp","element"] ***!
  \*********************************/
/***/ (function(module) {

module.exports = window["wp"]["element"];

/***/ }),

/***/ "@wordpress/i18n":
/*!******************************!*\
  !*** external ["wp","i18n"] ***!
  \******************************/
/***/ (function(module) {

module.exports = window["wp"]["i18n"];

/***/ }),

/***/ "@wordpress/primitives":
/*!************************************!*\
  !*** external ["wp","primitives"] ***!
  \************************************/
/***/ (function(module) {

module.exports = window["wp"]["primitives"];

/***/ }),

/***/ "@wordpress/url":
/*!*****************************!*\
  !*** external ["wp","url"] ***!
  \*****************************/
/***/ (function(module) {

module.exports = window["wp"]["url"];

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = __webpack_modules__;
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/compat get default export */
/******/ 	!function() {
/******/ 		// getDefaultExport function for compatibility with non-harmony modules
/******/ 		__webpack_require__.n = function(module) {
/******/ 			var getter = module && module.__esModule ?
/******/ 				function() { return module['default']; } :
/******/ 				function() { return module; };
/******/ 			__webpack_require__.d(getter, { a: getter });
/******/ 			return getter;
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/create fake namespace object */
/******/ 	!function() {
/******/ 		var getProto = Object.getPrototypeOf ? function(obj) { return Object.getPrototypeOf(obj); } : function(obj) { return obj.__proto__; };
/******/ 		var leafPrototypes;
/******/ 		// create a fake namespace object
/******/ 		// mode & 1: value is a module id, require it
/******/ 		// mode & 2: merge all properties of value into the ns
/******/ 		// mode & 4: return value when already ns object
/******/ 		// mode & 16: return value when it's Promise-like
/******/ 		// mode & 8|1: behave like require
/******/ 		__webpack_require__.t = function(value, mode) {
/******/ 			if(mode & 1) value = this(value);
/******/ 			if(mode & 8) return value;
/******/ 			if(typeof value === 'object' && value) {
/******/ 				if((mode & 4) && value.__esModule) return value;
/******/ 				if((mode & 16) && typeof value.then === 'function') return value;
/******/ 			}
/******/ 			var ns = Object.create(null);
/******/ 			__webpack_require__.r(ns);
/******/ 			var def = {};
/******/ 			leafPrototypes = leafPrototypes || [null, getProto({}), getProto([]), getProto(getProto)];
/******/ 			for(var current = mode & 2 && value; typeof current == 'object' && !~leafPrototypes.indexOf(current); current = getProto(current)) {
/******/ 				Object.getOwnPropertyNames(current).forEach(function(key) { def[key] = function() { return value[key]; }; });
/******/ 			}
/******/ 			def['default'] = function() { return value; };
/******/ 			__webpack_require__.d(ns, def);
/******/ 			return ns;
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	!function() {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = function(exports, definition) {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/ensure chunk */
/******/ 	!function() {
/******/ 		__webpack_require__.f = {};
/******/ 		// This file contains only the entry chunk.
/******/ 		// The chunk loading function for additional chunks
/******/ 		__webpack_require__.e = function(chunkId) {
/******/ 			return Promise.all(Object.keys(__webpack_require__.f).reduce(function(promises, key) {
/******/ 				__webpack_require__.f[key](chunkId, promises);
/******/ 				return promises;
/******/ 			}, []));
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/get javascript chunk filename */
/******/ 	!function() {
/******/ 		// This function allow to reference async chunks
/******/ 		__webpack_require__.u = function(chunkId) {
/******/ 			// return url for filenames based on template
/******/ 			return "" + chunkId + ".js";
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/get mini-css chunk filename */
/******/ 	!function() {
/******/ 		// This function allow to reference async chunks
/******/ 		__webpack_require__.miniCssF = function(chunkId) {
/******/ 			// return url for filenames based on template
/******/ 			return undefined;
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/global */
/******/ 	!function() {
/******/ 		__webpack_require__.g = (function() {
/******/ 			if (typeof globalThis === 'object') return globalThis;
/******/ 			try {
/******/ 				return this || new Function('return this')();
/******/ 			} catch (e) {
/******/ 				if (typeof window === 'object') return window;
/******/ 			}
/******/ 		})();
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	!function() {
/******/ 		__webpack_require__.o = function(obj, prop) { return Object.prototype.hasOwnProperty.call(obj, prop); }
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/load script */
/******/ 	!function() {
/******/ 		var inProgress = {};
/******/ 		var dataWebpackPrefix = "godaddy-launch:";
/******/ 		// loadScript function to load a script via script tag
/******/ 		__webpack_require__.l = function(url, done, key, chunkId) {
/******/ 			if(inProgress[url]) { inProgress[url].push(done); return; }
/******/ 			var script, needAttach;
/******/ 			if(key !== undefined) {
/******/ 				var scripts = document.getElementsByTagName("script");
/******/ 				for(var i = 0; i < scripts.length; i++) {
/******/ 					var s = scripts[i];
/******/ 					if(s.getAttribute("src") == url || s.getAttribute("data-webpack") == dataWebpackPrefix + key) { script = s; break; }
/******/ 				}
/******/ 			}
/******/ 			if(!script) {
/******/ 				needAttach = true;
/******/ 				script = document.createElement('script');
/******/ 		
/******/ 				script.charset = 'utf-8';
/******/ 				script.timeout = 120;
/******/ 				if (__webpack_require__.nc) {
/******/ 					script.setAttribute("nonce", __webpack_require__.nc);
/******/ 				}
/******/ 				script.setAttribute("data-webpack", dataWebpackPrefix + key);
/******/ 				script.src = url;
/******/ 			}
/******/ 			inProgress[url] = [done];
/******/ 			var onScriptComplete = function(prev, event) {
/******/ 				// avoid mem leaks in IE.
/******/ 				script.onerror = script.onload = null;
/******/ 				clearTimeout(timeout);
/******/ 				var doneFns = inProgress[url];
/******/ 				delete inProgress[url];
/******/ 				script.parentNode && script.parentNode.removeChild(script);
/******/ 				doneFns && doneFns.forEach(function(fn) { return fn(event); });
/******/ 				if(prev) return prev(event);
/******/ 			}
/******/ 			;
/******/ 			var timeout = setTimeout(onScriptComplete.bind(null, undefined, { type: 'timeout', target: script }), 120000);
/******/ 			script.onerror = onScriptComplete.bind(null, script.onerror);
/******/ 			script.onload = onScriptComplete.bind(null, script.onload);
/******/ 			needAttach && document.head.appendChild(script);
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	!function() {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = function(exports) {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/publicPath */
/******/ 	!function() {
/******/ 		var scriptUrl;
/******/ 		if (__webpack_require__.g.importScripts) scriptUrl = __webpack_require__.g.location + "";
/******/ 		var document = __webpack_require__.g.document;
/******/ 		if (!scriptUrl && document) {
/******/ 			if (document.currentScript)
/******/ 				scriptUrl = document.currentScript.src
/******/ 			if (!scriptUrl) {
/******/ 				var scripts = document.getElementsByTagName("script");
/******/ 				if(scripts.length) scriptUrl = scripts[scripts.length - 1].src
/******/ 			}
/******/ 		}
/******/ 		// When supporting browsers where an automatic publicPath is not supported you must specify an output.publicPath manually via configuration
/******/ 		// or pass an empty string ("") and set the __webpack_public_path__ variable from your code to use your own logic.
/******/ 		if (!scriptUrl) throw new Error("Automatic publicPath is not supported in this browser");
/******/ 		scriptUrl = scriptUrl.replace(/#.*$/, "").replace(/\?.*$/, "").replace(/\/[^\/]+$/, "/");
/******/ 		__webpack_require__.p = scriptUrl;
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/jsonp chunk loading */
/******/ 	!function() {
/******/ 		// no baseURI
/******/ 		
/******/ 		// object to store loaded and loading chunks
/******/ 		// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 		// [resolve, reject, Promise] = chunk loading, 0 = chunk loaded
/******/ 		var installedChunks = {
/******/ 			"live-site-control": 0
/******/ 		};
/******/ 		
/******/ 		__webpack_require__.f.j = function(chunkId, promises) {
/******/ 				// JSONP chunk loading for javascript
/******/ 				var installedChunkData = __webpack_require__.o(installedChunks, chunkId) ? installedChunks[chunkId] : undefined;
/******/ 				if(installedChunkData !== 0) { // 0 means "already installed".
/******/ 		
/******/ 					// a Promise means "currently loading".
/******/ 					if(installedChunkData) {
/******/ 						promises.push(installedChunkData[2]);
/******/ 					} else {
/******/ 						if(true) { // all chunks have JS
/******/ 							// setup Promise in chunk cache
/******/ 							var promise = new Promise(function(resolve, reject) { installedChunkData = installedChunks[chunkId] = [resolve, reject]; });
/******/ 							promises.push(installedChunkData[2] = promise);
/******/ 		
/******/ 							// start chunk loading
/******/ 							var url = __webpack_require__.p + __webpack_require__.u(chunkId);
/******/ 							// create error before stack unwound to get useful stacktrace later
/******/ 							var error = new Error();
/******/ 							var loadingEnded = function(event) {
/******/ 								if(__webpack_require__.o(installedChunks, chunkId)) {
/******/ 									installedChunkData = installedChunks[chunkId];
/******/ 									if(installedChunkData !== 0) installedChunks[chunkId] = undefined;
/******/ 									if(installedChunkData) {
/******/ 										var errorType = event && (event.type === 'load' ? 'missing' : event.type);
/******/ 										var realSrc = event && event.target && event.target.src;
/******/ 										error.message = 'Loading chunk ' + chunkId + ' failed.\n(' + errorType + ': ' + realSrc + ')';
/******/ 										error.name = 'ChunkLoadError';
/******/ 										error.type = errorType;
/******/ 										error.request = realSrc;
/******/ 										installedChunkData[1](error);
/******/ 									}
/******/ 								}
/******/ 							};
/******/ 							__webpack_require__.l(url, loadingEnded, "chunk-" + chunkId, chunkId);
/******/ 						} else installedChunks[chunkId] = 0;
/******/ 					}
/******/ 				}
/******/ 		};
/******/ 		
/******/ 		// no prefetching
/******/ 		
/******/ 		// no preloaded
/******/ 		
/******/ 		// no HMR
/******/ 		
/******/ 		// no HMR manifest
/******/ 		
/******/ 		// no on chunks loaded
/******/ 		
/******/ 		// install a JSONP callback for chunk loading
/******/ 		var webpackJsonpCallback = function(parentChunkLoadingFunction, data) {
/******/ 			var chunkIds = data[0];
/******/ 			var moreModules = data[1];
/******/ 			var runtime = data[2];
/******/ 			// add "moreModules" to the modules object,
/******/ 			// then flag all "chunkIds" as loaded and fire callback
/******/ 			var moduleId, chunkId, i = 0;
/******/ 			if(chunkIds.some(function(id) { return installedChunks[id] !== 0; })) {
/******/ 				for(moduleId in moreModules) {
/******/ 					if(__webpack_require__.o(moreModules, moduleId)) {
/******/ 						__webpack_require__.m[moduleId] = moreModules[moduleId];
/******/ 					}
/******/ 				}
/******/ 				if(runtime) var result = runtime(__webpack_require__);
/******/ 			}
/******/ 			if(parentChunkLoadingFunction) parentChunkLoadingFunction(data);
/******/ 			for(;i < chunkIds.length; i++) {
/******/ 				chunkId = chunkIds[i];
/******/ 				if(__webpack_require__.o(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 					installedChunks[chunkId][0]();
/******/ 				}
/******/ 				installedChunks[chunkId] = 0;
/******/ 			}
/******/ 		
/******/ 		}
/******/ 		
/******/ 		var chunkLoadingGlobal = self["webpackChunkgodaddy_launch"] = self["webpackChunkgodaddy_launch"] || [];
/******/ 		chunkLoadingGlobal.forEach(webpackJsonpCallback.bind(null, 0));
/******/ 		chunkLoadingGlobal.push = webpackJsonpCallback.bind(null, chunkLoadingGlobal.push.bind(chunkLoadingGlobal));
/******/ 	}();
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
!function() {
/*!****************************************!*\
  !*** ./src/live-site-control/index.js ***!
  \****************************************/
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @wordpress/i18n */ "@wordpress/i18n");
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _wordpress_dom_ready__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @wordpress/dom-ready */ "@wordpress/dom-ready");
/* harmony import */ var _wordpress_dom_ready__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_wordpress_dom_ready__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _wordpress_data__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @wordpress/data */ "@wordpress/data");
/* harmony import */ var _wordpress_data__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_wordpress_data__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _wordpress_core_data__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @wordpress/core-data */ "@wordpress/core-data");
/* harmony import */ var _wordpress_core_data__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_wordpress_core_data__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _common_components_gdl_tool_tip__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../common/components/gdl-tool-tip */ "./src/common/components/gdl-tool-tip.js");
/* harmony import */ var _live_site_confirm_modal__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./live-site-confirm-modal */ "./src/live-site-control/live-site-confirm-modal.js");
/* harmony import */ var _live_site_launch_success_modal__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./live-site-launch-success-modal */ "./src/live-site-control/live-site-launch-success-modal.js");
/* harmony import */ var _live_site_now_modal__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./live-site-now-modal */ "./src/live-site-control/live-site-now-modal.js");
/* harmony import */ var _live_site_publish_guide_button__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./live-site-publish-guide-button */ "./src/live-site-control/live-site-publish-guide-button.js");
/* harmony import */ var _index_scss__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./index.scss */ "./src/live-site-control/index.scss");


/* global gdlLiveSiteControlData */

/**
 * WordPress dependencies
 */





/**
 * Internal dependencies
 */







/**
 * To reset this plugin. Run this in the browser console:
 * wp.apiFetch( { path: '/wp/v2/settings', method: 'POST', data: { 'gdl_site_published': false } } );
 * wp.apiFetch( { path: '/wp/v2/settings', method: 'POST', data: { 'gdl_live_site_dismiss': false } } );
 *
 * You can also use the npm command: `npm run reset:live-site-control`
 */

const LiveSiteControl = () => {
  const {
    saveEditedEntityRecord
  } = (0,_wordpress_data__WEBPACK_IMPORTED_MODULE_3__.useDispatch)('core');
  const [tooltip, setTooltip] = (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.useState)(null);
  const [displayedTooltips, setDisplayedTooltips] = (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.useState)([]); // todo: fix this
  // eslint-disable-next-line no-unused-vars

  const [showPopOver, setShowPopOver] = (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.useState)(false);
  const [currentStep, setCurrentStep] = (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.useState)('choice');
  const [isModalDismissed, setIsModalDismissed] = (0,_wordpress_core_data__WEBPACK_IMPORTED_MODULE_4__.useEntityProp)('root', 'site', gdlLiveSiteControlData.settings.liveSiteDismiss); // todo: fix this
  // eslint-disable-next-line no-unused-vars

  const [sitePublish, setSitePublish] = (0,_wordpress_core_data__WEBPACK_IMPORTED_MODULE_4__.useEntityProp)('root', 'site', gdlLiveSiteControlData.settings.publishState); // todo: fix this
  // eslint-disable-next-line no-unused-vars

  const [blogPublic, setBlogPublic] = (0,_wordpress_core_data__WEBPACK_IMPORTED_MODULE_4__.useEntityProp)('root', 'site', gdlLiveSiteControlData.settings.blogPublic);

  const closeTooltip = () => {
    setTooltip(null);
  };

  const handleSetTooltip = newTooltip => {
    if (displayedTooltips.find(name => name === newTooltip)) {
      return null;
    }

    const activeTooltip = tooltips[newTooltip];

    if (!activeTooltip) {
      return null;
    }

    const tooltipTargetElement = document.getElementById(activeTooltip.refID);
    setTooltip({ ...activeTooltip,
      anchorRect: tooltipTargetElement.getBoundingClientRect()
    });
  };

  const closePublishGuide = () => {
    const publishGuideTriggerButton = document.getElementById('gdl-publish-guide-trigger-button');

    if (!publishGuideTriggerButton) {
      return;
    }

    publishGuideTriggerButton.click();
  };

  const tooltips = {
    launchLater: {
      autoHide: 10000,
      collapseSidePanel: false,
      description: (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__.__)('When you\'re ready for visitors, open the menu here and find the "Launch my site" button.', 'godaddy-launch'),
      refID: 'gdl-publish-guide-trigger-button',
      title: (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__.__)('Go live for the world to see', 'godaddy-launch')
    }
  };
  /**
   * Don't show popover if user has clicked on the launch my site button.
   */
  // todo: fix this
  // eslint-disable-next-line no-unused-vars

  const [forceHidePopOver, setForceHidePopOver] = (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.useState)(false);
  /**
   * When clicking on launch my site button we need to persist the modal even though isModalDismissed is true.
   */

  const [forceModalOpen, setForceModalOpen] = (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.useState)(false);
  const removeGlobalEventListeners = (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.useCallback)(() => {
    window.removeEventListener(gdlLiveSiteControlData.eventName, launchWorkflow);
  }, []);
  /**
   * This is used to catch the event triggered by the admin toolbar button Go Live.
   */

  (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.useEffect)(() => {
    window.addEventListener(gdlLiveSiteControlData.eventName, launchWorkflow);
    return removeGlobalEventListeners;
  }, []);
  const launchWorkflow = (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.useCallback)(() => {
    setCurrentStep('confirm');
    setForceModalOpen(true);
  }, []);
  const closeModal = (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.useCallback)(() => {
    setForceModalOpen(false);

    if (!isModalDismissed) {
      setIsModalDismissed(true);
      saveEditedEntityRecord('root', 'site');
    }
  }, [isModalDismissed]);
  const workflow = {
    choice: {
      Component: _live_site_now_modal__WEBPACK_IMPORTED_MODULE_8__["default"],
      props: {
        completeStep: launchSite => {
          if (launchSite === false) {
            closeModal();

            if (!forceHidePopOver) {
              setShowPopOver(true);
            }

            return;
          }

          setCurrentStep('confirm');
        }
      }
    },
    confirm: {
      Component: _live_site_confirm_modal__WEBPACK_IMPORTED_MODULE_6__["default"],
      props: {
        closeModal: () => {
          closeModal();
          handleSetTooltip('launchLater');
          setDisplayedTooltips([...displayedTooltips, 'launchLater']);
        },
        completeStep: () => {
          var _document$getElementB;

          setSitePublish(true);
          setBlogPublic(true); // Remove admin bar notice.

          (_document$getElementB = document.getElementById('wp-admin-bar-gdl-live-site')) === null || _document$getElementB === void 0 ? void 0 : _document$getElementB.remove();
          saveEditedEntityRecord('root', 'site');
          setCurrentStep('success');
        }
      }
    },
    success: {
      Component: _live_site_launch_success_modal__WEBPACK_IMPORTED_MODULE_7__["default"],
      props: {
        closeModal: () => {
          closeModal();
          (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.unmountComponentAtNode)(document.getElementById(gdlLiveSiteControlData.appContainerClass));
          removeGlobalEventListeners();
        }
      }
    }
  };

  if (typeof isModalDismissed === 'undefined') {
    return null;
  }

  const WorkflowStep = () => {
    if (!(forceModalOpen || !isModalDismissed)) {
      return null;
    }

    const currentWorkflowStep = workflow[currentStep];
    const DerivedComponent = currentWorkflowStep === null || currentWorkflowStep === void 0 ? void 0 : currentWorkflowStep.Component;
    const workflowProps = currentWorkflowStep === null || currentWorkflowStep === void 0 ? void 0 : currentWorkflowStep.props;
    return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(DerivedComponent, workflowProps);
  };

  const PublishGuideButton = () => {
    return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_live_site_publish_guide_button__WEBPACK_IMPORTED_MODULE_9__["default"], {
      launchWorkflow: () => {
        closePublishGuide();
        launchWorkflow();
      }
    });
  };

  const RenderTooltip = () => {
    if (!tooltip) {
      return null;
    }

    return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createPortal)((0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_common_components_gdl_tool_tip__WEBPACK_IMPORTED_MODULE_5__["default"], {
      closeCallback: closeTooltip,
      tooltip: tooltip
    }), document.body);
  };

  return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, WorkflowStep(), PublishGuideButton(), RenderTooltip());
};

_wordpress_dom_ready__WEBPACK_IMPORTED_MODULE_2___default()(() => {
  (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.render)((0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(LiveSiteControl, null), document.getElementById(gdlLiveSiteControlData.appContainerClass));
});
}();
/******/ })()
;
//# sourceMappingURL=live-site-control.js.map