/**
 * Login to our test WordPress site
 */
export function loginToSite() {
	goTo( '/wp-admin/post-new.php?post_type=post' )
		.then( ( window ) => {
			if ( window.location.pathname === '/wp-login.php' ) {
			// WordPress has a wp_attempt_focus() function that fires 200ms after the wp-login.php page loads.
			// We need to wait a short time before trying to login.
				cy.wait( 250 );

				cy.get( '#user_login' ).type( Cypress.env( 'wpUsername' ) );
				cy.get( '#user_pass' ).type( Cypress.env( 'wpPassword' ) );
				cy.get( '#wp-submit' ).click();
			}
		} );

	cy.get( '.block-editor-page' ).should( 'exist' );
}

/**
 * Go to a specific URI.
 *
 * @param {string} path The URI path to go to.
 */
export function goTo( path = '/wp-admin' ) {
	cy.visit( Cypress.env( 'testURL' ) + path );

	return getWindowObject();
}

/**
 * Safely obtain the window object or error
 * when the window object is not available.
 */
export function getWindowObject() {
	const editorUrlStrings = [ 'post-new.php', 'action=edit' ];
	return cy.window().then( ( win ) => {
		const isEditorPage = editorUrlStrings.filter( ( str ) => win.location.href.includes( str ) );

		if ( isEditorPage.length === 0 ) {
			throw new Error( 'Check the previous test, window property was invoked outside of Editor.' );
		}

		if ( ! win?.wp ) {
			throw new Error( 'Window property was invoked within Editor but `win.wp` is not defined.' );
		}

		return win;
	} );
}

/**
 * Disable Gutenberg Tips
 */
export function disableGutenbergFeatures() {
	getWindowObject().then( ( safeWin ) => {
		// Enable "Top Toolbar"
		if ( ! safeWin.wp.data.select( 'core/edit-post' ).isFeatureActive( 'fixedToolbar' ) ) {
			safeWin.wp.data.dispatch( 'core/edit-post' ).toggleFeature( 'fixedToolbar' );
		}

		if ( ! safeWin.wp.data.select( 'core/edit-post' ).isFeatureActive( 'welcomeGuide' ) ) {
			return;
		}

		safeWin.wp.data.dispatch( 'core/edit-post' ).toggleFeature( 'welcomeGuide' );
		safeWin.wp.data.dispatch( 'core/editor' ).disablePublishSidebar();
	} );
}
