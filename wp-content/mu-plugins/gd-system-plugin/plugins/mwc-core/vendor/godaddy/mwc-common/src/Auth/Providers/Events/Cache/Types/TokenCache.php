<?php

namespace GoDaddy\WordPress\MWC\Common\Auth\Providers\Events\Cache\Types;

use GoDaddy\WordPress\MWC\Common\Cache\Cache;

/**
 * Managed WooCommerce site access token cache handler class.
 */
class TokenCache extends Cache
{
    /** @var int how long in seconds should the cache be kept for */
    protected $expires = 300;

    /**
     * Constructor.
     *
     * @param int|null $userId
     */
    final public function __construct(int $userId = null)
    {
        $this->type('events_jwt');
        $this->key($userId ? "events_jwt_{$userId}" : 'events_jwt_site');
    }

    /**
     * Creates a new MWC token cache for a given user ID.
     *
     * @param int|null $userId
     * @return TokenCache
     */
    public static function for(int $userId = null) : TokenCache
    {
        return new static($userId);
    }
}
