<?php

namespace GoDaddy\WordPress\MWC\Common\Http;

use Exception;
use GoDaddy\WordPress\MWC\Common\Auth\Contracts\AuthMethodContract;
use GoDaddy\WordPress\MWC\Common\Helpers\ArrayHelper;
use GoDaddy\WordPress\MWC\Common\Http\Contracts\RequestContract;
use GoDaddy\WordPress\MWC\Common\Repositories\ManagedWooCommerceRepository;
use GoDaddy\WordPress\MWC\Common\Repositories\WordPress\HttpRepository;
use stdClass;

/**
 * HTTP Request handler.
 */
class Request implements RequestContract
{
    /** @var array request body */
    public $body;

    /** @var array request headers */
    public $headers;

    /** @var string request method */
    public $method;

    /** @var object|array request query parameters */
    public $query;

    /** @var bool whether should verify SSL */
    public $sslVerify;

    /** @var int default timeout in seconds */
    public $timeout;

    /** @var string the URL to send the request to */
    public $url;

    /** @var array allowed request method types */
    protected $allowedMethodTypes = ['GET', 'POST', 'HEAD', 'PUT', 'DELETE', 'TRACE', 'OPTIONS', 'PATCH'];

    /** @var string default allowed method */
    protected $defaultAllowedMethod = 'get';

    /** @var stdClass the type of response the request should return */
    protected $responseClass = Response::class;

    /** @var AuthMethodContract|null The authentication method for this request. */
    protected $authMethod;

    /**
     * Request constructor.
     *
     * @param string|null $url
     */
    public function __construct(string $url = null)
    {
        $this->setHeaders()
             ->setMethod()
             ->sslVerify()
             ->setTimeout();

        if ($url) {
            $this->setUrl($url);
        }
    }

    /**
     * @deprecated Please use setBody()
     */
    public function body(array $body) : Request
    {
        return $this->setBody($body);
    }

    /**
     * {@inheritDoc}
     */
    public function setAuthMethod(AuthMethodContract $value)
    {
        $this->authMethod = $value;
        $this->authMethod->prepare($this);

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getAuthMethod() : ?AuthMethodContract
    {
        return $this->authMethod;
    }

    /**
     * {@inheritDoc}
     */
    public function buildUrlString() : string
    {
        $queryString = ! empty($this->query) ? '?'.ArrayHelper::query($this->query) : '';

        return $this->url.$queryString;
    }

    /**
     * Sets Request headers.
     *
     * @param array|null $additionalHeaders
     *
     * @return $this
     */
    public function headers($additionalHeaders = []) : Request
    {
        return $this->setHeaders($additionalHeaders);
    }

    /**
     * Sets the request method.
     *
     * @param string|null $method
     * @return $this
     */
    public function setMethod(string $method = null) : RequestContract
    {
        if (! $method || ! ArrayHelper::contains($this->allowedMethodTypes, strtoupper($method))) {
            $method = $this->defaultAllowedMethod ?? 'get';
        }

        $this->method = strtoupper($method);

        return $this;
    }

    /**
     * @deprecated use setQuery()
     */
    public function query(array $params) : Request
    {
        return $this->setQuery($params);
    }

    /**
     * Sends the request.
     *
     * @return Response|Request
     * @throws Exception
     */
    public function send()
    {
        $this->validate();

        return new $this->responseClass(HttpRepository::performRequest(
            $this->buildUrlString(),
            [
                'body'      => $this->body ? json_encode($this->body) : null,
                'headers'   => $this->headers,
                'method'    => $this->method,
                'sslverify' => $this->sslVerify,
                'timeout'   => $this->timeout,
            ]
        ));
    }

    /**
     * Sets the body of the request.
     *
     * @param array $body
     * @return $this
     */
    public function setBody(array $body) : RequestContract
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Sets Request headers.
     *
     * @param array|null $additionalHeaders
     * @return $this
     * @throws Exception
     */
    public function setHeaders($additionalHeaders = []) : RequestContract
    {
        $this->headers = ArrayHelper::combine([
            'Content-Type' => 'application/json',
            'Accept'       => 'application/json',
        ], $additionalHeaders);

        return $this;
    }

    /**
     * Merges the provided Request headers with the headers already set.
     *
     * @param array $additionalHeaders
     * @return $this
     * @throws Exception
     */
    public function addHeaders(array $additionalHeaders) : RequestContract
    {
        $this->headers = ArrayHelper::combine($this->headers, $additionalHeaders);

        return $this;
    }

    /**
     * Sets query parameters.
     *
     * @param array $params
     * @return $this
     */
    public function setQuery(array $params) : RequestContract
    {
        $this->query = $params;

        return $this;
    }

    /**
     * Sets the request timeout.
     *
     * @param int $seconds
     * @return $this
     */
    public function setTimeout(int $seconds = 30) : RequestContract
    {
        $this->timeout = $seconds;

        return $this;
    }

    /**
     * Sets the url of the request.
     *
     * @param string $url
     * @return $this
     */
    public function setUrl(string $url) : RequestContract
    {
        $this->url = $url;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function sslVerify(bool $default = false) : RequestContract
    {
        $this->sslVerify = $default || ManagedWooCommerceRepository::isProductionEnvironment();

        return $this;
    }

    /**
     * @deprecated use setTimeout()
     */
    public function timeout(int $seconds = 30) : Request
    {
        return $this->setTimeout($seconds);
    }

    /**
     * @deprecated use setUrl()
     */
    public function url(string $url) : Request
    {
        return $this->setUrl($url);
    }

    /**
     * {@inheritDoc}
     */
    public function validate()
    {
        if (! $this->url) {
            throw new Exception('You must provide a url for an outgoing request');
        }
    }
}
