<?php

namespace GoDaddy\WordPress\MWC\Core\Features\CartRecoveryEmails\Models;

use DateInterval;
use DateTime;
use Exception;
use GoDaddy\WordPress\MWC\Common\Helpers\ArrayHelper;
use GoDaddy\WordPress\MWC\Common\Models\Cart;
use GoDaddy\WordPress\MWC\Common\Models\CurrencyAmount;
use GoDaddy\WordPress\MWC\Common\Models\Orders\LineItem;
use GoDaddy\WordPress\MWC\Common\Models\User;
use GoDaddy\WordPress\MWC\Core\Email\EmailService;
use GoDaddy\WordPress\MWC\Core\Features\CartRecoveryEmails\CartRecoveryEmails;
use GoDaddy\WordPress\MWC\Core\Features\CartRecoveryEmails\Contracts\CheckoutEmailNotificationContract;
use GoDaddy\WordPress\MWC\Core\Features\CartRecoveryEmails\DataProviders\CheckoutDataProvider;
use GoDaddy\WordPress\MWC\Core\Features\CartRecoveryEmails\Repositories\WooCommerce\OrdersRepository;
use GoDaddy\WordPress\MWC\Core\Features\CartRecoveryEmails\Settings\OptOutSetting;
use GoDaddy\WordPress\MWC\Core\Features\CartRecoveryEmails\Traits\IsCheckoutEmailNotificationTrait;
use GoDaddy\WordPress\MWC\Core\Features\EmailNotifications\Contracts\ConditionalEmailNotificationContract;
use GoDaddy\WordPress\MWC\Core\Features\EmailNotifications\Contracts\DelayableEmailNotificationContract;
use GoDaddy\WordPress\MWC\Core\Features\EmailNotifications\Models\EmailNotification;
use GoDaddy\WordPress\MWC\Core\Features\EmailNotifications\Models\EmailNotificationSetting;
use GoDaddy\WordPress\MWC\Core\Features\EmailNotifications\Traits\IsDelayableEmailNotificationTrait;
use WC_Product;

/**
 * Email notification for cart recovery emails.
 */
class CartRecoveryEmailNotification extends EmailNotification implements CheckoutEmailNotificationContract, ConditionalEmailNotificationContract, DelayableEmailNotificationContract
{
    use IsCheckoutEmailNotificationTrait;
    use IsDelayableEmailNotificationTrait;

    /** @var string */
    protected $id = 'cart_recovery';

    /** @var string[] */
    protected $categories = ['cart_recovery'];

    /**
     * Configures the email notification.
     */
    public function __construct()
    {
        $this->setName($this->getId())
            ->setLabel(__('Cart Recovery', 'mwc-core'))
            ->setDescription(__("Send a reminder email to customers that left products in their cart but didn't complete the checkout.", 'mwc-core'));
    }

    /**
     * Gets the email notification's initial data providers.
     *
     * @throws Exception
     */
    protected function getInitialDataProviders() : array
    {
        return ArrayHelper::combine(parent::getInitialDataProviders(), [
            new CheckoutDataProvider($this),
        ]);
    }

    /**
     * Gets the email notification's initial settings.
     *
     * @return EmailNotificationSetting[]
     * @throws Exception
     */
    protected function getInitialSettings() : array
    {
        return [
            $this->getEnabledSettingObject(),
            $this->getSubjectSettingObject()->setDefault(__('Hey there {{customer_first_name}}, did you forget something?', 'mwc-core'))->setIsRequired(true),
            $this->getPreviewTextSettingObject()->setDefault(__('Did you forget something?', 'mwc-core')),
            $this->getDelayValueSettingObject(),
            $this->getDelayUnitSettingObject(),
        ];
    }

    /**
     * Determines whether the email notification is available.
     *
     * @return bool
     */
    public function isAvailable() : bool
    {
        return CartRecoveryEmails::shouldLoad();
    }

    /**
     * Determines whether the email notification is enabled.
     *
     * Disables the notification if the MWC Emails Service is not available (for instance if the email sender is not verified).
     *
     * @return bool
     */
    public function isEnabled() : bool
    {
        return parent::isEnabled() && EmailService::shouldLoad();
    }

    /**
     * Determines whether the email should be sent.
     *
     * This should be true when:
     * - the checkout is recoverable {@see Checkout::getStatus()} (the email has been scheduled to be sent)
     * - there is a cart hash (meaning the cart is not empty)
     * - the customer has not opted out from receiving this email
     *
     * @return bool
     * @throws Exception
     */
    public function shouldSend() : bool
    {
        $checkout = $this->getCheckout();

        return $checkout
            && CartRecoveryEmails::isCartRecoveryEmailNotificationEnabled()
            && $checkout->isRecoverable()
            && ! empty($checkout->getWcCartHash())
            && empty(OptOutSetting::get($checkout->getEmailAddress()))
            && ! $this->recipientHasPlacedRecentOrder();
    }

    /**
     * Determines whether the recipient of the email notification has placed a recent order.
     *
     * @return bool true if the recipient has placed a paid order within the past 5 days
     * @throws Exception
     */
    protected function recipientHasPlacedRecentOrder() : bool
    {
        $checkout = $this->getCheckout();

        return $checkout && $checkout->getEmailAddress() && ! empty(OrdersRepository::query([
            'limit'        => 1,
            'return'       => 'ids',
            'type'         => 'shop_order',
            'customer'     => $checkout->getEmailAddress(),
            'status'       => OrdersRepository::getPaidStatuses(),
            'date_created' => '>='.current_datetime()->modify('-5 days')->format('Y-m-d'),
        ]));
    }

    /**
     * Determines the DateTime the email should be scheduled for.
     *
     * @return DateTime|null
     */
    public function sendAt() : ?DateTime
    {
        $checkout = $this->getCheckout();

        if (! $checkout || ! $checkout->getUpdatedAt()) {
            return null;
        }

        try {
            $delay = DateInterval::createFromDateString("{$this->getDelayValue()} {$this->getDelayUnit()}");
        } catch (Exception $exception) {
            // configured delay is invalid, ignore it
            return null;
        }

        return (clone $checkout->getUpdatedAt())->add($delay);
    }

    /**
     * Gets preview data for the custom components that represent non-editable parts for previewing the email notification.
     *
     * @return array
     * @throws Exception
     */
    protected function getAdditionalPreviewData() : array
    {
        $previewCustomer = (new User())
            ->setFirstName('John')
            ->setLastName('Doe');

        $realProducts = wc_get_products([
            'orderby' => 'date',
            'order'   => 'DESC',
            'limit'   => 2,
        ]);

        $lineItems = [];
        $cartTotal = 0;

        /** @var WC_Product $product */
        foreach ($realProducts as $product) {
            $productPrice = $product->get_price('edit');

            $lineItems[] = (new LineItem())
                ->setProduct($product)
                ->setQuantity(2)
                ->setSubTotalAmount(
                    (new CurrencyAmount())
                        ->setAmount($productPrice * 100)
                        ->setCurrencyCode(get_woocommerce_currency()))
                // hardcoded a 7% tax amount just so we have something to show if there are no taxes
                ->setTaxAmount(
                    (new CurrencyAmount())
                        ->setAmount($productPrice * 7)
                        ->setCurrencyCode(get_woocommerce_currency()))
                ->setTotalAmount(
                    (new CurrencyAmount())
                        ->setAmount($productPrice * 214)
                        ->setCurrencyCode(get_woocommerce_currency()));

            $cartTotal += $productPrice * 214;
        }

        $previewCart = (new Cart())
            ->setTotalAmount(
                (new CurrencyAmount())
                    ->setAmount($cartTotal)
                    ->setCurrencyCode(get_woocommerce_currency())
            )
            ->setLineItems($lineItems);

        $previewCheckout = (new Checkout())
            ->setCart($previewCart)
            ->setCustomer($previewCustomer);

        $this->setCheckout($previewCheckout);

        return $this->getData();
    }
}
