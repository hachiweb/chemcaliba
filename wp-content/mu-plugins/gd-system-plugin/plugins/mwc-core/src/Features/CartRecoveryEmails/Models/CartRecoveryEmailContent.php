<?php

namespace GoDaddy\WordPress\MWC\Core\Features\CartRecoveryEmails\Models;

use GoDaddy\WordPress\MWC\Common\Settings\Models\Control;
use GoDaddy\WordPress\MWC\Core\Features\EmailNotifications\Models\DefaultEmailContent;
use GoDaddy\WordPress\MWC\Core\Features\EmailNotifications\Models\EmailNotificationSetting;

class CartRecoveryEmailContent extends DefaultEmailContent
{
    /** @var string */
    const SETTING_ID_CONTENT = 'content';

    /** @var string */
    const SETTING_ID_BUTTON_TEXT = 'buttonText';

    /**
     * Gets the initial settings.
     *
     * @since 2.15.0
     *
     * @return EmailNotificationSetting[]
     */
    public function getInitialSettings() : array
    {
        return [
            (new EmailNotificationSetting())
                ->setId(static::SETTING_ID_HEADING)
                ->setName(static::SETTING_ID_HEADING)
                ->setLabel(__('Heading', 'mwc-core'))
                ->setType(EmailNotificationSetting::TYPE_STRING)
                ->setControl((new Control())
                    ->setType(Control::TYPE_TEXT)
                )
                ->setIsRequired(true)
                ->setDefault(__('Did you forget something?', 'mwc-core')),
            (new EmailNotificationSetting())
                ->setId(static::SETTING_ID_CONTENT)
                ->setName(static::SETTING_ID_CONTENT)
                ->setLabel(__('Content', 'mwc-core'))
                ->setType(EmailNotificationSetting::TYPE_STRING)
                ->setControl((new Control())
                    ->setType(Control::TYPE_TEXTAREA)
                )
                ->setDefault(__("Hey there! You left your shopping cart without completing your purchase just a bit ago. It's not too late to complete your purchase! All of the products are still waiting for you.", 'mwc-core')),
            (new EmailNotificationSetting())
                ->setId(static::SETTING_ID_BUTTON_TEXT)
                ->setName(static::SETTING_ID_BUTTON_TEXT)
                ->setLabel(__('Button text', 'mwc-core'))
                ->setType(EmailNotificationSetting::TYPE_STRING)
                ->setControl((new Control())
                    ->setType(Control::TYPE_TEXT)
                )
                ->setIsRequired(true)
                ->setDefault(__('Complete your purchase', 'mwc-core')),
            (new EmailNotificationSetting())
                ->setId(static::SETTING_ID_ADDITIONAL_CONTENT)
                ->setName(static::SETTING_ID_ADDITIONAL_CONTENT)
                ->setLabel(__('Additional content', 'mwc-core'))
                ->setType(EmailNotificationSetting::TYPE_STRING)
                ->setControl((new Control())
                    ->setType(Control::TYPE_TEXTAREA)
                )
                ->setDefault(__("Trouble checking out? We're here to help. Please reply to this email if you have any questions or need our assistance.", 'mwc-core')),
        ];
    }
}
