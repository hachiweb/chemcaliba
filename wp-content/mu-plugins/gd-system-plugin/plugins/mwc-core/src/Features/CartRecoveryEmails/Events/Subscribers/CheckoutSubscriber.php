<?php

namespace GoDaddy\WordPress\MWC\Core\Features\CartRecoveryEmails\Events\Subscribers;

use DateTime;
use Exception;
use GoDaddy\WordPress\MWC\Common\Email\Emails;
use GoDaddy\WordPress\MWC\Common\Events\Contracts\EventContract;
use GoDaddy\WordPress\MWC\Common\Events\Contracts\SubscriberContract;
use GoDaddy\WordPress\MWC\Common\Events\ModelEvent;
use GoDaddy\WordPress\MWC\Common\Exceptions\BaseException;
use GoDaddy\WordPress\MWC\Common\Exceptions\SentryException;
use GoDaddy\WordPress\MWC\Common\Exceptions\WordPressDatabaseTableDoesNotExistException;
use GoDaddy\WordPress\MWC\Common\Helpers\ArrayHelper;
use GoDaddy\WordPress\MWC\Core\Features\CartRecoveryEmails\CartRecoveryEmails;
use GoDaddy\WordPress\MWC\Core\Features\CartRecoveryEmails\Models\CartRecoveryEmailNotification;
use GoDaddy\WordPress\MWC\Core\Features\CartRecoveryEmails\Models\Checkout;
use GoDaddy\WordPress\MWC\Core\Features\EmailNotifications\Contracts\EmailNotificationContract;
use GoDaddy\WordPress\MWC\Core\Features\EmailNotifications\DataStores\EmailNotificationDataStore;
use GoDaddy\WordPress\MWC\Core\Features\EmailNotifications\DataStores\WooCommerce\EmailTemplateDataStore;
use GoDaddy\WordPress\MWC\Core\Features\EmailNotifications\EmailBuilder;
use ReflectionException;

/**
 * Subscriber that listens to {@see Checkout} model events and possibly schedules cart recovery emails.
 *
 * @see Checkout
 * @see ModelEvent
 */
class CheckoutSubscriber implements SubscriberContract
{
    /** @var string transient key placeholder */
    const TRANSIENT_PROCESSING_CHECKOUT_EVENT = 'mwc_cart_recovery_processing_checkout_event_checkout_id_';

    /**
     * Handles the event.
     *
     * @param EventContract|ModelEvent $event
     * @throws BaseException|ReflectionException
     */
    public function handle(EventContract $event)
    {
        if (! $this->shouldHandle($event)) {
            return;
        }

        /** @var Checkout $checkout */
        if (empty($checkout = $event->getModel())) {
            return;
        }

        // set transient to prevent overlapping
        set_transient(static::TRANSIENT_PROCESSING_CHECKOUT_EVENT.$checkout->getId(), 1, 5);

        if ($this->shouldScheduleEmail($checkout)) {
            $this->scheduleEmail($checkout);
        }

        delete_transient(static::TRANSIENT_PROCESSING_CHECKOUT_EVENT.$checkout->getId());
    }

    /**
     * Determines whether the given event should be handled.
     *
     * @param EventContract $event event object
     * @return bool
     */
    protected function shouldHandle(EventContract $event) : bool
    {
        return CartRecoveryEmails::shouldLoad()
            && $event instanceof ModelEvent
            && 'checkout' === $event->getResource()
            && ArrayHelper::contains(['create', 'update'], $event->getAction())
            && $event->getModel() instanceof Checkout
            && empty(get_transient(static::TRANSIENT_PROCESSING_CHECKOUT_EVENT.$event->getModel()->getId()));
    }

    /**
     * Determines whether we should send an email for this checkout.
     *
     * @param Checkout $checkout
     * @return bool
     */
    protected function shouldScheduleEmail(Checkout $checkout) : bool
    {
        return CartRecoveryEmails::isCartRecoveryEmailNotificationEnabled()
            && ! empty($checkout->getWcCartHash())
            && ! empty($checkout->getEmailAddress())
            && null === $checkout->getEmailScheduledAt();
    }

    /**
     * Schedules an email for this checkout.
     *
     * @param Checkout $checkout
     * @return CheckoutSubscriber
     * @throws BaseException|ReflectionException|SentryException|Exception
     */
    protected function scheduleEmail(Checkout $checkout) : self
    {
        /** @var CartRecoveryEmailNotification $cartRecoveryEmailNotification */
        $cartRecoveryEmailNotification = EmailNotificationDataStore::getNewInstance()->read((new CartRecoveryEmailNotification())->getId());
        $cartRecoveryEmailNotification->setCheckout($checkout);
        // we have a single email template available and all email notifications use it
        $cartRecoveryEmailNotification->setTemplate(EmailTemplateDataStore::getNewInstance()->read('default'));

        $renderableEmail = $this->getEmailBuilder($cartRecoveryEmailNotification)
            ->setRecipients([$checkout->getEmailAddress()])
            ->build();

        try {
            Emails::send($renderableEmail);
            $checkout->setEmailScheduledAt(new DateTime())->save();
        } catch (WordPressDatabaseTableDoesNotExistException $exception) {
            // do not report to Sentry to reduce noise (we are already reporting when the table creation fails)
        } catch (Exception $exception) {
            new SentryException($exception->getMessage());
        }

        return $this;
    }

    /**
     * Helper method to get EmailBuilder.
     *
     * @param CartRecoveryEmailNotification|EmailNotificationContract $cartRecoveryEmailNotification
     * @return EmailBuilder
     */
    protected function getEmailBuilder(EmailNotificationContract $cartRecoveryEmailNotification) : EmailBuilder
    {
        return new EmailBuilder($cartRecoveryEmailNotification);
    }
}
