<?php

namespace GoDaddy\WordPress\MWC\Core\Features\Stripe\Interceptors;

use Exception;
use GoDaddy\WordPress\MWC\Common\Configuration\Configuration;
use GoDaddy\WordPress\MWC\Common\Helpers\ArrayHelper;
use GoDaddy\WordPress\MWC\Common\Helpers\StringHelper;
use GoDaddy\WordPress\MWC\Common\Http\GoDaddyRequest;
use GoDaddy\WordPress\MWC\Common\Http\Redirect;
use GoDaddy\WordPress\MWC\Common\Interceptors\AbstractInterceptor;
use GoDaddy\WordPress\MWC\Common\Register\Register;
use GoDaddy\WordPress\MWC\Common\Repositories\ManagedWooCommerceRepository;
use GoDaddy\WordPress\MWC\Common\Repositories\WordPress\SiteRepository;
use GoDaddy\WordPress\MWC\Core\Features\Stripe\Stripe as StripeFeature;
use GoDaddy\WordPress\MWC\Core\Payments\Stripe;
use GoDaddy\WordPress\MWC\Core\Payments\Stripe\Onboarding;
use GoDaddy\WordPress\MWC\Core\WooCommerce\Exceptions\ConnectionFailedException;
use GoDaddy\WordPress\MWC\Core\WooCommerce\Exceptions\InvalidNonceException;
use GoDaddy\WordPress\MWC\Core\WooCommerce\Exceptions\InvalidPermissionsException;
use GoDaddy\WordPress\MWC\Core\WooCommerce\Exceptions\InvalidSignatureException;
use GoDaddy\WordPress\MWC\Core\WooCommerce\Exceptions\MissingRequestBodyException;
use GoDaddy\WordPress\MWC\Core\WooCommerce\Exceptions\MissingSignatureException;

class RedirectInterceptor extends AbstractInterceptor
{
    /**
     * Adds hooks.
     *
     * @return void
     * @throws Exception
     */
    public function addHooks()
    {
        // handle starting the Stripe onboarding
        Register::action()
            ->setGroup('admin_action_'.Onboarding::ACTION_START)
            ->setHandler([$this, 'handleOnboardingStart'])
            ->execute();

        // handle the redirect back from the MWC API after onboarding
        Register::action()
            ->setGroup('woocommerce_api_'.Onboarding::ACTION_FINISH)
            ->setHandler([$this, 'handleOnboardingFinish'])
            ->execute();

        // handle the onboarding webhook request
        Register::action()
            ->setGroup('woocommerce_api_mwc_oauth_connection_webhook')
            ->setHandler([$this, 'handleOnboardingWebhook'])
            ->execute();
    }

    /**
     * Determines whether the component should be loaded or not.
     *
     * @return bool
     */
    public static function shouldLoad() : bool
    {
        return StripeFeature::shouldLoad();
    }

    /**
     * Handles starting the Stripe onboarding.
     */
    public function handleOnboardingStart()
    {
        try {
            check_admin_referer(Onboarding::ACTION_START);

            if (! current_user_can('manage_woocommerce')) {
                throw new InvalidPermissionsException(__('User does not have permission to manage WooCommerce', 'mwc-core'));
            }

            $request = GoDaddyRequest::withAuth()
                ->setUrl(StringHelper::trailingSlash(Configuration::get('mwc.extensions.api.url')).'oauth/stripe/start')
                ->setBody([
                    'siteId'        => ManagedWooCommerceRepository::getXid(),
                    'siteUrl'       => SiteRepository::getHomeUrl(),
                    'webhookSecret' => Onboarding::getWebhookSecret(),
                ])->setMethod('POST');

            $response = $request->send();

            if ($response->isError()) {
                throw new ConnectionFailedException(sprintf(
                    /* translators: Placeholders: %s - error message from the MWC API */
                    __('Could not create connection: %s', 'mwc-core'),
                    $response->getErrorMessage() ?? 'Unknown error'
                ));
            }

            if (! $redirectUri = ArrayHelper::get($response->getBody(), 'redirectUri')) {
                throw new ConnectionFailedException(__('No redirect available', 'mwc-core'));
            }

            Redirect::to(Onboarding::getConnectionUrl($redirectUri))
                ->setSafe(false)
                ->execute();
        } catch (Exception $exception) {
            wp_die($exception->getMessage());
        }
    }

    /**
     * Handles the redirect back from the MWC API after onboarding.
     */
    public function handleOnboardingFinish()
    {
        if (ArrayHelper::get($_GET, 'serviceName', '') !== 'stripe') {
            return;
        }

        try {
            $state = json_decode(ArrayHelper::get($_GET, 'state'), true);

            if (! wp_verify_nonce(ArrayHelper::get($state, 'nonce', ''), Onboarding::ACTION_FINISH)) {
                throw new InvalidNonceException('Invalid nonce');
            }

            if (! Onboarding::getStatus()) {
                Onboarding::setStatus(Onboarding::STATUS_PENDING);
            }
        } catch (Exception $exception) {
        }

        Redirect::to(add_query_arg([
            'page'    => 'wc-settings',
            'tab'     => 'settings',
            'section' => 'stripe',
        ], admin_url('admin.php')))->execute();
    }

    /**
     * Handles the onboarding webhook request.
     *
     * @internal
     */
    public function handleOnboardingWebhook()
    {
        $payload = $this->getWebhookPayload();
        $payloadData = json_decode($payload, true);

        // only handle requests for the Stripe OAuth service
        if ('stripe' !== ArrayHelper::get($payloadData, 'serviceName')) {
            return;
        }

        try {
            $this->validateWebhookSignature($payload);

            if ($accountId = StringHelper::sanitize(ArrayHelper::get($payloadData, 'externalAccountId'))) {
                Stripe::setAccountId($accountId);
            }

            if ($publicKey = StringHelper::sanitize(ArrayHelper::get($payloadData, 'publicKey'))) {
                Stripe::setApiPublicKey($publicKey);
            }

            if ($secretKey = StringHelper::sanitize(ArrayHelper::get($payloadData, 'accessToken'))) {
                Stripe::setApiSecretKey($secretKey);
            }

            Onboarding::setStatus(Onboarding::STATUS_CONNECTED);

            status_header(200);
        } catch (Exception $exception) {
            status_header($exception->getCode());

            echo $exception->getMessage();
        }
    }

    /**
     * Gets the webhook payload from the request.
     *
     * @return array|null
     */
    protected function getWebhookPayload() : ?string
    {
        return file_get_contents('php://input');
    }

    /**
     * Validates the webhook request signature.
     *
     * @param mixed $payload
     *
     * @throws InvalidSignatureException|MissingRequestBodyException|MissingSignatureException
     */
    protected function validateWebhookSignature($payload)
    {
        if (empty($payload)) {
            throw new MissingRequestBodyException('Missing request body');
        }

        if (! $signature = $_SERVER['HTTP_MWC_WEBHOOK_SIGNATURE']) {
            throw new MissingSignatureException('Missing signature');
        }

        $secret = Onboarding::getWebhookSecret();
        $hash = hash_hmac('sha512', $payload, $secret);

        if (! hash_equals($signature, $hash)) {
            throw new InvalidSignatureException('Invalid signature');
        }
    }
}
