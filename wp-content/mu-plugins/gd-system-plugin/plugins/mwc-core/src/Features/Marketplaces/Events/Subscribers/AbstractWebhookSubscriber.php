<?php

namespace GoDaddy\WordPress\MWC\Core\Features\Marketplaces\Events\Subscribers;

use GoDaddy\WordPress\MWC\Common\Helpers\ArrayHelper;
use GoDaddy\WordPress\MWC\Common\Helpers\StringHelper;
use GoDaddy\WordPress\MWC\Common\Repositories\ManagedWooCommerceRepository;
use GoDaddy\WordPress\MWC\Core\Auth\Providers\Marketplaces\Methods\Header;
use GoDaddy\WordPress\MWC\Core\Auth\Providers\Marketplaces\Models\Credentials;
use GoDaddy\WordPress\MWC\Core\Events\AbstractWebhookReceivedEvent;
use GoDaddy\WordPress\MWC\Core\Events\Subscribers\AbstractWebhookReceivedSubscriber;

/**
 * The base class for Marketplaces webhook subscribers.
 */
abstract class AbstractWebhookSubscriber extends AbstractWebhookReceivedSubscriber
{
    /**
     * {@inheritDoc}
     */
    public function validate(AbstractWebhookReceivedEvent $event) : bool
    {
        if (empty($customerId = ManagedWooCommerceRepository::getCustomerId())
            || empty($ventureId = ManagedWooCommerceRepository::getVentureId())) {
            return false;
        }

        if (! StringHelper::isJson($event->getPayload())) {
            return false;
        }

        $credentials = (new Credentials())->setCustomerId($customerId)->setVentureId($ventureId);

        $signature = (new Header($credentials))->getSignature($event->getPayload());

        if (empty($header = ArrayHelper::get($event->getHeaders(), Header::HEADER_NAME))) {
            return false;
        }

        return hash_equals($signature, $header);
    }
}
