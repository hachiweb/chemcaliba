<?php

namespace GoDaddy\WordPress\MWC\Core\Features\Marketplaces\Pages;

use Exception;
use GoDaddy\WordPress\MWC\Common\Components\Contracts\ComponentContract;
use GoDaddy\WordPress\MWC\Common\Configuration\Configuration;
use GoDaddy\WordPress\MWC\Common\Content\AbstractAdminPage;
use GoDaddy\WordPress\MWC\Common\Content\Context\Screen;
use GoDaddy\WordPress\MWC\Common\Enqueue\Enqueue;
use GoDaddy\WordPress\MWC\Common\Exceptions\SentryException;
use GoDaddy\WordPress\MWC\Common\Helpers\StringHelper;
use GoDaddy\WordPress\MWC\Common\Register\Register;
use GoDaddy\WordPress\MWC\Common\Repositories\WordPressRepository;
use GoDaddy\WordPress\MWC\Core\Features\Marketplaces\Repositories\ChannelRepository;

/**
 * The Marketplaces admin page.
 */
class MarketplacesPage extends AbstractAdminPage implements ComponentContract
{
    /** @var string the page and menu item slug */
    const SLUG = 'gd-marketplaces';

    /** @var string required capability to interact with page and related menu item */
    const CAPABILITY = 'manage_woocommerce';

    /** @var int the menu index level to be located right below the Products menu */
    const MENU_LEVEL = 56;

    /**
     * Sets up the page properties.
     */
    public function __construct()
    {
        $this->screenId = static::SLUG;
        $this->title = __('Marketplaces', 'mwc-core');
        $this->menuTitle = __('Marketplaces', 'mwc-core');
        $this->parentMenuSlug = static::SLUG;
        $this->capability = static::CAPABILITY;

        parent::__construct();
    }

    /**
     * Loads the page components.
     *
     * @return void
     * @throws Exception
     */
    public function load()
    {
        Register::action()
            ->setGroup('wp_after_admin_bar_render')
            ->setHandler([$this, 'renderHeading'])
            ->setPriority(PHP_INT_MIN)
            ->execute();

        Register::action()
            ->setGroup('admin_enqueue_scripts')
            ->setHandler([$this, 'maybeEnqueueAssets'])
            ->execute();
    }

    /**
     * Overrides the default addMenuItem method to be able to add a root menu item.
     *
     * @throws Exception
     */
    public function addMenuItem() : AbstractAdminPage
    {
        $pageTitle = __('Marketplaces', 'mwc-core');
        $pageIcon = $this->getMarketplacesMenuIcon();

        add_menu_page(
            $pageTitle,
            $pageTitle.'<div id="gd-marketplaces-main-menu-item"></div>',
            static::CAPABILITY,
            static::SLUG,
            [$this, 'render'],
            $pageIcon,
            static::MENU_LEVEL
        );

        return $this;
    }

    /**
     * Loads the Marketplaces menu item from its SVG file.
     *
     * @return string
     */
    protected function getMarketplacesMenuIcon() : string
    {
        $iconFilePath = StringHelper::trailingSlash(Configuration::get('mwc.directory', '')).'assets/images/marketplaces/gd-marketplaces-icon.svg';

        try {
            return 'data:image/svg+xml;base64,'.$this->getBase64FileContents($iconFilePath);
        } catch (Exception $ex) {
            new SentryException('Marketplaces icon not loaded');
        }

        return '';
    }

    /**
     * Gets the SVG file base 64 contents.
     *
     * @param string $iconFilePath
     *
     * @return string
     */
    protected function getBase64FileContents(string $iconFilePath) : string
    {
        return base64_encode(file_get_contents($iconFilePath));
    }

    /**
     * Determines if the current admin page is the Marketplaces page.
     *
     * @return bool
     */
    public static function isMarketplacesPage() : bool
    {
        $screen = WordPressRepository::getCurrentScreen();

        return $screen && 'admin-'.static::SLUG === $screen->getPageId();
    }

    /**
     * Determines whether the Marketplaces assets should be loaded for the current screen.
     *
     * @return bool
     */
    protected function shouldEnqueueAssets() : bool
    {
        return static::isMarketplacesPage();
    }

    /**
     * Enqueues the page assets.
     *
     * @return void
     * @throws Exception
     */
    protected function enqueueAssets() : void
    {
        Enqueue::style()
            ->setHandle('gd-marketplaces')
            ->setSource(WordPressRepository::getAssetsUrl('css/gd-marketplaces.css'))
            ->execute();
    }

    /**
     * Renders the page contents.
     *
     * @internal
     *
     * @return void
     */
    public function render() : void
    {
        ?>
        <div id="gd-marketplaces" class="gd-marketplaces-page">
            <h2><?php esc_html_e('Manage Sales Channels', 'mwc-core'); ?></h2>
        <?php $connectedChannels = ChannelRepository::connected();

        if (! empty($connectedChannels)) {
            $this->renderManageConnectedSalesChannels();
        } else {
            $this->renderNoConnectedSalesChannels();
        }

        $this->renderResources(); ?>
        </div>
        <?php
    }

    /**
     * Renders the page heading.
     *
     * @internal
     *
     * @return void
     */
    public function renderHeading() : void
    {
        if (! static::isMarketplacesPage()) {
            return;
        } ?>
        <div id="gd-marketplaces-page-heading">
            <h1><?php esc_html_e('Marketplaces &amp; Social', 'mwc-core'); ?></h1>
        </div>
        <?php
    }

    /**
     * Renders the output to manage connected sales channels.
     *
     * @return void
     */
    protected function renderManageConnectedSalesChannels() : void
    {
        // @TODO implement this method in MWC-6077 {unfulvio 2022-05-25}
    }

    /**
     * Renders the output when there are no connected sales channels.
     *
     * @return void
     */
    protected function renderNoConnectedSalesChannels() : void
    {
        ?>
        <p><?php esc_html_e('You haven\'t added any sales channels yet. Add sales channels in order to post your listings on third party marketplaces and social platforms.', 'mwc-core'); ?></p>
        <?php
        $this->renderAddSalesChannelButton();
    }

    /**
     * Renders the button to add a sales channel.
     *
     * @return void
     */
    protected function renderAddSalesChannelButton() : void
    {
        ?>
        <a href="https://marketplaces.godaddy.com/settings/channels" target="_blank" class="button button-primary"><?php esc_html_e('Add Sales Channel', 'mwc-core'); ?></a>
        <?php
    }

    /**
     * Renders the Marketplaces resources information.
     *
     * @return void
     */
    protected function renderResources() : void
    {
        // @TODO implement this method in MWC-6078 {unfulvio 2022-05-25}
    }
}
