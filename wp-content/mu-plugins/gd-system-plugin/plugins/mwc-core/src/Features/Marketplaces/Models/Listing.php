<?php

namespace GoDaddy\WordPress\MWC\Core\Features\Marketplaces\Models;

use GoDaddy\WordPress\MWC\Common\Models\AbstractModel;

/**
 * Model representing a Marketplaces product listing.
 */
class Listing extends AbstractModel
{
    /** @var string */
    protected $channelType;

    /** @var bool */
    protected $isPublished = false;

    /** @var string */
    protected $link;

    /**
     * Gets the listing channel type name (e.g. Amazon).
     *
     * @return string
     */
    public function getChannelType() : string
    {
        return $this->channelType;
    }

    /**
     * Sets the listing channel type name.
     *
     * @param string $channelType
     * @return Listing
     */
    public function setChannelType(string $channelType) : Listing
    {
        $this->channelType = $channelType;

        return $this;
    }

    /**
     * Returns true if the listing is published or false if it is a draft.
     *
     * @return bool
     */
    public function isPublished() : bool
    {
        return $this->isPublished;
    }

    /**
     * Sets if the listing is published or a draft.
     *
     * @param bool $isPublished
     * @return Listing
     */
    public function setIsPublished(bool $isPublished) : Listing
    {
        $this->isPublished = $isPublished;

        return $this;
    }

    /**
     * Gets the listing link.
     *
     * @return string
     */
    public function getLink() : string
    {
        return $this->link;
    }

    /**
     * Sets the listing link.
     *
     * @param string $link
     * @return Listing
     */
    public function setLink(string $link) : Listing
    {
        $this->link = $link;

        return $this;
    }
}
