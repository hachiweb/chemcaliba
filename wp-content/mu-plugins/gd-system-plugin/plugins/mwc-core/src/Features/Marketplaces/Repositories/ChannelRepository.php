<?php

namespace GoDaddy\WordPress\MWC\Core\Features\Marketplaces\Repositories;

class ChannelRepository
{
    /**
     * Gets all Marketplaces channel types.
     *
     * @return string[]
     */
    public static function all() : array
    {
        return [
            'Amazon',
            'eBay',
            'Facebook',
            'Walmart',
            'Etsy',
            'Google',
        ];
    }

    /**
     * Gets a list of the connected Marketplaces channel types.
     *
     * @return string[]
     */
    public static function connected() : array
    {
        // @TODO: get this list from the GDM API (and cache it) {dmagalhaes 2022-05-20}
        return static::all();
    }
}
