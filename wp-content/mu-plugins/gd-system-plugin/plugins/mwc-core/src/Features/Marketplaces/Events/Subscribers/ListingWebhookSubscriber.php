<?php

namespace GoDaddy\WordPress\MWC\Core\Features\Marketplaces\Events\Subscribers;

use GoDaddy\WordPress\MWC\Common\Components\Contracts\ComponentContract;
use GoDaddy\WordPress\MWC\Core\Events\AbstractWebhookReceivedEvent;

/**
 * The Marketplaces listing webhook subscriber.
 */
class ListingWebhookSubscriber extends AbstractWebhookSubscriber implements ComponentContract
{
    /**
     * {@inheritDoc}
     */
    public function handlePayload(AbstractWebhookReceivedEvent $event)
    {
    }

    /**
     * {@inheritDoc}
     */
    public function load()
    {
    }
}
