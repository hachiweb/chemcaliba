<?php

namespace GoDaddy\WordPress\MWC\Core\Features\Marketplaces\Adapters;

use GoDaddy\WordPress\MWC\Common\Contracts\FulfillmentStatusContract;
use GoDaddy\WordPress\MWC\Common\DataSources\Contracts\DataSourceAdapterContract;
use GoDaddy\WordPress\MWC\Common\Helpers\ArrayHelper;
use GoDaddy\WordPress\MWC\Common\Models\Orders\LineItem;
use GoDaddy\WordPress\MWC\Core\Features\Marketplaces\Traits\ConvertsMarketplaceAmountTrait;
use GoDaddy\WordPress\MWC\Shipping\Models\Orders\FulfillmentStatuses\FulfilledFulfillmentStatus;
use GoDaddy\WordPress\MWC\Shipping\Models\Orders\FulfillmentStatuses\PartiallyFulfilledFulfillmentStatus;
use GoDaddy\WordPress\MWC\Shipping\Models\Orders\FulfillmentStatuses\UnfulfilledFulfillmentStatus;
use WC_Product;

/**
 * Adapts data from a GDM webhook to a native core order object.
 */
class LineItemAdapter implements DataSourceAdapterContract
{
    use ConvertsMarketplaceAmountTrait;

    /** @var array Line item data from the webhook */
    protected $source;

    /**
     * LineItemAdapter constructor.
     *
     * @param array $lineItemAdapter Line item data from the webhook payload.
     */
    public function __construct(array $lineItemAdapter)
    {
        $this->source = $lineItemAdapter;
    }

    /**
     * Converts a source line item from a GDM payload to a core native order.
     *
     * @return LineItem
     */
    public function convertFromSource() : LineItem
    {
        $lineItem = new LineItem();

        $lineItem
            ->setQuantity((float) ArrayHelper::get($this->source, 'quantity', 0.0))
            ->setName(ArrayHelper::get($this->source, 'title', ''))
            ->setSubTotalAmount($this->parseAndConvertAmountFromSource('unit_price'))
            ->setTotalAmount($this->parseAndConvertAmountFromSource('total'))
            ->setTaxAmount($this->parseAndConvertAmountFromSource('tax'))
            ->setProduct($this->convertProductFromSource())
            ->setFulfillmentStatus($this->convertFulfillmentStatusFromSource());

        return $lineItem;
    }

    /**
     * {@inheritDoc}
     */
    public function convertToSource() : array
    {
        // Not implemented.
        return [];
    }

    /**
     * Converts a source line item product data from a GDM payload to a core native order.
     *
     * @TODO: [MWC-5945] Implement the product query (maybe by "order_item_ref"=>"456-346") {acastro1 2022-05-18}
     *
     * @return null|WC_Product
     */
    protected function convertProductFromSource() : ?WC_Product
    {
        return null;
    }

    /**
     * Converts a source line item fulfillment status data from a GDM payload to a core native order.
     *
     * @return FulfillmentStatusContract
     */
    protected function convertFulfillmentStatusFromSource() : FulfillmentStatusContract
    {
        $quantity = (float) ArrayHelper::get($this->source, 'quantity', 0.0);
        $quantityFulfilled = (float) ArrayHelper::get($this->source, 'quantity_fulfilled', 0.0);

        if ($quantityFulfilled === $quantity) {
            return new FulfilledFulfillmentStatus();
        }

        if ($quantityFulfilled > 0) {
            return new PartiallyFulfilledFulfillmentStatus();
        }

        return new UnfulfilledFulfillmentStatus();
    }
}
