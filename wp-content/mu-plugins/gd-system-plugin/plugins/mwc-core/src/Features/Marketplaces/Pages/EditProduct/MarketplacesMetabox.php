<?php

namespace GoDaddy\WordPress\MWC\Core\Features\Marketplaces\Pages\EditProduct;

use Exception;
use GoDaddy\WordPress\MWC\Common\Components\Contracts\ComponentContract;
use GoDaddy\WordPress\MWC\Common\Content\AbstractPostMetabox;
use GoDaddy\WordPress\MWC\Common\Enqueue\Enqueue;
use GoDaddy\WordPress\MWC\Common\Exceptions\SentryException;
use GoDaddy\WordPress\MWC\Common\Helpers\ArrayHelper;
use GoDaddy\WordPress\MWC\Common\Register\Register;
use GoDaddy\WordPress\MWC\Common\Repositories\WordPressRepository;
use GoDaddy\WordPress\MWC\Core\Features\Marketplaces\Models\Listing;
use GoDaddy\WordPress\MWC\Core\Features\Marketplaces\Repositories\ChannelRepository;
use GoDaddy\WordPress\MWC\Core\Features\Marketplaces\Repositories\ListingRepository;
use GoDaddy\WordPress\MWC\Core\WooCommerce\Adapters\ProductAdapter;
use GoDaddy\WordPress\MWC\Core\WooCommerce\Models\Products\Product;
use WP_Post;

class MarketplacesMetabox extends AbstractPostMetabox implements ComponentContract
{
    /** @var string The post type associated with this metabox */
    protected $postType = 'product';

    /** @var string The ID for the metabox */
    protected $id = 'gd-marketplaces';

    /** @var string The priority for the metabox (will be displayed after the Product data metabox) */
    protected $priority = self::PRIORITY_CORE;

    /**
     * MarketplacesMetabox constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->setTitle(__('Marketplaces & Social', 'mwc-core'));
    }

    /**
     * Loads the metabox.
     *
     * @return void
     * @throws Exception
     */
    public function load()
    {
        Register::action()
            ->setGroup('admin_enqueue_scripts')
            ->setHandler([$this, 'enqueueAssets'])
            ->execute();
    }

    /**
     * Determines if the assets should be enqueued.
     *
     * @return bool
     */
    public function shouldEnqueueAssets() : bool
    {
        return ! empty($screen = WordPressRepository::getCurrentScreen())
            && 'edit_product' === $screen->getPageId();
    }

    /**
     * Enqueues the assets.
     *
     * @internal
     *
     * @throws Exception
     */
    public function enqueueAssets()
    {
        if (! $this->shouldEnqueueAssets()) {
            return;
        }

        Enqueue::style()
            ->setHandle('gd-marketplaces')
            ->setSource(WordPressRepository::getAssetsUrl('css/gd-marketplaces.css'))
            ->execute();
    }

    /**
     * Renders metabox markup.
     *
     * @param WP_Post|null $post
     * @param array $args
     */
    public function render($post = null, $args = [])
    {
        $channels = ChannelRepository::all();

        try {
            $product = (new ProductAdapter(wc_get_product($post)))->convertFromSource();
        } catch (Exception $exception) {
            return;
        } ?>
        <div class="panel-wrap woocommerce <?php echo $this->getId(); ?>">

            <ul class="<?php echo $this->getId(); ?>-tabs wc-tabs">
                <?php

                foreach ($channels as $key => $channel) :

                    $class = 0 === $key ? ['active'] : [];
        $lowerChannel = strtolower($channel);
        $hasPublishedListing = ! empty(ListingRepository::getProductListingsByChannel($product, $channel, true)); ?>
                    <li class="<?php echo sanitize_html_class($lowerChannel); ?>-tab <?php echo implode(' ', array_map('sanitize_html_class', $class)); ?>">
                        <a href="#<?php echo esc_attr("gd-marketplaces-{$lowerChannel}"); ?>">
                            <span><?php echo esc_html($channel); ?></span>
                            <?php if ($hasPublishedListing) : ?>
                                <span class="alignright">
                                    <?php echo $this->getCheckBadgeIcon(); ?>
                                </span>
                            <?php endif; ?>
                        </a>
                    </li>
                <?php

                endforeach; ?>
            </ul>
            <?php

            if (! empty($channels)) {
                foreach ($channels as $channel) {
                    $firstChannelListing = null;
                    if (! empty($channelListings = ListingRepository::getProductListingsByChannel($product, $channel))) {
                        $firstChannelListing = current($channelListings);
                    }
                    $this->renderChannelPanel($product, $post, $channel, $firstChannelListing);
                }
            } ?>
            <div class="clear"></div>
        </div><!-- //.panel-wrap -->
        <?php
    }

    /**
     * Renders the markup for a channel panel.
     *
     * @param Product|null $product
     * @param WP_Post|null $post
     * @param string $channel
     * @param Listing|null $listing
     */
    protected function renderChannelPanel(?Product $product, ?WP_Post $post, string $channel, ?Listing $listing)
    {
        $lowerChannel = strtolower($channel); ?>
        <div id="<?php echo esc_attr("gd-marketplaces-{$lowerChannel}"); ?>" class="panel woocommerce_options_panel">
            <?php
                if (! ArrayHelper::contains(ChannelRepository::connected(), $channel)) {
                    $this->renderChannelNotConnected($product, $post, $channel);
                } elseif (empty($listing)) {
                    $this->renderProductNotListed($product, $post, $channel);
                } elseif (! $listing->isPublished()) {
                    $this->renderDraftListingCreated($product, $post, $listing);
                } else {
                    $this->renderProductListed($product, $post, $listing);
                } ?>
        </div>
        <?php
    }

    /**
     * Renders the markup for a channel panel when the channel is not connected.
     *
     * @param Product|null $product
     * @param WP_Post|null $post
     * @param string $channel
     */
    protected function renderChannelNotConnected(?Product $product, ?WP_Post $post, string $channel)
    {
        ?>
        <p>
            <?php esc_html_e('You haven\'t added this sales channel yet.', 'mwc-core'); ?>
        </p>
        <p>
            <a href="https://marketplaces.godaddy.com/settings/channels" target="_blank" class="button button-primary"><?php esc_html_e('Manage Sales Channels', 'mwc-core'); ?></a>
        </p>
        <?php
    }

    /**
     * Renders the markup for a channel panel when the channel is connected and the product is not listed on it.
     *
     * @param Product|null $product
     * @param WP_Post|null $post
     * @param string $channel
     */
    protected function renderProductNotListed(?Product $product, ?WP_Post $post, string $channel)
    {
        ?>
        <div id="<?php printf(esc_attr('gd-marketplaces-product-not-listed-%s'), sanitize_title($channel)); ?>" class="gd-marketplaces-product-not-listed">
            <p>
                <?php printf(
                    /* translators: Placeholders: %s: Marketplaces channel name (e.g. Amazon, eBay, Facebook, etc.) */
                    esc_html__('This product hasn\'t been listed on %s yet.', 'mwc-core'), esc_html($channel)
                ); ?>
            </p>
            <?php // TODO implement this button in MB9 {unfulvio 2022-05-24}?>
            <p>
                <button id="<?php printf(esc_attr('mwc-marketplaces-create-draft-listing-for-%s'), sanitize_title($channel)); ?>" class="button button-primary mwc-marketplaces-create-draft-listing"><?php esc_html_e('Create Draft Listing', 'mwc-core'); ?></button>
            </p>
            <div class="gd-marketplaces-product-requirements">
                <p>
                    <?php printf(
                        /* translators: Placeholders: %1%s - Opening HTML <a> link tag, %2$s - Closing HTML </a> link tag */
                        esc_html__('The requirements below must be met in order to list this product. %1$sLearn more about product sync%2$s', 'mwc-core'),
                        // @TODO determine the correct URL for this link in MWC 6063 {unfulvio 2022-05-24}
                        '<a href="" target="_blank">',
                        '</a>'
                    ); ?>
                </p>
                <ul class="ul-list ul-disc">
                    <li><?php echo esc_html_x('Simple or variable product (not virtual or downloadable)', 'Product property requirement', 'mwc-core'); ?></li>
                    <li><?php echo esc_html_x('Published product', 'Product property requirement', 'mwc-core'); ?></li>
                    <li><?php echo esc_html_x('Manage stock enabled and stock quantity > 0', 'Product property requirement', 'mwc-core'); ?></li>
                    <li><?php echo esc_html_x('No backorders allowed', 'Product property requirement', 'mwc-core'); ?></li>
                    <li><?php echo esc_html_x('Brand and Condition fields filled', 'Product property requirement', 'mwc-core'); ?></li>
                </ul>
            </div>
        </div>
        <?php
    }

    /**
     * Renders the markup for a channel panel when the channel is connected and the product has a draft listing on it.
     *
     * @param Product|null $product
     * @param WP_Post|null $post
     * @param Listing $listing
     */
    protected function renderDraftListingCreated(?Product $product, ?WP_Post $post, Listing $listing)
    {
        ?>
        <p>
            <?php
                /* translators: Placeholders: %s Marketplaces channel name (e.g. Amazon, eBay, Facebook, etc.) */
                echo esc_html(sprintf(__('A draft listing has been created for this product on %s.', 'mwc-core'), $listing->getChannelType())); ?>
        </p>
        <p>
            <a href="<?php echo esc_url($listing->getLink()); ?>" target="_blank" class="button"><?php esc_html_e('View Draft Listing', 'mwc-core'); ?></a>
        </p>
        <?php
    }

    /**
     * Renders the markup for a channel panel when the channel is connected and the product has a published listing on it.
     *
     * @param Product|null $product
     * @param WP_Post|null $post
     * @param Listing $listing
     */
    protected function renderProductListed(?Product $product, ?WP_Post $post, Listing $listing)
    {
        ?>
        <p class="gd-marketplaces-product-listed">
            <?php
            echo $this->getCheckBadgeIcon();

        /* translators: Placeholders: %s Marketplaces channel name (e.g. Amazon, eBay, Facebook, etc.) */
        echo esc_html(sprintf(__('This product has been listed on %s.', 'mwc-core'), $listing->getChannelType())); ?>
        </p>
        <p>
            <a href="<?php echo esc_url($listing->getLink()); ?>" target="_blank" class="button"><?php esc_html_e('Edit Listing', 'mwc-core'); ?></a>
        </p>
        <?php
    }

    /**
     * Returns the `<img>` tag for the check badge icon.
     *
     * @return string `<img>` tag on success, empty string if the icon cannot be found.
     */
    protected function getCheckBadgeIcon() : string
    {
        try {
            $imageUrl = WordPressRepository::getAssetsUrl('images/check-badge-icon.svg');
        } catch (Exception $ex) {
            new SentryException('Check badge icon not loaded');

            return '';
        }

        if (empty($imageUrl)) {
            return '';
        }

        return '<img src="'.esc_url($imageUrl).'" width="16" height="16"/>';
    }
}
