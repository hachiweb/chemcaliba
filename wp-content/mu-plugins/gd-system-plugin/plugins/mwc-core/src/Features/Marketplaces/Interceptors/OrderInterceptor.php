<?php

namespace GoDaddy\WordPress\MWC\Core\Features\Marketplaces\Interceptors;

use Exception;
use GoDaddy\WordPress\MWC\Common\Exceptions\SentryException;
use GoDaddy\WordPress\MWC\Common\Helpers\ArrayHelper;
use GoDaddy\WordPress\MWC\Common\Interceptors\AbstractInterceptor;
use GoDaddy\WordPress\MWC\Common\Register\Register;
use GoDaddy\WordPress\MWC\Common\Repositories\WooCommerceRepository;
use GoDaddy\WordPress\MWC\Common\Repositories\WordPressRepository;
use GoDaddy\WordPress\MWC\Core\WooCommerce\Adapters\OrderAdapter;
use WC_Order;

/**
 * Handles orders that may be tied to a marketplace order.
 */
class OrderInterceptor extends AbstractInterceptor
{
    /**
     * Adds hooks.
     *
     * @return void
     * @throws Exception
     */
    public function addHooks()
    {
        Register::filter()
            ->setGroup('woocommerce_admin_order_should_render_refunds')
            ->setHandler([$this, 'removeRefundActionsForMarketplaceOrders'])
            ->setArgumentsCount(3)
            ->execute();

        Register::action()
            ->setGroup('woocommerce_order_item_add_action_buttons')
            ->setHandler([$this, 'removeRefundButtonForMarketplaceOrders'])
            ->setCondition(static function () {
                return ! WordPressRepository::isCliMode() && version_compare(WooCommerceRepository::getWooCommerceVersion(), '6.4', '<=');
            })
            ->execute();

        Register::filter()
            ->setGroup('wc_order_statuses')
            ->setHandler([$this, 'removeOrderStatusesForMarketplaceOrders'])
            ->execute();
    }

    /**
     * Removes refund button for marketplace orders.
     *
     * @TODO Remove this callback when WooCommerce 6.4 is the minimum supported version {unfulvio 2022-05-11}
     * @see OrderInterceptor::removeRefundActionsForMarketplaceOrders()
     *
     * @internal
     *
     * @param WC_Order|mixed $order
     * @return void
     */
    public function removeRefundButtonForMarketplaceOrders($order) : void
    {
        try {
            if (! $order instanceof WC_Order || ! OrderAdapter::getNewInstance($order)->convertFromSource()->hasMarketplaceOrder()) {
                return;
            }
        } catch (Exception $exception) {
            // since we are in a callback context, we should catch any exceptions and just report them to Sentry
            new SentryException($exception);

            return;
        }

        wc_enqueue_js("jQuery('button.button.refund-items').hide();");
    }

    /**
     * Removes refund actions for marketplace orders.
     *
     * @internal
     *
     * @param bool|mixed $shouldRenderRefunds
     * @param int|mixed $orderId
     * @param WC_Order|mixed $order
     * @return bool|mixed
     */
    public function removeRefundActionsForMarketplaceOrders($shouldRenderRefunds, $orderId, $order)
    {
        if (! $shouldRenderRefunds || ! $orderId || ! $order instanceof WC_Order) {
            return $shouldRenderRefunds;
        }

        try {
            $order = OrderAdapter::getNewInstance($order)->convertFromSource();
            $shouldRenderRefunds = ! $order->hasMarketplaceOrder();
        } catch (Exception $exception) {
            // since we are in a callback context, we should catch any exceptions and just report them to Sentry
            new SentryException($exception->getMessage());
        }

        return $shouldRenderRefunds;
    }

    /**
     * Removes refund and failed statuses for marketplace orders.
     *
     * @see \wc_get_order_statuses() but in the context of {@see WC_Meta_Box_Order_Data::output()}
     *
     * @internal
     *
     * @return string[]|mixed
     */
    public function removeOrderStatusesForMarketplaceOrders($statuses)
    {
        global $theorder;

        if (! $theorder instanceof WC_Order || ! ArrayHelper::accessible($statuses) || ! WordPressRepository::isAdmin()) {
            return $statuses;
        }

        try {
            $order = OrderAdapter::getNewInstance($theorder)->convertFromSource();

            if ($order->hasMarketplaceOrder()) {
                unset($statuses['wc-refunded'], $statuses['wc-failed']);
            }
        } catch (Exception $exception) {
            // since we are in a callback context, we should catch any exceptions and just report them to Sentry
            new SentryException($exception->getMessage());
        }

        return $statuses;
    }
}
