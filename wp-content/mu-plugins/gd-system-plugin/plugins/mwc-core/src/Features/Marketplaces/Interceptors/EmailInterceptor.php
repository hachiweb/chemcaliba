<?php

namespace GoDaddy\WordPress\MWC\Core\Features\Marketplaces\Interceptors;

use Exception;
use GoDaddy\WordPress\MWC\Common\Exceptions\SentryException;
use GoDaddy\WordPress\MWC\Common\Interceptors\AbstractInterceptor;
use GoDaddy\WordPress\MWC\Common\Register\Register;
use GoDaddy\WordPress\MWC\Core\Features\EmailNotifications\WooCommerce\EmailCatcher;
use GoDaddy\WordPress\MWC\Core\WooCommerce\Adapters\OrderAdapter;
use WC_Email;
use WC_Email_Customer_Note;
use WC_Order;

/**
 * Intercepts WooCommerce emails to adjust handling for Marketplaces-related order emails.
 */
class EmailInterceptor extends AbstractInterceptor
{
    /**
     * Adds hooks.
     *
     * @return void
     * @throws Exception
     */
    public function addHooks()
    {
        /* @see EmailCatcher::addHooks() overridden here via PHP_INT_MAX */
        Register::filter()
            ->setGroup('woocommerce_mail_callback')
            ->setHandler([$this, 'preventSendingMarketplaceOrderCustomerEmails'])
            ->setArgumentsCount(2)
            ->setPriority(PHP_INT_MAX)
            ->execute();
    }

    /**
     * Determines whether the email should be processed by WooCommerce using the default callback.
     *
     * @param WC_Email|mixed $email
     * @return bool
     */
    protected function shouldUseDefaultEmailCallback($email) : bool
    {
        return ! $email instanceof WC_Email
            || $email instanceof WC_Email_Customer_Note
            || ! $email->object instanceof WC_Order
            || ! $email->is_customer_email()
            || $email->is_manual();
    }

    /**
     * Prevents sending emails for WooCommerce orders related to Marketplaces that are customer-facing.
     *
     * @see WC_Email::send() filters the email callback so that the email isn't sent if the conditions are met
     * @see \__return_false()
     *
     * @internal
     *
     * @param string|array|callable $callback
     * @param WC_Email|mixed $email
     * @return string|array|callable
     */
    public function preventSendingMarketplaceOrderCustomerEmails($callback, $email)
    {
        if ($this->shouldUseDefaultEmailCallback($email)) {
            return $callback;
        }

        try {
            $order = OrderAdapter::getNewInstance($email->object)->convertFromSource();

            if (! $order->hasMarketplaceOrder()) {
                return $callback;
            }
        } catch (Exception $exception) {
            // since we are in a callback context, we should catch any exceptions and just report them to Sentry
            new SentryException($exception->getMessage());

            return $callback;
        }

        return '__return_false';
    }
}
