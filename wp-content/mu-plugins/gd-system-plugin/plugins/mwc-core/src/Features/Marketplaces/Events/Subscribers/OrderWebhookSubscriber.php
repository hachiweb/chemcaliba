<?php

namespace GoDaddy\WordPress\MWC\Core\Features\Marketplaces\Events\Subscribers;

use GoDaddy\WordPress\MWC\Common\Components\Contracts\ComponentContract;
use GoDaddy\WordPress\MWC\Common\Exceptions\AdapterException;
use GoDaddy\WordPress\MWC\Core\Events\AbstractWebhookReceivedEvent;
use GoDaddy\WordPress\MWC\Core\Features\Marketplaces\Adapters\OrderAdapter as MarketplacesOrderAdapter;
use GoDaddy\WordPress\MWC\Core\WooCommerce\Adapters\OrderAdapter;
use WC_Order;

/**
 * The Marketplaces order webhook subscriber.
 */
class OrderWebhookSubscriber extends AbstractWebhookSubscriber implements ComponentContract
{
    /**
     * Maybe creates an order in WooCommerce from a new order placed through GDM.
     *
     * @param AbstractWebhookReceivedEvent $event
     * @return void
     * @throws AdapterException
     */
    public function handlePayload(AbstractWebhookReceivedEvent $event)
    {
        if (! $this->isOrderEvent($event)) {
            return;
        }

        // @TODO do we need to call the Order::save() method to trigger the associated ModelEvent here? {unfulvio 2022-05-19}
        $coreOrder = MarketplacesOrderAdapter::getNewInstance($event->getPayloadDecoded())->convertFromSource();
        $wcOrder = OrderAdapter::getNewInstance(new WC_Order())->convertToSource($coreOrder);

        $wcOrder->save();
    }

    /**
     * Determines if the webhook received is for an order event.
     *
     * @param AbstractWebhookReceivedEvent $event
     * @return bool
     */
    protected function isOrderEvent(AbstractWebhookReceivedEvent $event) : bool
    {
        // @TODO implement this method in MWC-5971 {unfulvio 2022-05-19}
        return false;
    }

    /**
     * {@inheritDoc}
     */
    public function load()
    {
        // no-op
    }
}
