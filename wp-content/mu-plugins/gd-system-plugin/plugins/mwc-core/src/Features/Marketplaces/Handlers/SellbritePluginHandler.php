<?php

namespace GoDaddy\WordPress\MWC\Core\Features\Marketplaces\Handlers;

use Exception;
use GoDaddy\WordPress\MWC\Common\Components\Contracts\ComponentContract;
use GoDaddy\WordPress\MWC\Common\Events\Events;
use GoDaddy\WordPress\MWC\Common\Extensions\Types\PluginExtension;
use GoDaddy\WordPress\MWC\Common\Models\User;
use GoDaddy\WordPress\MWC\Common\Register\Register;
use GoDaddy\WordPress\MWC\Common\Repositories\WordPress\SiteRepository;
use GoDaddy\WordPress\MWC\Core\Admin\Notices as AdminNotices;
use GoDaddy\WordPress\MWC\Core\Events\PluginDeactivatedEvent;
use GoDaddy\WordPress\MWC\Core\Features\Marketplaces\Pages\MarketplacesPage;

/**
 * The Sellbrite plugin handler.
 */
class SellbritePluginHandler implements ComponentContract
{
    /** @var string the community plugin name */
    protected $pluginName = 'sellbrite/sellbrite.php';

    /** @var string the community plugin slug */
    protected $pluginSlug = 'sellbrite';

    /** @var string flag used for when the Sellbrite plugin was automatically deactivated */
    protected $pluginDeactivationFlag = 'mwc_sellbrite_plugin_deactivated';

    /**
     * Loads the component and adds hooks.
     *
     * @throws Exception
     */
    public function load()
    {
        Register::action()
            ->setGroup('admin_init')
            ->setHandler([$this, 'maybeDeactivatePlugin'])
            ->execute();

        Register::action()
            ->setGroup('admin_notices')
            ->setHandler([$this, 'displayNoticeUponPluginDeactivation'])
            ->execute();
    }

    /**
     * May deactivate the Sellbrite plugin.
     *
     * @internal
     *
     * @return void
     * @throws Exception
     */
    public function maybeDeactivatePlugin() : void
    {
        if (function_exists('deactivate_plugins') && $this->isSellbritePluginActivated()) {
            deactivate_plugins($this->pluginName);

            update_option($this->pluginDeactivationFlag, true);

            $this->broadcastDeactivationEvent();
        }
    }

    /**
     * Checks if the Sellbrite plugin is activated.
     *
     * @return bool
     */
    protected function isSellbritePluginActivated() : bool
    {
        return function_exists('is_plugin_active') && is_plugin_active($this->pluginName);
    }

    /**
     * Broadcasts a plugin deactivation event.
     *
     * @return void
     * @throws Exception
     */
    protected function broadcastDeactivationEvent() : void
    {
        $pluginExtension = (new PluginExtension())
            ->setName($this->pluginName)
            ->setSlug($this->pluginSlug);

        Events::broadcast(new PluginDeactivatedEvent($pluginExtension));
    }

    /**
     * Displays a notice to admins if the Sellbrite plugin has been deactivated.
     *
     * @TODO this notice CTA must be updated with a working link to the Marketplaces page {unfulvio 2022-05-11}
     *
     * @internal
     *
     * @return void
     */
    public function displayNoticeUponPluginDeactivation() : void
    {
        if (! get_option($this->pluginDeactivationFlag) || ! current_user_can('manage_woocommerce') || AdminNotices::isNoticeDismissed(User::getCurrent(), $this->pluginDeactivationFlag)) {
            return;
        }

        $marketplacesPageUrl = SiteRepository::getAdminUrl('admin.php?page='.MarketplacesPage::SLUG); ?>
        <div class="notice notice-info is-dismissible" data-message-id="mwc_sellbrite_plugin_deactivated">
            <p><strong><?php esc_html_e('Your products, everywhere.', 'mwc-core'); ?></strong></p>
            <p><?php esc_html_e('The Sellbrite plugin is included in your hosting plan as GoDaddy Marketplaces. The plugin has been deactivated.', 'mwc-core'); ?></p>
            <p><a href="<?php echo esc_url($marketplacesPageUrl); ?>" class="button button-primary"><?php esc_html_e('Set up', 'mwc-core'); ?></a></p>
        </div>
        <?php
    }
}
