<?php

namespace GoDaddy\WordPress\MWC\Core\Features\Marketplaces\Http\Requests;

use GoDaddy\WordPress\MWC\Common\Http\Request;

/**
 * Marketplaces request class.
 */
class GoDaddyMarketplacesRequest extends Request
{
}
