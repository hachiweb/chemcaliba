<?php

namespace GoDaddy\WordPress\MWC\Core\Features\Marketplaces\Repositories;

use GoDaddy\WordPress\MWC\Common\Helpers\ArrayHelper;
use GoDaddy\WordPress\MWC\Core\Features\Marketplaces\Models\Listing;
use GoDaddy\WordPress\MWC\Core\WooCommerce\Models\Products\Product;

class ListingRepository
{
    /**
     * Gets the product listing(s) by channel.
     *
     * @param Product $product
     * @param string $channel
     * @param bool $onlyPublished
     * @return Listing[]
     */
    public static function getProductListingsByChannel(Product $product, string $channel, bool $onlyPublished = false) : array
    {
        return ArrayHelper::where($product->getMarketplacesListings(), function ($listing) use ($channel, $onlyPublished) {
            /* @var Listing $listing */
            return (! $onlyPublished || $listing->isPublished())
                   && $channel === $listing->getChannelType();
        }, false);
    }
}
