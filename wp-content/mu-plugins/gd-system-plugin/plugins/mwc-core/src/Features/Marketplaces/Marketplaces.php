<?php

namespace GoDaddy\WordPress\MWC\Core\Features\Marketplaces;

use Exception;
use GoDaddy\WordPress\MWC\Common\Components\Exceptions\ComponentClassesNotDefinedException;
use GoDaddy\WordPress\MWC\Common\Components\Exceptions\ComponentLoadFailedException;
use GoDaddy\WordPress\MWC\Common\Components\Traits\HasComponentsTrait;
use GoDaddy\WordPress\MWC\Common\Features\AbstractFeature;
use GoDaddy\WordPress\MWC\Common\Helpers\ArrayHelper;
use GoDaddy\WordPress\MWC\Common\Models\User;
use GoDaddy\WordPress\MWC\Common\Register\Register;
use GoDaddy\WordPress\MWC\Common\Repositories\WooCommerceRepository;
use GoDaddy\WordPress\MWC\Common\Repositories\WordPress\SiteRepository;
use GoDaddy\WordPress\MWC\Core\Admin\Notices;
use GoDaddy\WordPress\MWC\Core\Features\Marketplaces\Events\Subscribers\ListingWebhookSubscriber;
use GoDaddy\WordPress\MWC\Core\Features\Marketplaces\Events\Subscribers\OrderWebhookSubscriber;
use GoDaddy\WordPress\MWC\Core\Features\Marketplaces\Handlers\SellbritePluginHandler;
use GoDaddy\WordPress\MWC\Core\Features\Marketplaces\Interceptors\EmailInterceptor;
use GoDaddy\WordPress\MWC\Core\Features\Marketplaces\Interceptors\OrderInterceptor;
use GoDaddy\WordPress\MWC\Core\Features\Marketplaces\Interceptors\ProductBulkSyncActionInterceptor;
use GoDaddy\WordPress\MWC\Core\Features\Marketplaces\Pages\EditProduct\MarketplacesMetabox;
use GoDaddy\WordPress\MWC\Core\Features\Marketplaces\Pages\MarketplacesPage;
use GoDaddy\WordPress\MWC\Core\Features\Marketplaces\Pages\Orders\Columns\SalesChannelColumn;

/**
 * The GoDaddy Marketplaces feature class.
 */
class Marketplaces extends AbstractFeature
{
    use HasComponentsTrait;

    /** @var array alphabetically ordered list of components to load */
    protected $componentClasses = [
        EmailInterceptor::class,
        ListingWebhookSubscriber::class,
        MarketplacesMetabox::class,
        MarketplacesPage::class,
        OrderInterceptor::class,
        OrderWebhookSubscriber::class,
        ProductBulkSyncActionInterceptor::class,
        SellbritePluginHandler::class,
        SalesChannelColumn::class,
    ];

    /**
     * {@inheritDoc}
     */
    public static function shouldLoad() : bool
    {
        return parent::shouldLoad()
               && 'usd' === strtolower(WooCommerceRepository::getCurrency())
               && 'us' === strtolower(WooCommerceRepository::getBaseCountry())
               && ArrayHelper::contains(['oz', 'lbs'], get_option('woocommerce_weight_unit'))
               && ArrayHelper::contains(['in', 'yds'], get_option('woocommerce_dimension_unit'));
    }

    /**
     * {@inheritDoc}
     */
    public static function getName() : string
    {
        return 'marketplaces';
    }

    /**
     * Loads the feature.
     *
     * @return void
     * @throws ComponentClassesNotDefinedException|ComponentLoadFailedException|Exception
     */
    public function load()
    {
        $this->loadComponents();

        Register::action()
            ->setGroup('admin_notices')
            ->setHandler([$this, 'displayNewFeatureAdminNotice'])
            ->execute();
    }

    /**
     * Displays a dismissible admin notice about the new feature.
     *
     * @return void
     */
    public function displayNewFeatureAdminNotice() : void
    {
        $noticeId = 'mwc_marketplaces_new_feature';

        if (! current_user_can('manage_woocommerce') || Notices::isNoticeDismissed(User::getCurrent(), $noticeId)) {
            return;
        }

        $productsPageUrl = SiteRepository::getAdminUrl('edit.php?post_type=product'); ?>
        <div class="notice notice-info is-dismissible" data-message-id="<?php echo esc_attr($noticeId); ?>">
            <p><strong><?php esc_html_e('Welcome to GoDaddy Marketplaces!', 'mwc-core'); ?></strong></p>
            <p><?php printf(
                /* translators: Placeholders: %1$s - opening HTML link tag <a> to products page, %2$s - closing HTML link tag </a> */
                __('Create your first product listing by adding a new product or editing an existing one in WooCommerce. You can list a product on each of the marketplaces you\'ve added. Once listed, the product info and inventory will sync automatically. Visit %1$sProducts%2$s to get started.', 'mwc-core'),
                '<a href="'.esc_url($productsPageUrl).'">',
                    '</a>'
                ); ?></p>
        </div>
        <?php
    }
}
