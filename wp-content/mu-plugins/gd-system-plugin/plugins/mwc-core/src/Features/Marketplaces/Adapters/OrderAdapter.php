<?php

namespace GoDaddy\WordPress\MWC\Core\Features\Marketplaces\Adapters;

use DateTime;
use Exception;
use GoDaddy\WordPress\MWC\Common\Contracts\FulfillmentStatusContract;
use GoDaddy\WordPress\MWC\Common\Contracts\OrderStatusContract;
use GoDaddy\WordPress\MWC\Common\DataSources\Contracts\DataSourceAdapterContract;
use GoDaddy\WordPress\MWC\Common\DataSources\WooCommerce\Adapters\AddressAdapter;
use GoDaddy\WordPress\MWC\Common\Exceptions\AdapterException;
use GoDaddy\WordPress\MWC\Common\Helpers\ArrayHelper;
use GoDaddy\WordPress\MWC\Common\Models\Address;
use GoDaddy\WordPress\MWC\Common\Models\Orders\LineItem;
use GoDaddy\WordPress\MWC\Common\Traits\CanGetNewInstanceTrait;
use GoDaddy\WordPress\MWC\Core\Features\Marketplaces\Traits\ConvertsMarketplaceAmountTrait;
use GoDaddy\WordPress\MWC\Core\WooCommerce\Models\Orders\Order;
use GoDaddy\WordPress\MWC\Shipping\Models\Orders\FulfillmentStatuses\FulfilledFulfillmentStatus;
use GoDaddy\WordPress\MWC\Shipping\Models\Orders\FulfillmentStatuses\PartiallyFulfilledFulfillmentStatus;
use GoDaddy\WordPress\MWC\Shipping\Models\Orders\FulfillmentStatuses\UnfulfilledFulfillmentStatus;

/**
 * Adapts data from a GDM webhook to a native core order object.
 */
class OrderAdapter implements DataSourceAdapterContract
{
    use CanGetNewInstanceTrait, ConvertsMarketplaceAmountTrait;

    /** @var array Order data from the webhook */
    protected $source;

    /**
     * OrderAdapter constructor.
     *
     * @param array $order Order data from the webhook payload.
     */
    public function __construct(array $order)
    {
        $this->source = $order;
    }

    /**
     * Converts a source order from a GDM payload to a core native order.
     *
     * @return Order
     * @throws AdapterException
     */
    public function convertFromSource() : Order
    {
        $order = new Order();

        if ($status = $this->convertStatusFromSource()) {
            $order->setStatus($status);
        }

        // Dates
        try {
            if ($createdAt = ArrayHelper::get($this->source, 'ordered_at')) {
                $order->setCreatedAt(new DateTime($createdAt));
            }
        } catch (Exception $exception) {
            throw new AdapterException($exception->getMessage());
        }

        // Customer information.
        if ($email = ArrayHelper::get($this->source, 'billing_email')) {
            $order->setEmailAddress($email);
        }

        // Addresses
        $order->setBillingAddress($this->convertBillingAddressFromSource())
            ->setShippingAddress($this->convertShippingAddressFromSource());

        // Order items.
        $order->setLineItems($this->convertOrderItemsFromSource());

        // Fulfillment.
        $order->setFulfillmentStatus($this->convertFulfillmentStatusFromSource());

        // Order notes.
        if ($orderNotes = ArrayHelper::get($this->source, 'customer_notes')) {
            $order->setOrderNotes((string) $orderNotes);
        }

        // Order amounts.
        $order->setLineAmount($this->parseAndConvertAmountFromSource('subtotal'))
            ->setDiscountAmount($this->parseAndConvertAmountFromSource('discount'))
            ->setShippingAmount($this->parseAndConvertAmountFromSource('shipping_cost'))
            ->setTaxAmount($this->parseAndConvertAmountFromSource('tax'))
            ->setTotalAmount($this->parseAndConvertAmountFromSource('total'));

        // Marketplace data.
        if ($marketplaceOrderNumber = ArrayHelper::get($this->source, 'order_id')) { // @TODO Confirm this key. I made it up because it's not in the payload. {agibson 2022-05-16}
            $order->setMarketplaceOrderNumber($marketplaceOrderNumber);
        }
        if ($marketplaceChannelUuid = ArrayHelper::get($this->source, 'channel_uuid')) {
            $order->setMarketplaceChannelUuid($marketplaceChannelUuid);
        }
        if ($marketplaceChannelName = ArrayHelper::get($this->source, 'channel_name')) {
            $order->setMarketplaceChannelName($marketplaceChannelName);
        }
        if ($marketplaceChannelType = ArrayHelper::get($this->source, 'channel_type_display_name')) {
            $order->setMarketplaceChannelType($marketplaceChannelType);
        }

        return $order;
    }

    /**
     * Converts the GDM source key to a native OrderStatusContract interface.
     *
     * @return OrderStatusContract|null
     */
    protected function convertStatusFromSource() : ?OrderStatusContract
    {
        // @TODO Verify this key is the correct one in MWC-5935 {agibson 2022-05-16}
        if (! $statusName = ArrayHelper::get($this->source, 'sb_status')) {
            return null;
        }

        // Order status relationships. Key is what we'll find in the source, value is the `OrderStatusContract` class name.
        // @TODO Set this value in MWC-5935 when we have a list of available status keys. {agibson 2022-05-16}
        $statuses = [];

        if (! $statusClass = ArrayHelper::get($statuses, $statusName)) {
            return null;
        }

        if (! class_exists($statusClass)) {
            return null;
        }

        return new $statusClass();
    }

    /**
     * Converts the GDM billing address to a native Address model.
     *
     * @return Address
     */
    protected function convertBillingAddressFromSource() : Address
    {
        $addressData = [
            'company'    => ArrayHelper::get($this->source, 'billing_company'),
            'first_name' => ArrayHelper::get($this->source, 'billing_contact_name'),
            'address_1'  => ArrayHelper::get($this->source, 'billing_address_1'),
            'address_2'  => ArrayHelper::get($this->source, 'billing_address_2'),
            'city'       => ArrayHelper::get($this->source, 'billing_city'),
            'state'      => ArrayHelper::get($this->source, 'billing_state_region'),
            'postcode'   => ArrayHelper::get($this->source, 'billing_postal_code'),
            'country'    => ArrayHelper::get($this->source, 'billing_country_code'),
            'phone'      => ArrayHelper::get($this->source, 'billing_phone_number'),
        ];

        return (new AddressAdapter(array_filter($addressData)))->convertFromSource();
    }

    /**
     * Converts the GDM shipping address to a native Address model.
     *
     * @return Address
     */
    protected function convertShippingAddressFromSource() : Address
    {
        $addressData = [
            'company'    => ArrayHelper::get($this->source, 'shipping_company_name'),
            'first_name' => ArrayHelper::get($this->source, 'shipping_contact_name'),
            'address_1'  => ArrayHelper::get($this->source, 'shipping_address_1'),
            'address_2'  => ArrayHelper::get($this->source, 'shipping_address_2'),
            'city'       => ArrayHelper::get($this->source, 'shipping_city'),
            'state'      => ArrayHelper::get($this->source, 'shipping_state_region'),
            'postcode'   => ArrayHelper::get($this->source, 'shipping_postal_code'),
            'country'    => ArrayHelper::get($this->source, 'shipping_country_code'),
            'phone'      => ArrayHelper::get($this->source, 'shipping_phone_number'),
        ];

        return (new AddressAdapter(array_filter($addressData)))->convertFromSource();
    }

    /**
     * Converts the GDM order items into native order items counterparts.
     *
     * @return array
     */
    protected function convertOrderItemsFromSource() : array
    {
        $lineItems = ArrayHelper::get($this->source, 'items');

        $items = [];

        if (ArrayHelper::accessible($lineItems)) {
            foreach ($lineItems as $lineItem) {
                $items[] = $this->convertOrderItemFromSource($lineItem);
            }
        }

        return $items;
    }

    /**
     * Converts a source line item into a native LineItem object.
     *
     * @param array $item Single line item from the source.
     * @return LineItem
     */
    protected function convertOrderItemFromSource(array $item) : LineItem
    {
        return (new LineItemAdapter($item))->convertFromSource();
    }

    /**
     * Converts the GDM fulfillment status into a native FulfillmentStatusContract.
     *
     * @return FulfillmentStatusContract|null
     */
    protected function convertFulfillmentStatusFromSource() : ?FulfillmentStatusContract
    {
        if (! $statusName = ArrayHelper::get($this->source, 'shipment_status')) {
            return null;
        }

        // Fulfillment status relationships. Key is what we'll find in the source, value is the `FulfillmentStatusContract` class name.
        $statuses = [
            'all'     => FulfilledFulfillmentStatus::class,
            'partial' => PartiallyFulfilledFulfillmentStatus::class,
            'none'    => UnfulfilledFulfillmentStatus::class,
        ];

        if (! $statusClass = ArrayHelper::get($statuses, $statusName)) {
            return null;
        }

        if (! class_exists($statusClass)) {
            return null;
        }

        return new $statusClass();
    }

    /**
     * {@inheritDoc}
     */
    public function convertToSource()
    {
        // Not implemented.
        return [];
    }
}
