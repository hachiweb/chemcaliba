<?php

namespace GoDaddy\WordPress\MWC\Core\Features\EmailNotifications\API\Auth\Providers;

use Exception;
use Firebase\JWT\JWK;
use Firebase\JWT\JWT;
use GoDaddy\WordPress\MWC\Common\Helpers\ArrayHelper;
use GoDaddy\WordPress\MWC\Common\Traits\CanGetNewInstanceTrait;
use GoDaddy\WordPress\MWC\Core\Features\EmailNotifications\API\Auth\Cache\CacheJwk;
use GoDaddy\WordPress\MWC\Core\Features\EmailNotifications\API\Auth\Http\Requests\GetJwkRequest;

/**
 * Decodes a JWT token using the MWC API JWK.
 */
class JwtAuthProvider
{
    use CanGetNewInstanceTrait;

    /**
     * Gets the JWK from cache or make a request to the MWC API to get them.
     *
     * @return array
     * @throws Exception
     */
    protected function getJwk() : array
    {
        $cache = CacheJwk::getInstance();

        if ($cachedBody = $cache->get()) {
            return $cachedBody;
        }

        try {
            $response = GetJwkRequest::getNewInstance()->send();
        } catch (Exception $exception) {
            return [];
        }

        $body = $response->getBody();

        if (! empty(ArrayHelper::get($body, 'keys'))) {
            $cache->set($body);
        }

        return $body;
    }

    /**
     * Decodes a JWT token with a known JWK (retrieved from the MWC API).
     *
     * @param string $token
     * @return array
     * @throws Exception
     */
    public function decodeToken(string $token) : array
    {
        return (array) JWT::decode($token, JWK::parseKeySet($this->getJwk()), ['RS256']);
    }
}
