<?php

namespace GoDaddy\WordPress\MWC\Core\Auth\ManagedWooCommerce\Models;

use GoDaddy\WordPress\MWC\Common\Helpers\DeprecationHelper;
use GoDaddy\WordPress\MWC\Common\Models\AbstractModel;
use GoDaddy\WordPress\MWC\Common\Traits\CanBulkAssignPropertiesTrait;

/**
 * Managed WooCommerce JWT Token.
 *
 * TODO: remove this class after 2022-11-09 or on version 4.0 {llessa 2022-05-09}
 *
 * @deprecated
 */
class Token extends AbstractModel
{
    use CanBulkAssignPropertiesTrait;
    /** @var string A JWT Access token */
    protected $accessToken = '';

    /** @var string The list of scopes for the JWT separated by space */
    protected $scope = '';

    /** @var string The ID for the token */
    protected $tokenId = '';

    /** @var string The type of token used */
    protected $tokenType = 'BEARER';

    /** @var int The expiration timestamp */
    protected $expiration = 0;

    /**
     * Constructor.
     */
    final public function __construct()
    {
        DeprecationHelper::deprecatedClass(__CLASS__, '3.3.1');

        // to prevent overriding the constructor.
    }

    /**
     * Retrieves the access token.
     *
     * @return string The token value.
     *
     * @deprecated
     */
    public function getAccessToken() : string
    {
        DeprecationHelper::deprecatedFunction(__FUNCTION__, '3.3.1');

        return $this->accessToken;
    }

    /**
     * Retrieves the list of scopes, as a string.
     *
     * @return string List of scopes, separated by a space.
     *
     * @deprecated
     */
    public function getScope() : string
    {
        DeprecationHelper::deprecatedFunction(__FUNCTION__, '3.3.1');

        return $this->scope;
    }

    /**
     * Retrieves the access token ID.
     *
     * @return string The ID.
     *
     * @deprecated
     */
    public function getTokenId() : string
    {
        DeprecationHelper::deprecatedFunction(__FUNCTION__, '3.3.1');

        return $this->tokenId;
    }

    /**
     * Retrieves the access token type.
     *
     * @return string The type.
     *
     * @deprecated
     */
    public function getTokenType() : string
    {
        DeprecationHelper::deprecatedFunction(__FUNCTION__, '3.3.1');

        return $this->tokenType;
    }

    /**
     * Retrieves the expiration timestamp.
     *
     * @return int The expiration.
     *
     * @deprecated
     */
    public function getExpiration() : int
    {
        DeprecationHelper::deprecatedFunction(__FUNCTION__, '3.3.1');

        return $this->expiration;
    }

    /**
     * Sets the access token.
     *
     * @return $this The token instance.
     *
     * @deprecated
     */
    public function setAccessToken($token) : Token
    {
        DeprecationHelper::deprecatedFunction(__FUNCTION__, '3.3.1');

        $this->accessToken = $token;

        return $this;
    }

    /**
     * Sets the scope.
     *
     * @return $this The token instance.
     *
     * @deprecated
     */
    public function setScope($scope) : Token
    {
        DeprecationHelper::deprecatedFunction(__FUNCTION__, '3.3.1');

        $this->scope = $scope;

        return $this;
    }

    /**
     * Sets the token ID.
     *
     * @return $this The token instance.
     *
     * @deprecated
     */
    public function setTokenId($tokenId) : Token
    {
        DeprecationHelper::deprecatedFunction(__FUNCTION__, '3.3.1');

        $this->tokenId = $tokenId;

        return $this;
    }

    /**
     * Sets the token type.
     *
     * @return $this The token instance.
     *
     * @deprecated
     */
    public function setTokenType($tokenType) : Token
    {
        DeprecationHelper::deprecatedFunction(__FUNCTION__, '3.3.1');

        $this->tokenType = $tokenType;

        return $this;
    }

    /**
     * Sets the expiration timestamp.
     *
     * @return $this The token instance.
     *
     * @deprecated
     */
    public function setExpiration($expiration) : Token
    {
        DeprecationHelper::deprecatedFunction(__FUNCTION__, '3.3.1');

        $this->expiration = $expiration;

        return $this;
    }

    /**
     * {@inheritDoc}
     *
     * @deprecated
     */
    public function toArray() : array
    {
        DeprecationHelper::deprecatedFunction(__FUNCTION__, '3.3.1');

        $data = parent::toArray();
        $data['expiresIn'] = $this->getExpiresIn();

        return $data;
    }

    /**
     * Retrieves the number of seconds before this token expires, based on the expiration date.
     *
     * @return int Seconds before this token expires.
     *
     * @deprecated
     */
    public function getExpiresIn() : int
    {
        DeprecationHelper::deprecatedFunction(__FUNCTION__, '3.3.1');

        return $this->getExpiration() - time();
    }
}
