<?php

namespace GoDaddy\WordPress\MWC\Core\Auth\ManagedWooCommerce\Cache\Types;

use GoDaddy\WordPress\MWC\Common\Cache\Cache;
use GoDaddy\WordPress\MWC\Common\Cache\Contracts\CacheableContract;
use GoDaddy\WordPress\MWC\Common\Helpers\DeprecationHelper;

/**
 * Managed WooCommerce site access token cache handler class.
 *
 * TODO: remove this class after 2022-11-09 or on version 4.0 {llessa 2022-05-09}
 *
 * @deprecated
 */
class CacheToken extends Cache implements CacheableContract
{
    /** @var int how long in seconds should the cache be kept for */
    protected $expires = 300;

    /**
     * Constructor.
     *
     * @param int $userId
     * @deprecated
     */
    final public function __construct(int $userId = null)
    {
        DeprecationHelper::deprecatedClass(__CLASS__, '3.3.1');

        $this->type('platform_jwt');
        $this->key($userId ? "platform_jwt_{$userId}" : 'platform_jwt_site');
    }

    /**
     * Creates a new MWC token cache for a given user ID.
     *
     * @param int $userId
     * @return CacheToken
     */
    public static function for(int $userId = null)
    {
        return new static($userId);
    }
}
