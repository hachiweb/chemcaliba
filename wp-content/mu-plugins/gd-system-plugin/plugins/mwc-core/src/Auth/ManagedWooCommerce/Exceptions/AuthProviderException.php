<?php

namespace GoDaddy\WordPress\MWC\Core\Auth\ManagedWooCommerce\Exceptions;

/**
 * Exception to report to Sentry that an error occurred trying to authenticate with MWC site.
 *
 * TODO: remove this class after 2022-11-09 or on version 4.0 {wvega 2022-05-09}
 *
 * @deprecated use {@see \GoDaddy\WordPress\MWC\Common\Auth\Exceptions\AuthProviderException} instead
 */
class AuthProviderException extends AuthException
{
}
