<?php

namespace GoDaddy\WordPress\MWC\Core\Auth\ManagedWooCommerce\Cache\Types;

use GoDaddy\WordPress\MWC\Common\Cache\Cache;
use GoDaddy\WordPress\MWC\Common\Cache\Contracts\CacheableContract;
use GoDaddy\WordPress\MWC\Common\Helpers\DeprecationHelper;
use GoDaddy\WordPress\MWC\Common\Traits\IsSingletonTrait;

/**
 * Cache for errors received trying to authenticate against the MWC API.
 *
 * TODO: remove this class after 2022-11-09 or on version 4.0 {wvega 2022-05-09}
 *
 * @deprecated
 */
class CacheErrorResponse extends Cache implements CacheableContract
{
    use IsSingletonTrait;

    /** @var string the type of object we are caching */
    protected $type = 'platform_jwt_error';

    /** @var int how long in seconds should the cache be kept for */
    protected $expires = 900;

    /**
     * Constructor.
     *
     * @deprecated
     */
    public function __construct()
    {
        DeprecationHelper::deprecatedClass(__CLASS__, '3.3.1');

        $this->key($this->type);
    }
}
