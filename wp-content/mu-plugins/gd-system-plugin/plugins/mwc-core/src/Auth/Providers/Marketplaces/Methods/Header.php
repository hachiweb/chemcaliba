<?php

namespace GoDaddy\WordPress\MWC\Core\Auth\Providers\Marketplaces\Methods;

use Exception;
use GoDaddy\WordPress\MWC\Common\Auth\Contracts\AuthMethodContract;
use GoDaddy\WordPress\MWC\Common\Http\Contracts\RequestContract;
use GoDaddy\WordPress\MWC\Core\Auth\Providers\Marketplaces\Models\Credentials;

/**
 * Authentication method for GDM webhook (incoming) and API (outgoing) requests.
 */
class Header implements AuthMethodContract
{
    /** @var string */
    const HEADER_NAME = 'HTTP_GDM_SIGNATURE';

    /** @var Credentials */
    protected $credentials;

    /**
     * @param Credentials $credentials a credentials object that holds the customer ID and venture ID for the site
     */
    public function __construct(Credentials $credentials)
    {
        $this->credentials = $credentials;
    }

    /**
     * Generates a signature by hashing the payload.
     *
     * @param string $encodedPayload The JSON encoded request payload.
     * @return string
     */
    public function getSignature(string $encodedPayload) : string
    {
        return base64_encode(hash_hmac('sha256', $encodedPayload, $this->credentials->getKey(), true));
    }

    /**
     * Prepares the given outgoing request to use this authentication method.
     *
     * @param RequestContract $request request instance
     * @return RequestContract
     */
    public function prepare(RequestContract $request) : RequestContract
    {
        try {
            $request->addHeaders([
                static::HEADER_NAME => $this->getSignature(json_encode($request->body)),
            ]);
        } catch (Exception $exception) {
            // ignore unlikely exception: addHeaders() can only throw if the value of the headers property or the
            // additional headers parameter is not an array
        }

        return $request;
    }
}
