<?php

namespace GoDaddy\WordPress\MWC\Core\Auth\Providers\EmailsService;

use GoDaddy\WordPress\MWC\Common\Auth\Providers\EmailsService\AbstractAuthProvider;
use GoDaddy\WordPress\MWC\Common\Configuration\Configuration;
use GoDaddy\WordPress\MWC\Common\Contracts\GraphQLOperationContract;
use GoDaddy\WordPress\MWC\Common\Repositories\ManagedWooCommerceRepository;
use GoDaddy\WordPress\MWC\Core\Email\Http\GraphQL\Mutations\IssueTokenForSiteMutation;

class AuthProvider extends AbstractAuthProvider
{
    /**
     * {@inheritDoc}
     */
    protected function getIssueTokenForSiteMutation() : GraphQLOperationContract
    {
        return (new IssueTokenForSiteMutation())
            ->setVariables([
                'siteId'    => ManagedWooCommerceRepository::getSiteId(),
                'uid'       => Configuration::get('godaddy.account.uid'),
                'siteToken' => Configuration::get('godaddy.site.token', 'empty'),
            ]);
    }
}
