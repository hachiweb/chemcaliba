<?php

namespace GoDaddy\WordPress\MWC\Core\Auth\Providers\ManagedWooCommerce\Methods;

use Exception;
use GoDaddy\WordPress\MWC\Common\Auth\Contracts\AuthMethodContract;
use GoDaddy\WordPress\MWC\Common\Http\Contracts\RequestContract;
use GoDaddy\WordPress\MWC\Core\Auth\Providers\ManagedWooCommerce\Models\SiteCredentials;

/**
 * Authentication method for MWC API requests.
 */
class SiteHeaders implements AuthMethodContract
{
    /** @var SiteCredentials */
    protected $credentials;

    /**
     * @param SiteCredentials $credentials a credentials object that holds the Account UID and Site Token for the site
     */
    public function __construct(SiteCredentials $credentials)
    {
        $this->credentials = $credentials;
    }

    /**
     * Prepares the given request to use site headers as the authentication method.
     *
     * @param RequestContract $request request instance
     * @return RequestContract
     */
    public function prepare(RequestContract $request) : RequestContract
    {
        try {
            $request->addHeaders([
                'X-Account-UID' => $this->credentials->getAccountUid(),
                'X-Site-Token'  => $this->credentials->getSiteToken(),
            ]);
        } catch (Exception $exception) {
            // ignore unlikely exception: addHeaders() can only throw if the value of the headers property or the
            // additional headers parameter is not an array
        }

        return $request;
    }
}
