<?php

namespace GoDaddy\WordPress\MWC\Core\Auth\Providers\ManagedWooCommerce;

use GoDaddy\WordPress\MWC\Common\Auth\Contracts\AuthCredentialsContract;
use GoDaddy\WordPress\MWC\Common\Auth\Contracts\AuthMethodContract;
use GoDaddy\WordPress\MWC\Common\Auth\Contracts\AuthProviderContract;
use GoDaddy\WordPress\MWC\Common\Configuration\Configuration;
use GoDaddy\WordPress\MWC\Core\Auth\Providers\ManagedWooCommerce\Methods\SiteHeaders;
use GoDaddy\WordPress\MWC\Core\Auth\Providers\ManagedWooCommerce\Models\SiteCredentials;

class AuthProvider implements AuthProviderContract
{
    /**
     * {@inheritDoc}
     */
    public function getMethod() : AuthMethodContract
    {
        return new SiteHeaders($this->getCredentials());
    }

    /**
     * Retrieves the credentials.
     *
     * @return SiteCredentials
     */
    public function getCredentials() : AuthCredentialsContract
    {
        return (new SiteCredentials())
            ->setAccountUid(Configuration::get('godaddy.account.uid', ''))
            ->setSiteToken(Configuration::get('godaddy.site.token', ''));
    }

    /**
     * deleteCredentials will do nothing in this case, because the credentials are always available as constants, so we
     * can’t delete them and don’t need to store them in the cache either.
     */
    public function deleteCredentials() : void
    {
        // NOOP
    }
}
