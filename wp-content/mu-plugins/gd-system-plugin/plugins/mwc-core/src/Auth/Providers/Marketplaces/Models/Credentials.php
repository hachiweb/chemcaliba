<?php

namespace GoDaddy\WordPress\MWC\Core\Auth\Providers\Marketplaces\Models;

use GoDaddy\WordPress\MWC\Common\Auth\Contracts\AuthCredentialsContract;
use GoDaddy\WordPress\MWC\Common\Models\AbstractModel;

/**
 * Credentials used to validate requests from GDM and sign requests to GDM.
 */
class Credentials extends AbstractModel implements AuthCredentialsContract
{
    /* @var string customer ID */
    protected $customerId = '';

    /* @var string venture ID */
    protected $ventureId = '';

    /**
     * Gets the customer ID.
     *
     * @return string
     */
    public function getCustomerId() : string
    {
        return $this->customerId;
    }

    /**
     * Gets the venture ID.
     *
     * @return string
     */
    public function getVentureId() : string
    {
        return $this->ventureId;
    }

    /**
     * Sets the customer ID.
     *
     * @param string $value
     * @return Credentials
     */
    public function setCustomerId(string $value) : Credentials
    {
        $this->customerId = $value;

        return $this;
    }

    /**
     * Sets the venture ID.
     *
     * @param string $value
     * @return Credentials
     */
    public function setVentureId(string $value) : Credentials
    {
        $this->ventureId = $value;

        return $this;
    }

    /**
     * Gets the composite key used to generate the signature header.
     */
    public function getKey() : string
    {
        return "{$this->getCustomerId()}+{$this->getVentureId()}";
    }
}
