<?php

namespace GoDaddy\WordPress\MWC\Core\Auth\Providers\Marketplaces;

use Exception;
use GoDaddy\WordPress\MWC\Common\Auth\Contracts\AuthCredentialsContract;
use GoDaddy\WordPress\MWC\Common\Auth\Contracts\AuthMethodContract;
use GoDaddy\WordPress\MWC\Common\Auth\Contracts\AuthProviderContract;
use GoDaddy\WordPress\MWC\Common\Auth\Exceptions\CredentialsCreateFailedException;
use GoDaddy\WordPress\MWC\Common\Repositories\ManagedWooCommerceRepository;
use GoDaddy\WordPress\MWC\Core\Auth\Providers\Marketplaces\Methods\Header;
use GoDaddy\WordPress\MWC\Core\Auth\Providers\Marketplaces\Models\Credentials;

/**
 * Marketplaces auth provider.
 */
class AuthProvider implements AuthProviderContract
{
    /**
     * Retrieves the authentication method.
     *
     * @return Header
     * @throws CredentialsCreateFailedException
     */
    public function getMethod() : AuthMethodContract
    {
        return new Header($this->getCredentials());
    }

    /**
     * Retrieves the credentials.
     *
     * @return Credentials
     * @throws CredentialsCreateFailedException
     * @throws Exception
     */
    public function getCredentials() : AuthCredentialsContract
    {
        return (new Credentials())
            ->setCustomerId(ManagedWooCommerceRepository::getCustomerId())
            ->setVentureId(ManagedWooCommerceRepository::getVentureId());
    }

    /**
     * Deletes the credentials.
     *
     * This method is not implemented because the credentials are always available as constants.
     * So we can't delete them and don't need to store them in the cache either.
     *
     * @return void
     */
    public function deleteCredentials() : void
    {
        // NOOP
    }
}
