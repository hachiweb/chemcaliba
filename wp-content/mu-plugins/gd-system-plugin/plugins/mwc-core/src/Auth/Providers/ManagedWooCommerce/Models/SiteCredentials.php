<?php

namespace GoDaddy\WordPress\MWC\Core\Auth\Providers\ManagedWooCommerce\Models;

use GoDaddy\WordPress\MWC\Common\Auth\Contracts\AuthCredentialsContract;
use GoDaddy\WordPress\MWC\Common\Models\AbstractModel;

class SiteCredentials extends AbstractModel implements AuthCredentialsContract
{
    /* @var string account UID */
    protected $accountUid = '';

    /* @var string site token */
    protected $siteToken = '';

    /**
     * @return string
     */
    public function getAccountUid() : string
    {
        return $this->accountUid;
    }

    /**
     * @return string
     */
    public function getSiteToken() : string
    {
        return $this->siteToken;
    }

    /**
     * @param string $value
     * @return SiteCredentials
     */
    public function setAccountUid(string $value) : SiteCredentials
    {
        $this->accountUid = $value;

        return $this;
    }

    /**
     * @param string $value
     * @return SiteCredentials
     */
    public function setSiteToken(string $value) : SiteCredentials
    {
        $this->siteToken = $value;

        return $this;
    }
}
