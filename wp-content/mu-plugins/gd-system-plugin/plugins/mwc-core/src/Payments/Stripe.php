<?php

namespace GoDaddy\WordPress\MWC\Core\Payments;

use GoDaddy\WordPress\MWC\Common\Configuration\Configuration;

/**
 * The primary Stripe payments integration class.
 */
class Stripe
{
    /**
     * Gets the connected account object.
     *
     * TODO: update when:
     * - The account object exists from MWC-5613
     * - The provider methods exists from MWC-5603
     *
     * @return null
     */
    public static function getAccount()
    {
        return null;
    }

    /**
     * Gets the account ID.
     *
     * @return string|null
     */
    public static function getAccountId() : ?string
    {
        return Configuration::get('payments.stripe.accountId');
    }

    /**
     * Sets the account ID.
     *
     * @param string $value
     */
    public static function setAccountId(string $value)
    {
        Configuration::set('payments.stripe.accountId', $value);

        update_option('mwc_payments_stripe_accountId', $value);
    }

    /**
     * Gets the API public key.
     *
     * @return string|null
     */
    public static function getApiPublicKey() : ?string
    {
        return Configuration::get('payments.stripe.api.publicKey');
    }

    /**
     * Sets the API public key.
     *
     * @param string $value
     */
    public static function setApiPublicKey(string $value)
    {
        Configuration::set('payments.stripe.api.publicKey', $value);

        update_option('mwc_payments_stripe_api_publicKey', $value);
    }

    /**
     * Gets the API secret key.
     *
     * @return string|null
     */
    public static function getApiSecretKey() : ?string
    {
        return Configuration::get('payments.stripe.api.secretKey');
    }

    /**
     * Sets the API secret key.
     *
     * @param string $value
     */
    public static function setApiSecretKey(string $value)
    {
        Configuration::set('payments.stripe.api.secretKey', $value);

        update_option('mwc_payments_stripe_api_secretKey', $value);
    }
}
