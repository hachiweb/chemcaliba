<?php

namespace GoDaddy\WordPress\MWC\Core\Payments\Stripe\Models;

use GoDaddy\WordPress\MWC\Common\Models\AbstractModel;
use GoDaddy\WordPress\MWC\Common\Traits\CanBulkAssignPropertiesTrait;

/**
 * The payment intent model.
 */
class PaymentIntent extends AbstractModel
{
    use CanBulkAssignPropertiesTrait;

    /** @var string */
    protected $id;

    /** @var int */
    protected $amount;

    /** @var string */
    protected $currency;

    /**
     * Gets the ID.
     *
     * @return string|null
     */
    public function getId() : ?string
    {
        return $this->id;
    }

    /**
     * Sets the ID.
     *
     * @param string $value
     * @return PaymentIntent
     */
    public function setId(string $value) : PaymentIntent
    {
        $this->id = $value;

        return $this;
    }

    /**
     * Gets the amount.
     *
     * @return int|null
     */
    public function getAmount() : ?int
    {
        return $this->amount;
    }

    /**
     * Sets the amount.
     *
     * @param int $value
     * @return PaymentIntent
     */
    public function setAmount(int $value) : PaymentIntent
    {
        $this->amount = $value;

        return $this;
    }

    /**
     * Gets the currency.
     *
     * @return int|null
     */
    public function getCurrency() : ?string
    {
        return $this->currency;
    }

    /**
     * Sets the currency.
     *
     * @param string $value
     * @return PaymentIntent
     */
    public function setCurrency(string $value) : PaymentIntent
    {
        $this->currency = $value;

        return $this;
    }
}
