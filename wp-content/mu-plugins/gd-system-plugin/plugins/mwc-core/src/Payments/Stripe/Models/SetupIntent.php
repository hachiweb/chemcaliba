<?php

namespace GoDaddy\WordPress\MWC\Core\Payments\Stripe\Models;

use GoDaddy\WordPress\MWC\Common\Models\AbstractModel;
use GoDaddy\WordPress\MWC\Common\Traits\CanBulkAssignPropertiesTrait;

class SetupIntent extends AbstractModel
{
    use CanBulkAssignPropertiesTrait;

    /** @var string */
    protected $clientSecret;

    /** @var string */
    protected $id;

    /** @var string */
    protected $status;

    /**
     * Gets the client secret.
     *
     * @return string|null
     */
    public function getClientSecret() : ?string
    {
        return $this->clientSecret;
    }

    /**
     * Sets the client secret.
     *
     * @param string $value
     * @return self
     */
    public function setClientSecret(string $value) : SetupIntent
    {
        $this->clientSecret = $value;

        return $this;
    }

    /**
     * Gets the ID.
     *
     * @return string|null
     */
    public function getId() : ?string
    {
        return $this->id;
    }

    /**
     * Sets the ID.
     *
     * @param string $value
     * @return self
     */
    public function setId(string $value) : SetupIntent
    {
        $this->id = $value;

        return $this;
    }

    /**
     * Gets the status.
     *
     * @return string|null
     */
    public function getStatus() : ?string
    {
        return $this->status;
    }

    /**
     * Sets the status.
     *
     * @param string $value
     * @return self
     */
    public function setStatus(string $value) : SetupIntent
    {
        $this->status = $value;

        return $this;
    }
}
