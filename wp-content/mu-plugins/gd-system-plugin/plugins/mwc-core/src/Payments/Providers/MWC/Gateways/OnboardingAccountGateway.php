<?php

namespace GoDaddy\WordPress\MWC\Core\Payments\Providers\MWC\Gateways;

use Exception;
use GoDaddy\WordPress\MWC\Common\Traits\CanGetNewInstanceTrait;
use GoDaddy\WordPress\MWC\Core\Payments\Providers\MWC\Http\Adapters\FindOrCreateOnboardingAccountRequestAdapter;
use GoDaddy\WordPress\MWC\Payments\Gateways\AbstractGateway;

/**
 * Gateway for handling onboarding accounts via the MWC API.
 */
class OnboardingAccountGateway extends AbstractGateway
{
    use CanGetNewInstanceTrait;

    /**
     * Finds or creates GoDaddy Payments account details for the given values.
     *
     * @param string $serviceId
     * @param string $webhookSecret
     *
     * @return array
     * @throws Exception
     */
    public function findOrCreate(string $serviceId, string $webhookSecret) : array
    {
        // TODO: remove this when the real data is available in MWC-4748 {@cwiseman 2022-03-18}
        throw new Exception('GoDaddy Payments account not found');

        return $this->doAdaptedRequest($serviceId, FindOrCreateOnboardingAccountRequestAdapter::getNewInstance($serviceId, $webhookSecret));
    }
}
