<?php

namespace GoDaddy\WordPress\MWC\Core\Payments\API\Controllers\ApplePay;

use Exception;
use GoDaddy\WordPress\MWC\Common\Components\Contracts\ConditionalComponentContract;
use GoDaddy\WordPress\MWC\Common\Exceptions\BaseException;
use GoDaddy\WordPress\MWC\Common\Helpers\ArrayHelper;
use GoDaddy\WordPress\MWC\Common\Helpers\StringHelper;
use GoDaddy\WordPress\MWC\Common\Repositories\WooCommerce\CartRepository;
use GoDaddy\WordPress\MWC\Common\Repositories\WooCommerce\CountriesRepository;
use GoDaddy\WordPress\MWC\Common\Repositories\WooCommerce\CouponsRepository;
use GoDaddy\WordPress\MWC\Common\Repositories\WooCommerce\ProductsRepository;
use GoDaddy\WordPress\MWC\Common\Repositories\WooCommerce\SessionRepository;
use GoDaddy\WordPress\MWC\Common\Repositories\WooCommerceRepository;
use GoDaddy\WordPress\MWC\Common\Repositories\WordPress\SiteRepository;
use GoDaddy\WordPress\MWC\Core\Payments\API\Traits\InitializesCartTrait;
use GoDaddy\WordPress\MWC\Core\Payments\API\Traits\VerifiesNonceTrait;
use GoDaddy\WordPress\MWC\Core\Payments\Exceptions\WooCommerceCartException;
use GoDaddy\WordPress\MWC\Core\Payments\Exceptions\WooCommerceHandlerException;
use GoDaddy\WordPress\MWC\Core\WooCommerce\Payments\GoDaddyPayments\ApplePayGateway;
use GoDaddy\WordPress\MWC\Dashboard\API\Controllers\AbstractController;
use WC_Shipping_Rate;
use WP_Error;
use WP_REST_Request;
use WP_REST_Response;

/**
 * Payment request controller.
 */
class PaymentRequestController extends AbstractController implements ConditionalComponentContract
{
    use InitializesCartTrait;
    use VerifiesNonceTrait;

    /** @var array[] products passed in by the request */
    protected $products = [];

    /**
     * Sets the endpoint route.
     */
    public function __construct()
    {
        $this->route = 'payments/apple-pay';
    }

    /**
     * Loads the component and registers the endpoint routes.
     */
    public function load()
    {
        $this->registerRoutes();
    }

    /**
     * Registers the endpoint routes.
     */
    public function registerRoutes()
    {
        register_rest_route($this->namespace, '/'.$this->route.'/payment-request', [
            [
                'methods'             => 'GET',
                'callback'            => [$this, 'getPaymentRequest'],
                'permission_callback' => '__return_true',
                'schema'              => [$this, 'getItemSchema'],
            ],
        ]);
    }

    /**
     * Gets the chosen shipping methods.
     *
     * @return string[] list of chosen shipping methods
     * @throws Exception
     */
    protected function getChosenShippingMethods() : array
    {
        return ArrayHelper::wrap(SessionRepository::get('chosen_shipping_methods', []));
    }

    /**
     * Gets the available shipping methods.
     *
     * @return array
     * @throws WooCommerceHandlerException|WooCommerceCartException
     * @throws Exception
     */
    protected function getShippingMethods() : array
    {
        $methods = array_values(array_map(static function (WC_Shipping_Rate $rate) {
            return [
                'id'     => $rate->get_id(),
                'label'  => $rate->get_label(),
                'amount' => round((float) $rate->get_cost(), 2),
                'detail' => '',
            ];
        }, $this->getShippingPackageRates()));

        if ($chosenShippingMethod = $this->getChosenShippingMethods()[0]) {
            usort($methods, static function ($method) use ($chosenShippingMethod) {
                return $chosenShippingMethod === $method['id'] ? -1 : 1;
            });
        }

        return $methods;
    }

    /**
     * Gets the current shipping package rates.
     *
     * @return WC_Shipping_Rate[]
     * @throws WooCommerceCartException|WooCommerceHandlerException
     */
    protected function getShippingPackageRates() : array
    {
        if (! $wooCommerceInstance = WooCommerceRepository::getInstance()) {
            throw new WooCommerceHandlerException(__('WooCommerce is not available', 'mwc-core'));
        }

        $packages = $wooCommerceInstance->shipping()->get_packages();

        if (count($packages) > 1) {
            throw new WooCommerceCartException(__('Apple Pay cannot be used for carts with multiple shipments.', 'mwc-core'));
        }

        return ArrayHelper::get(current($packages), 'rates', []);
    }

    /**
     * Gets the shipping type for an order.
     *
     * @return string|null
     * @throws Exception
     */
    protected function getShippingType() : ?string
    {
        $shippingMethods = $this->getChosenShippingMethods();

        if (empty($shippingMethods) || count($shippingMethods) > 1) {
            return null;
        }

        switch (StringHelper::before(current($shippingMethods), ':')) {

            case 'local_pickup':
            case 'local_pickup_plus':
                return 'pickup';

            case 'mwc_local_delivery':
                return 'delivery';

            default:
                return 'shipping';
        }
    }

    /**
     * Gets the line items, formatted for this payment request.
     *
     * @return array[] list of line amounts, and labels
     * @throws Exception
     */
    protected function getLineItems() : array
    {
        // TODO: consider implementing full line item support when using products from request. {@itambek 2022-05-16}
        if ($this->hasProductsFromRequest()) {
            return [];
        }

        $cart = $this->getCartInstance();
        $result = [];

        if ($subTotal = $cart->get_subtotal()) {
            $result[] = [
                'amount' => round($subTotal, 2),
                'label'  => __('Subtotal', 'mwc-core'),
            ];
        }

        if ($discountTotal = $cart->get_discount_total()) {
            $result[] = [
                'amount' => -round($discountTotal, 2),
                'label'  => __('Discount', 'mwc-core'),
            ];
        }

        if ($shippingTotal = $cart->get_shipping_total()) {
            $result[] = [
                'amount' => round($shippingTotal, 2),
                'label'  => __('Shipping', 'mwc-core'),
            ];
        }

        if (! empty($fees = $cart->get_fees())) {
            foreach ($fees as $fee) {
                $result[] = [
                    'amount' => round($fee->amount, 2),
                    'label'  => $fee->name,
                ];
            }
        }

        if ($taxTotal = $cart->get_taxes_total(false, false)) {
            $result[] = [
                'amount' => round($taxTotal, 2),
                'label'  => __('Tax', 'mwc-core'),
            ];
        }

        return $result;
    }

    /**
     * Gets the cart total.
     *
     * @return float the total price in the cart
     * @throws Exception
     */
    protected function getTotal() : float
    {
        return ! $this->hasProductsFromRequest() ? round((float) ArrayHelper::get($this->getCartInstance()->get_totals(), 'total', 0.00), 2) : 0;
    }

    /**
     * Gets the items from the cart.
     *
     * @return array[] the session's cart items
     * @throws Exception
     */
    protected function getCartItems() : array
    {
        return ArrayHelper::wrap($this->getCartInstance()->get_cart());
    }

    /**
     * Gets the payment request for the current customer.
     *
     * @return WP_REST_Response|WP_Error
     * @throws Exception
     * @internal
     */
    public function getPaymentRequest(WP_REST_Request $request)
    {
        try {
            $this->initializeProducts($request);
            $this->initializeCart();

            $response = [
                'total' => [
                    'amount' => $this->getTotal(),
                    'label'  => SiteRepository::getTitle(),
                ],
                'country'                => WooCommerceRepository::getBaseCountry(),
                'currency'               => WooCommerceRepository::getCurrency(),
                'merchantName'           => SiteRepository::getTitle(),
                'shippingType'           => $this->needsShipping() ? $this->getShippingType() : null,
                'shippingMethods'        => $this->needsShipping() ? $this->getShippingMethods() : [],
                'lineItems'              => $this->getLineItems(),
                'requireEmail'           => $this->isAddressFieldRequired('email'),
                'requirePhone'           => $this->isAddressFieldRequired('phone'),
                'requireShippingAddress' => $this->needsShipping(),
                'supportCouponCode'      => CouponsRepository::couponsEnabled(),
                'disableWallets'         => [
                    'googlePay' => true,
                ],
            ];
        } catch (Exception $exception) {
            $response = $this->getPaymentRequestError($exception);
        }

        return rest_ensure_response($response);
    }

    /**
     * Gets a payment request error.
     *
     * @param Exception $exception
     * @return WP_Error
     */
    protected function getPaymentRequestError(Exception $exception) : WP_Error
    {
        return new WP_Error('UNKNOWN', $exception->getMessage(),
            [
                'status' => 500,
                'field'  => null,
            ]
        );
    }

    /**
     * Gets the item schema.
     *
     * @internal
     *
     * @return array
     */
    public function getItemSchema() : array
    {
        return [
            '$schema'    => 'http://json-schema.org/draft-04/schema#',
            'title'      => 'status',
            'type'       => 'object',
            'properties' => [
                'total' => [
                    'description' => __('Order total, based on cart total.', 'mwc-core'),
                    'type'        => 'object',
                    'context'     => ['view', 'edit'],
                    'readonly'    => true,
                    'properties'  => [
                        'amount' => [
                            'type' => 'float',
                        ],
                        'label' => [
                            'type' => 'string',
                        ],
                    ],
                ],
                'country' => [
                    'description' => __('2-letter ISO 3166 country code.', 'mwc-core'),
                    'type'        => 'string',
                    'context'     => ['view', 'edit'],
                    'readonly'    => true,
                ],
                'currency' => [
                    'description' => __('3-letter ISO 4217 currency code.', 'mwc-core'),
                    'type'        => 'string',
                    'context'     => ['view', 'edit'],
                    'readonly'    => true,
                ],
                'merchantName' => [
                    'description' => __('Name of the merchant.', 'mwc-core'),
                    'type'        => 'string',
                    'context'     => ['view', 'edit'],
                    'readonly'    => true,
                ],
                'shippingType' => [
                    'description' => __('The shipping type based on the chosen shipping method.', 'mwc-core'),
                    'type'        => 'string',
                    'context'     => ['view', 'edit'],
                    'readonly'    => true,
                    'enum'        => [
                        'delivery',
                        'pickup',
                        'shipping',
                    ],
                ],
                'shippingMethods' => [
                    'description' => __('Shipping methods for the payment request.', 'mwc-core'),
                    'type'        => 'array',
                    'context'     => ['view', 'edit'],
                    'readonly'    => true,
                    'items'       => [
                        'type' => 'string',
                    ],
                ],
                'lineItems' => [
                    'description' => __('Items in the order.', 'mwc-core'),
                    'type'        => 'array',
                    'context'     => ['view', 'edit'],
                    'readonly'    => true,
                    'items'       => [
                        'type'       => 'object',
                        'properties' => [
                            'amount' => [
                                'type' => 'float',
                            ],
                            'label' => [
                                'type' => 'string',
                            ],
                        ],
                    ],
                ],
                'requireEmail' => [
                    'description' => __('Whether to require customer email.', 'mwc-core'),
                    'type'        => 'boolean',
                    'context'     => ['view', 'edit'],
                    'readonly'    => true,
                ],
                'requirePhone' => [
                    'description' => __('Whether to require customer phone.', 'mwc-core'),
                    'type'        => 'boolean',
                    'context'     => ['view', 'edit'],
                    'readonly'    => true,
                ],
                'requireShippingAddress' => [
                    'description' => __('Whether to require customer shipping address.', 'mwc-core'),
                    'type'        => 'boolean',
                    'context'     => ['view', 'edit'],
                    'readonly'    => true,
                ],
                'supportCouponCode' => [
                    'description' => __('Whether the customer should be allowed to enter coupons.', 'mwc-core'),
                    'type'        => 'boolean',
                    'context'     => ['view', 'edit'],
                    'readonly'    => true,
                ],
                'disableWallets' => [
                    'description' => __('A list of wallets to disable.', 'mwc-core'),
                    'type'        => 'object',
                    'context'     => ['view', 'edit'],
                    'readonly'    => true,
                    'properties'  => [
                        'applePay' => [
                            'type' => 'boolean',
                        ],
                        'googlePay' => [
                            'type' => 'boolean',
                        ],
                    ],
                ],

            ],
        ];
    }

    /**
     * Determines whether the component should load.
     *
     * @return bool
     * @throws Exception
     */
    public static function shouldLoad() : bool
    {
        return ApplePayGateway::isActive();
    }

    /**
     * Gets the required fields, given the type.
     *
     * @param array[] $addressFields list of associative arrays containing a required field, keyed by the field type
     * @param string $addressType the field type - "shipping" or "billing"
     * @return string[] list of required field identifiers (email, phone, etc)
     */
    protected function getRequiredAddressFields(array $addressFields, string $addressType) : array
    {
        $requiredFields = [];

        foreach ($addressFields as $key => $field) {
            if (isset($field['required']) && $field['required']) {
                $requiredFields[] = str_replace($addressType.'_', '', $key);
            }
        }

        return array_unique($requiredFields);
    }

    /**
     * Determines if the given field is required either for billing or shipping.
     *
     * @param string $field
     * @return bool
     * @throws BaseException
     * @throws Exception
     */
    protected function isAddressFieldRequired(string $field) : bool
    {
        return ArrayHelper::contains(
            ArrayHelper::combine(
                $this->getRequiredAddressFields(CountriesRepository::getInstance()->get_address_fields('', 'billing_'), 'billing'),
                $this->getRequiredAddressFields(CountriesRepository::getInstance()->get_address_fields('', 'shipping_'), 'shipping')
            ),
            $field
        );
    }

    /**
     * Initialized products passed in by the request.
     *
     * @param WP_REST_Request $request
     * @return array|array[]
     * @throws Exception
     */
    protected function initializeProducts(WP_REST_Request $request) : array
    {
        $products = $request->get_param('products');

        if (empty($products)) {
            return [];
        }

        if (! ArrayHelper::accessible($products)) {
            throw new Exception(__('Invalid products data.', 'mwc-core'));
        }

        foreach ($products as $productData) {
            $product = ProductsRepository::get((int) ArrayHelper::get($productData, 'id'));

            if (! $product || ! $product->is_purchasable() || ! $product->is_in_stock()) {
                continue;
            }

            $this->products[] = [
                'product'  => $product,
                'quantity' => (float) ArrayHelper::get($productData, 'quantity', 1),
            ];
        }

        return $this->products;
    }

    /**
     * Checks whether we have products passed in by the request.
     *
     * @return bool
     */
    protected function hasProductsFromRequest() : bool
    {
        return ! empty($this->products);
    }

    /**
     * Checks whether any of the products passed in by the request require shipping.
     *
     * @return bool
     */
    protected function anyProductNeedsShipping() : bool
    {
        foreach ($this->products as $productLine) {
            if ($productLine['product']->needs_shipping()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Determines whether the payment request requires shipping.
     *
     * @return bool
     * @throws Exception
     */
    protected function needsShipping() : bool
    {
        return $this->hasProductsFromRequest() ? $this->anyProductNeedsShipping() : CartRepository::getInstance()->needs_shipping_address();
    }
}
