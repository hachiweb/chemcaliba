<?php

namespace GoDaddy\WordPress\MWC\Core\Payments\API\Traits;

use WP_Error;
use WP_REST_Request;

/**
 * A trait for API controllers that need to verify the request nonce.
 */
trait VerifiesNonceTrait
{
    /**
     * Verifies a nonce to prevent CSRF attacks.
     *
     * Nonces will mismatch if the logged in session cookie is different! If using a client to test, set this cookie
     * to match the logged in cookie in your browser.
     *
     * @param WP_REST_Request $request Request object.
     * @param string $action
     * @return WP_Error|bool
     */
    protected function verifyNonce(WP_REST_Request $request, string $action)
    {
        if (! wp_verify_nonce($nonce = $request->get_header('X-MWC-Payments-Nonce'), $action)) {
            return new WP_Error('INVALID_NONCE', __('Invalid or missing nonce', 'mwc-core'),
                [
                    'status' => $nonce ? 403 : 401,
                ]
            );
        }

        return true;
    }

    /**
     * Determines whether the given route should be authenticated by nonce verification.
     *
     * @param string $route
     * @return bool
     */
    public function shouldAuthenticateRouteByNonceVerification(string $route) : bool
    {
        return 0 === strpos($route, '/'.$this->namespace.'/'.$this->route);
    }
}
