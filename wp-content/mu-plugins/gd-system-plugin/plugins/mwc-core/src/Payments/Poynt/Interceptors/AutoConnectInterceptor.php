<?php

namespace GoDaddy\WordPress\MWC\Core\Payments\Poynt\Interceptors;

use Exception;
use GoDaddy\WordPress\MWC\Common\Configuration\Configuration;
use GoDaddy\WordPress\MWC\Common\Events\Events;
use GoDaddy\WordPress\MWC\Common\Exceptions\SentryException;
use GoDaddy\WordPress\MWC\Common\Helpers\ArrayHelper;
use GoDaddy\WordPress\MWC\Common\Interceptors\AbstractInterceptor;
use GoDaddy\WordPress\MWC\Common\Register\Register;
use GoDaddy\WordPress\MWC\Common\Repositories\SentryRepository;
use GoDaddy\WordPress\MWC\Common\Repositories\WooCommerceRepository;
use GoDaddy\WordPress\MWC\Core\Payments\Events\ProviderAccountAssociatedEvent;
use GoDaddy\WordPress\MWC\Core\Payments\Events\ProviderAccountNotAssociatedEvent;
use GoDaddy\WordPress\MWC\Core\Payments\Exceptions\MissingParameterException;
use GoDaddy\WordPress\MWC\Core\Payments\Poynt;
use GoDaddy\WordPress\MWC\Core\Payments\Poynt\Onboarding;
use GoDaddy\WordPress\MWC\Core\Payments\Providers\MWC\Gateways\OnboardingAccountGateway;
use GoDaddy\WordPress\MWC\Core\WooCommerce\Payments\Events\Producers\OnboardingEventsProducer;
use GoDaddy\WordPress\MWC\Core\WooCommerce\Payments\GoDaddyPaymentsGateway;

/**
 * An interceptor to auto-connect GoDaddy Payments when the admin is loaded for the first time.
 */
class AutoConnectInterceptor extends AbstractInterceptor
{
    /** @var string the plugin name */
    protected static $attemptedFlagOptionName = 'mwc_payments_poynt_auto_connect_attempted';

    /**
     * Determines whether the interceptor should be loaded.
     *
     * @return bool
     * @throws Exception
     */
    public static function shouldLoad() : bool
    {
        return WooCommerceRepository::isWooCommerceActive() &&
               true === Configuration::get('features.gdp_by_default.enabled') &&
               ! static::wasAttempted();
    }

    /**
     * Adds the action & filter hooks.
     *
     * @throws Exception
     */
    public function addHooks() : void
    {
        Register::action()
            ->setGroup('admin_init')
            ->setHandler([$this, 'attemptAutoConnect'])
            ->execute();
    }

    /**
     * Attempts to auto-connect the site to GoDaddy Payments.
     *
     * @throws Exception
     * @internal
     */
    public function attemptAutoConnect() : void
    {
        static::setAttempted(true);

        try {
            // only proceed for sites that are eligible for GDP and haven't already attempted connection
            if (! GoDaddyPaymentsGateway::isActive() || Poynt::getServiceId()) {
                return;
            }

            Onboarding::generateIds();

            $account = OnboardingAccountGateway::getNewInstance()->findOrCreate(Poynt::getServiceId(), Onboarding::getWebhookSecret());

            $this->validateAccountData($account);

            Poynt::setAppId(ArrayHelper::get($account, 'cloudAppId'));
            Poynt::setApplicationId(ArrayHelper::get($account, 'applicationId'));
            Poynt::setBusinessId(ArrayHelper::get($account, 'businessId'));
            Poynt::setPrivateKey(ArrayHelper::get($account, 'privateKey'));
            Poynt::setPublicKey(ArrayHelper::get($account, 'publicKey'));
            Poynt::setServiceId(ArrayHelper::get($account, 'serviceId'));

            Events::broadcast(new ProviderAccountAssociatedEvent('godaddy-payments'));

            $this->updateAccount();
        } catch (Exception $exception) {
            Events::broadcast(new ProviderAccountNotAssociatedEvent('godaddy-payments'));

            if (SentryRepository::loadSDK()) {
                \Sentry\captureException(new SentryException($exception->getMessage()));
            }
        }
    }

    /**
     * Validates the Account Data array for required keys.
     *
     * @param array $account
     * @return void
     * @throws MissingParameterException
     */
    protected function validateAccountData(array $account) : void
    {
        foreach ([
            'cloudAppId',
            'applicationId',
            'businessId',
            'privateKey',
            'publicKey',
            'serviceId',
        ] as $key) {
            if (empty(ArrayHelper::get($account, $key))) {
                throw new MissingParameterException("Could not validate account data, missing {$key}");
            }
        }
    }

    /**
     * Updates account by calling getNewInstance on OnboardingEventsProducer.
     *
     * @return void
     * @throws Exception
     */
    protected function updateAccount() : void
    {
        OnboardingEventsProducer::getNewInstance()->updateAccount();
    }

    /**
     * Determines if the auto-connect was already attempted.
     *
     * @return bool
     */
    public static function wasAttempted() : bool
    {
        return 'yes' === get_option(static::$attemptedFlagOptionName);
    }

    /**
     * Sets whether the auto-connect has been attempted.
     *
     * @param bool $wasAttempted
     */
    public static function setAttempted(bool $wasAttempted) : void
    {
        update_option(static::$attemptedFlagOptionName, wc_bool_to_string($wasAttempted));
    }
}
