<?php

namespace GoDaddy\WordPress\MWC\Core\WooCommerce\Payments;

use GoDaddy\WordPress\MWC\Common\Exceptions\BaseException;
use GoDaddy\WordPress\MWC\Common\Repositories\WooCommerceRepository;
use GoDaddy\WordPress\MWC\Core\Features\Stripe\Stripe;
use GoDaddy\WordPress\MWC\Core\Payments\Stripe\Locales\LocalHelper;
use GoDaddy\WordPress\MWC\Payments\Models\PaymentMethods\AbstractPaymentMethod;
use GoDaddy\WordPress\MWC\Payments\Models\PaymentMethods\CardPaymentMethod;

/**
 * GoDaddy Stripe Native payment Gateway.
 */
class StripeGateway extends AbstractPaymentGateway
{
    /** @var string */
    public $providerName = 'stripe';

    /**
     * Constructs the gateway.
     */
    public function __construct()
    {
        $this->id = $this->providerName;

        $this->method_title = __('Stripe', 'mwc-core');
        $this->method_description = __('Accept online payments using credit/debit cards in over 35 international locations with Stripe.', 'mwc-core');

        // TODO: call the parent constructor after the provider is implemented in MWC-5603 {@cwiseman 2022-05-05}
    }

    /**
     * Determines if the gateway should be active for use.
     *
     * @return bool
     * @throws BaseException
     */
    public static function isActive() : bool
    {
        if (! Stripe::shouldLoad()) {
            return false;
        }

        if (static::isStripePluginActive()) {
            return false;
        }

        return
            LocalHelper::isSupportedCountry($country = WooCommerceRepository::getBaseCountry())
            && LocalHelper::isSupportedCurrency($country, WooCommerceRepository::getCurrency());
    }

    /**
     * Gets a card payment method to add.
     *
     * @return AbstractPaymentMethod
     */
    protected function getPaymentMethodForAdd() : AbstractPaymentMethod
    {
        // TODO: Implement method per MWC-5617. {ssmith1 2022-05-03}
        return new CardPaymentMethod();
    }

    /**
     * Checks if Stripe plugin is active.
     *
     * @return bool
     */
    protected static function isStripePluginActive() : bool
    {
        return function_exists('is_plugin_active') && is_plugin_active('woocommerce-gateway-stripe/woocommerce-gateway-stripe.php');
    }
}
