<?php

namespace GoDaddy\WordPress\MWC\Core\WooCommerce\Adapters;

use GoDaddy\WordPress\MWC\Common\DataSources\WooCommerce\Adapters\CurrencyAmountAdapter;
use GoDaddy\WordPress\MWC\Common\DataSources\WooCommerce\Adapters\Order\OrderAdapter as CommonOrderAdapter;
use GoDaddy\WordPress\MWC\Common\Exceptions\AdapterException;
use GoDaddy\WordPress\MWC\Common\Models\CurrencyAmount;
use GoDaddy\WordPress\MWC\Common\Models\Orders\Order;
use GoDaddy\WordPress\MWC\Common\Models\Orders\Statuses\CancelledOrderStatus;
use GoDaddy\WordPress\MWC\Common\Models\Orders\Statuses\FailedOrderStatus;
use GoDaddy\WordPress\MWC\Common\Models\Orders\Statuses\RefundedOrderStatus;
use GoDaddy\WordPress\MWC\Common\Traits\CanGetNewInstanceTrait;
use GoDaddy\WordPress\MWC\Core\WooCommerce\Models\Orders\Order as CoreOrder;
use WC_Order;

/**
 * The Core's Order adapter.
 *
 * Converts between a native core order object and a WooCommerce order object.
 */
class OrderAdapter extends CommonOrderAdapter
{
    use CanGetNewInstanceTrait;

    /** @var string the marketplace channel name order meta key */
    const MARKETPLACE_CHANNEL_NAME_META_KEY = 'marketplace_channel_name';

    /** @var string the marketplace channel type order meta key */
    const MARKETPLACE_CHANNEL_TYPE_META_KEY = 'marketplace_channel_type';

    /** @var string the marketplace channel uuid order meta key */
    const MARKETPLACE_CHANNEL_UUID_META_KEY = 'marketplace_channel_uuid';

    /** @var string the marketplace order number order meta key */
    const MARKETPLACE_ORDER_NUMBER_META_KEY = 'marketplace_order_number';

    /** {@inheritDoc} */
    protected $orderClass = CoreOrder::class;

    /**
     * Converts a source WooCommerce Order to a core native order.
     *
     * @return CoreOrder
     * @throws AdapterException
     */
    public function convertFromSource() : Order
    {
        /** @var CoreOrder $order */
        $order = parent::convertFromSource();

        $this->convertPaymentDataFromSource($order);

        $this->convertMarketplaceDataFromSource($order);

        return $order;
    }

    /**
     * Converts a core native order to a WooCommerce source order object.
     *
     * @param CoreOrder $order
     * @return WC_Order
     * @throws AdapterException
     */
    public function convertToSource($order = null) : WC_Order
    {
        $wcOrder = parent::convertToSource($order);

        $this->convertMarketplaceDataToSource($wcOrder, $order);

        return $wcOrder;
    }

    /**
     * Converts payment information from a WC Order object to a core order instance.
     *
     * @param CoreOrder $order
     */
    protected function convertPaymentDataFromSource(CoreOrder $order)
    {
        if ($emailAddress = $this->source->get_billing_email()) {
            $order->setEmailAddress($emailAddress);
        }

        if ($orderNotes = $this->source->get_customer_note()) {
            $order->setOrderNotes($orderNotes);
        }

        if ('yes' === $this->source->get_meta('_mwc_payments_is_captured')
            || ('poynt' !== $this->source->get_meta('_mwc_transaction_provider_name') && ! empty($this->source->get_date_paid()) && (! empty($this->source->get_transaction_id())))) {
            $order->setCaptured(true);
        } elseif ($this->isOrderReadyForCapture($order)) {
            $order->setReadyForCapture(true);
        }

        if ($remoteId = $this->source->get_meta('_poynt_order_remoteId')) {
            $order->setRemoteId($remoteId);
        }

        if ($createdVia = $this->source->get_created_via()) {
            $order->setSource((string) $createdVia);
        }

        // Order amounts
        $order->setDiscountAmount($this->convertCurrencyAmountFromSource((float) $this->source->get_total_discount()));
    }

    /**
     * Converts Marketplace information from a WC Order object to a core order instance.
     *
     * @param CoreOrder $order
     */
    protected function convertMarketplaceDataFromSource(CoreOrder $order)
    {
        if ($channelName = $this->source->get_meta(static::MARKETPLACE_CHANNEL_NAME_META_KEY)) {
            $order->setMarketplaceChannelName($channelName);
        }

        if ($channelType = $this->source->get_meta(static::MARKETPLACE_CHANNEL_TYPE_META_KEY)) {
            $order->setMarketplaceChannelType($channelType);
        }

        if ($channelUuid = $this->source->get_meta(static::MARKETPLACE_CHANNEL_UUID_META_KEY)) {
            $order->setMarketplaceChannelUuid($channelUuid);
        }

        if ($orderNumber = $this->source->get_meta(static::MARKETPLACE_ORDER_NUMBER_META_KEY)) {
            $order->setMarketplaceOrderNumber($orderNumber);
        }
    }

    /**
     * Converts Marketplace information from an order object to the WC Order metadata.
     *
     * @param WC_Order $wcOrder
     * @param null|CoreOrder $order
     */
    protected function convertMarketplaceDataToSource(WC_Order $wcOrder, $order = null)
    {
        if ($order) {
            $wcOrder->add_meta_data(static::MARKETPLACE_CHANNEL_NAME_META_KEY, $order->getMarketplaceChannelName());
            $wcOrder->add_meta_data(static::MARKETPLACE_CHANNEL_TYPE_META_KEY, $order->getMarketplaceChannelType());
            $wcOrder->add_meta_data(static::MARKETPLACE_CHANNEL_UUID_META_KEY, $order->getMarketplaceChannelUuid());
            $wcOrder->add_meta_data(static::MARKETPLACE_ORDER_NUMBER_META_KEY, $order->getMarketplaceOrderNumber());
        }
    }

    /**
     * Determines whether the order is ready to be captured.
     *
     * TODO: remove status classes from mwc-payments package {@wvega 2021-05-31}.
     *
     * @param Order $order
     * @return bool
     */
    protected function isOrderReadyForCapture(Order $order)
    {
        if (! $this->source->get_meta('_poynt_payment_remoteId')) {
            return false;
        }

        // @TODO: something I don't like about this method: these order status checks imply too much knowledge / dependency on the WC admin. I think the status checks should be done "higher up" near the UI layer, since really these are determining whether to render a WC admin button or not {JS - 2021-10-21}
        // @TODO: something I don't like about this method: 'isOrderReadyForCapture' is assuming an action (capture) rather than returning a state (open authorization). An authorization can be captured or can be voided, so a better method name would probably be something like 'hasOpenAuthorization' or something to that effect, and the calling code can determine what to do with that state {JS - 2021-10-21}
        if ($order->getStatus() instanceof CancelledOrderStatus) {
            return false;
        }

        if ($order->getStatus() instanceof RefundedOrderStatus) {
            return false;
        }

        if ($order->getStatus() instanceof FailedOrderStatus) {
            return false;
        }

        return true;
    }

    /**
     * Converts an order amount from source.
     *
     * @param float $amount
     * @return CurrencyAmount
     */
    protected function convertCurrencyAmountFromSource(float $amount) : CurrencyAmount
    {
        return (new CurrencyAmountAdapter($amount, (string) $this->source->get_currency()))->convertFromSource();
    }
}
