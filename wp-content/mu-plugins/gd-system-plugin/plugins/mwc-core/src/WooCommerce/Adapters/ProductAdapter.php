<?php

namespace GoDaddy\WordPress\MWC\Core\WooCommerce\Adapters;

use Exception;
use GoDaddy\WordPress\MWC\Common\DataSources\WooCommerce\Adapters\Product\ProductAdapter as CommonProductAdapter;
use GoDaddy\WordPress\MWC\Common\Models\Dimensions;
use GoDaddy\WordPress\MWC\Common\Models\Products\Product as CommonProduct;
use GoDaddy\WordPress\MWC\Common\Models\Weight;
use GoDaddy\WordPress\MWC\Common\Repositories\WooCommerce\ProductsRepository;
use GoDaddy\WordPress\MWC\Common\Traits\CanGetNewInstanceTrait;
use GoDaddy\WordPress\MWC\Core\Features\Marketplaces\Models\Listing;
use GoDaddy\WordPress\MWC\Core\WooCommerce\Models\Products\Product;
use WC_Product;

/**
 * Core product adapter.
 *
 * Converts between a native core product object and a WooCommerce product object.
 */
class ProductAdapter extends CommonProductAdapter
{
    use CanGetNewInstanceTrait;

    /** @var string the marketplaces listings meta key */
    const MARKETPLACES_LISTINGS_META_KEY = 'marketplaces_listings';

    /** @var string the product class name */
    protected $productClass = Product::class;

    /**
     * Adapts the product from source.
     *
     * @return Product
     * @throws Exception
     */
    public function convertFromSource() : CommonProduct
    {
        /* @var Product $product */
        $product = parent::convertFromSource();

        $product->setIsVirtual((bool) $this->source->is_virtual());
        $product->setIsDownloadable((bool) $this->source->is_downloadable());

        $isStockManaged = (bool) $this->source->get_manage_stock();

        $product->setStockManagementEnabled($isStockManaged);

        if ($isStockManaged && ($currentStock = $this->convertNumberToFloat($this->source->get_stock_quantity()))) {
            $product->setCurrentStock($currentStock);
        }

        $product->setWeight($this->convertWeightFromSource());
        $product->setDimensions($this->convertDimensionsFromSource());

        $product->setVariants($this->convertVariantsFromSource());

        $this->convertMarketplacesDataFromSource($product);

        return $product;
    }

    /**
     * Converts a core native product object into a WooCommerce product object.
     *
     * @param Product|null $product native core product object to convert
     * @param bool $getNewInstance whether to get a fresh instance of a WC_Product
     * @return WC_Product WooCommerce product object
     * @throws Exception
     */
    public function convertToSource($product = null, bool $getNewInstance = true) : WC_Product
    {
        $this->source = parent::convertToSource($product, $getNewInstance);

        $this->convertMarketplacesDataToSource($this->source, $product);

        return $this->source;
    }

    /**
     * Converts product weight from source.
     *
     * @return Weight
     */
    protected function convertWeightFromSource() : Weight
    {
        $weight = new Weight();

        if ($value = $this->convertNumberToFloat($this->source->get_weight())) {
            $weight->setValue($value);
        }

        if ($unit = (string) get_option('woocommerce_weight_unit', '')) {
            $weight->setUnitOfMeasurement($unit);
        }

        return $weight;
    }

    /**
     * Converts product dimensions from source.
     *
     * @return Dimensions
     */
    protected function convertDimensionsFromSource() : Dimensions
    {
        $dimensions = new Dimensions();

        foreach (['width', 'height', 'length'] as $dimension) {
            $getDimension = 'get_'.$dimension;
            $setDimension = 'set'.ucfirst($dimension);

            if ($value = $this->convertNumberToFloat($this->source->{$getDimension}())) {
                $dimensions->{$setDimension}($value);
            }
        }

        if ($unit = (string) get_option('woocommerce_dimension_unit', '')) {
            $dimensions->setUnitOfMeasurement($unit);
        }

        return $dimensions;
    }

    /**
     * Converts source product variations into native product variants, if any.
     *
     * @return Product[]
     * @throws Exception
     */
    protected function convertVariantsFromSource() : array
    {
        $variants = [];

        if ($variations = $this->source->get_children()) {
            foreach ($variations as $variationId) {
                if ($source = ProductsRepository::get($variationId)) {
                    $variants[] = static::getNewInstance($source)->convertFromSource();
                }
            }
        }

        return $variants;
    }

    /**
     * Ensures that a number will be adapted to a float.
     *
     * @param string|int|float|mixed $number
     * @return float|null
     */
    protected function convertNumberToFloat($number) : ?float
    {
        return is_numeric($number) ? (float) $number : null;
    }

    /**
     * Converts Marketplaces listings' information from a WC Product object to a core product instance.
     *
     * @param Product $product
     */
    protected function convertMarketplacesDataFromSource(Product $product)
    {
        if (! empty($marketplacesListingsMeta = $this->source->get_meta(static::MARKETPLACES_LISTINGS_META_KEY))) {
            $listings = [];

            foreach ($marketplacesListingsMeta as $marketplacesListingMeta) {
                $listing = new Listing();
                $listing->setProperties(array_filter($marketplacesListingMeta));

                $listings[] = $listing;
            }

            $product->setMarketplacesListings($listings);
        }
    }

    /**
     * Converts Marketplaces listings' information from a core product object to the WC Product metadata.
     *
     * @param WC_Product $wcProduct
     * @param null|Product $product
     */
    protected function convertMarketplacesDataToSource(WC_Product $wcProduct, $product = null)
    {
        if ($product) {
            $listings = $product->getMarketplacesListings();
            $marketplacesListingsMeta = [];

            foreach ($listings as $listing) {
                $marketplacesListingsMeta[] = $listing->toArray();
            }

            $wcProduct->update_meta_data(static::MARKETPLACES_LISTINGS_META_KEY, $marketplacesListingsMeta);
        }
    }
}
