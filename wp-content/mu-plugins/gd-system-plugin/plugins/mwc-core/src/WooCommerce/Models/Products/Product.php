<?php

namespace GoDaddy\WordPress\MWC\Core\WooCommerce\Models\Products;

use GoDaddy\WordPress\MWC\Common\Events\Events;
use GoDaddy\WordPress\MWC\Common\Events\Exceptions\EventTransformFailedException;
use GoDaddy\WordPress\MWC\Common\Models\Products\Product as CommonProduct;
use GoDaddy\WordPress\MWC\Common\Traits\HasDimensionsTrait;
use GoDaddy\WordPress\MWC\Common\Traits\HasWeightTrait;
use GoDaddy\WordPress\MWC\Core\Features\Marketplaces\Models\Listing;

/**
 * Core product object.
 */
class Product extends CommonProduct
{
    use HasDimensionsTrait, HasWeightTrait;

    /** @var bool whether the product is virtual */
    protected $isVirtual = false;

    /** @var bool whether the product is downloadable */
    protected $isDownloadable = false;

    /** @var bool whether stock is managed for the product */
    protected $stockManagementEnabled = false;

    /** @var float|null current stock quantity (if managed) */
    protected $currentStock;

    /** @var Product[]|null variations of this product */
    protected $variants;

    /** @var Listing[] */
    protected $marketplacesListings = [];

    /**
     * Gets the product's virtual status.
     *
     * @return bool
     */
    public function getIsVirtual() : bool
    {
        return $this->isVirtual;
    }

    /**
     * Determines if the product is a virtual product.
     *
     * @return bool
     */
    public function isVirtual() : bool
    {
        return $this->getIsVirtual();
    }

    /**
     * Gets the product's downloadable status.
     *
     * @return bool
     */
    public function getIsDownloadable() : bool
    {
        return $this->isDownloadable;
    }

    /**
     * Determines if the product is downloadable.
     *
     * @return bool
     */
    public function isDownloadable() : bool
    {
        return $this->getIsDownloadable();
    }

    /**
     * Gets the product stock management enabled value.
     *
     * @return bool
     */
    public function getStockManagementEnabled() : bool
    {
        return $this->stockManagementEnabled;
    }

    /**
     * Determines if the stock management is enabled for the product.
     *
     * @return bool
     */
    public function hasStockManagementEnabled() : bool
    {
        return $this->getStockManagementEnabled();
    }

    /**
     * Gets the product current stock level.
     *
     * @return float|null
     */
    public function getCurrentStock() : ?float
    {
        return $this->currentStock;
    }

    /**
     * Gets the product variants.
     *
     * @return Product[]|null
     */
    public function getVariants() : ?array
    {
        return $this->variants;
    }

    /**
     * Gets the product Marketplaces listings.
     *
     * @return Listing[]
     */
    public function getMarketplacesListings() : array
    {
        return $this->marketplacesListings;
    }

    /**
     * Sets the product virtual status.
     *
     * @param bool $value
     * @return $this
     */
    public function setIsVirtual(bool $value) : Product
    {
        $this->isVirtual = $value;

        return $this;
    }

    /**
     * Sets the product downloadable status.
     *
     * @param bool $value
     * @return $this
     */
    public function setIsDownloadable(bool $value) : Product
    {
        $this->isDownloadable = $value;

        return $this;
    }

    /**
     * Sets the stock management enabled value.
     *
     * @param bool $value
     * @return $this
     */
    public function setStockManagementEnabled(bool $value) : Product
    {
        $this->stockManagementEnabled = $value;

        return $this;
    }

    /**
     * Sets the current stock level.
     *
     * @param float|null $value
     * @return $this
     */
    public function setCurrentStock(?float $value) : Product
    {
        $this->currentStock = $value;

        return $this;
    }

    /**
     * Sets the product variants.
     *
     * @param Product[] $value
     * @return $this
     */
    public function setVariants(array $value) : Product
    {
        $this->variants = $value;

        return $this;
    }

    /**
     * Sets the product Marketplaces listings.
     *
     * @param \GoDaddy\WordPress\MWC\Core\Features\Marketplaces\Models\Listing[] $value
     * @return $this
     */
    public function setMarketplacesListings(array $value) : Product
    {
        $this->marketplacesListings = $value;

        return $this;
    }

    /**
     * Updates the product.
     *
     * This method also broadcast model events.
     *
     * @return self
     * @throws EventTransformFailedException
     */
    public function update() : Product
    {
        $product = parent::update();

        Events::broadcast($this->buildEvent('product', 'update'));

        return $product;
    }

    /**
     * Saves the product.
     *
     * This method also broadcast model events.
     *
     * @return self
     * @throws EventTransformFailedException
     */
    public function save() : Product
    {
        $product = parent::save();

        Events::broadcast($this->buildEvent('product', 'create'));

        return $product;
    }

    /**
     * Deletes the product.
     *
     * This method also broadcast model events.
     *
     * @throws EventTransformFailedException
     */
    public function delete()
    {
        parent::delete();

        Events::broadcast($this->buildEvent('product', 'delete'));
    }
}
